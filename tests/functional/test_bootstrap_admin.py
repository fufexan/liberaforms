"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import pytest
from urllib.parse import urlparse
import flask_login
from liberaforms.models.user import User
from liberaforms.models.invite import Invite
from tests import user_creds


class TestBootstrapAdmin():

    def test_bootstrap_admin(self, anon_client):
        """Create an admin user using ROOT_USER as defined in ./tests/test.ini

        Recover password using ROOT_USER email address creates an invitation.
        """
        response = anon_client.post(
                        "/site/recover-password",
                        data = {
                            "email": user_creds['root_user']['email'],
                        },
                        follow_redirects=False,
                    )
        assert Invite.find(email=user_creds['root_user']['email'])
        assert response.status_code == 302
        # this path contains the invitation
        # a POST will create an Admin user and login
        new_user_url = urlparse(response.location).path
        response = anon_client.post(
                            new_user_url,
                            data = {
                                "username": user_creds['root_user']['username'],
                                "email": user_creds['root_user']['email'],
                                "password": user_creds['root_user']['password'],
                                "password2": user_creds['root_user']['password'],
                            },
                            follow_redirects=True,
                        )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- user_settings_page -->" in html
        assert flask_login.current_user.is_authenticated
        assert flask_login.current_user.role == 'admin'
        assert '<a class="nav-link" href="/user/logout">' in html
