"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import werkzeug
from io import BytesIO
import mimetypes
from flask import current_app, url_for
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import AnswerAttachment
from tests import user_creds
from tests.assets.form_structure import get_form_structure
from tests.utils import login, logout, random_slug


def _create_form_user(form, editor):
    form_user = FormUser(
            user_id = editor.id,
            form_id = form.id,
            is_editor = True,
            notifications = editor.new_form_notifications()
    )
    form_user.save()


class TestAnswerAttachment():

    @classmethod
    def setup_class(cls):
        cls.properties={}

    def test_requirements(self, editor):
        self.properties['editor']=editor

    def test_create_form_with_file_field(self, editor, editor_client):
        """ Creates a form with valid data.
            Tests Preview page and saves form
            Tests for a new FormLog
        """
        assert editor.uploads_enabled
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        "/forms/new",
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<form id="result" method="POST" action="/form/edit" >' in html
        slug = random_slug()
        response = editor_client.post(
                        "/form/edit",
                        data = {
                            "introductionTextMD": "# hello",
                            "structure": get_form_structure(with_attachment=True),
                            "slug": slug,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<form action="/form/save" method="post">' in html
        response = editor_client.post(
                        "/form/save",
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- inspect_form_page -->' in html
        form = Form.find(slug=slug)
        _create_form_user(form, editor)
        assert form.log.count() == 1
        self.properties['form'] = form

    def test_submit_valid_attachment(self, anon_client):
        form = self.properties['form']
        form.enabled = True
        form.save()
        valid_attachment_name = "valid_attachment.pdf"
        valid_attachment_path = f"./assets/{valid_attachment_name}"
        mimetype = mimetypes.guess_type(valid_attachment_path)[0]
        with open(valid_attachment_path, 'rb') as f:
            stream = BytesIO(f.read())
        valid_file = werkzeug.datastructures.FileStorage(
            stream=stream,
            filename=valid_attachment_name,
            content_type=mimetype,
        )
        name = "Julia"
        response = anon_client.post(
                        form.url,
                        data = {
                            "text-1620232883208": name,
                            "file-1622045746136": valid_file,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- thank_you_page -->" in html
        answer = form.answers[0]
        assert answer.data['text-1620232883208'] == name
        attachment = AnswerAttachment.find(form_id=form.id, answer_id=answer.id)
        assert attachment.does_attachment_exist() is True
        assert len(os.listdir(form.get_attachment_dir())) == 1
        assert attachment.file_name == valid_attachment_name

    def test_delete_answer_and_attachment(self, editor_client):
        form = self.properties['form']
        initial_log_count = form.log.count()
        initial_answers_count = form.answers.count()
        answer = form.answers[-1]
        answer_id = vars(answer)['id']
        attachment = AnswerAttachment.find(form_id=form.id,
                                           answer_id=answer_id)
        response = editor_client.delete(
                    url_for('data_display_bp.delete_answer', answer_id=answer_id),
                    follow_redirects=False,
                )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['deleted'] is True
        assert form.answers.count() == initial_answers_count - 1
        assert form.log.count() == initial_log_count + 1
        assert attachment.does_attachment_exist() is False

    def test_delete_all_answers_and_attachment(self, anon_client, editor_client):
        form = self.properties['form']
        valid_attachment_name = "valid_attachment.pdf"
        valid_attachment_path = f"./assets/{valid_attachment_name}"
        mimetype = mimetypes.guess_type(valid_attachment_path)[0]
        with open(valid_attachment_path, 'rb') as f:
            stream = BytesIO(f.read())
        valid_file = werkzeug.datastructures.FileStorage(
            stream=stream,
            filename=valid_attachment_name,
            content_type=mimetype,
        )
        name = "Julia"
        response = anon_client.post(
                        form.url,
                        data = {
                            "text-1620232883208": name,
                            "file-1622045746136": valid_file,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- thank_you_page -->" in html
        initial_answers_count = form.answers.count()
        initial_log_count = form.log.count()
        response = editor_client.get(
                        f"/form/{form.id}/delete-all-answers",
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert f'<div class="title_3">{initial_answers_count}</div>' in html
        assert os.path.isdir(form.get_attachment_dir()) is True
        response = editor_client.post(
                        f"/form/{form.id}/delete-all-answers",
                        data = {
                            "totalAnswers": initial_answers_count,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<div class="success flash_message">' in html
        assert '<!-- list_answers_page -->' in html
        assert form.answers.count() == 0
        assert form.log.count() == initial_log_count + 1
        assert os.path.isdir(form.get_attachment_dir()) is False

    @pytest.mark.skip(reason="Unsure")
    def test_submit_invalid_attachment(self, anon_client):
        form = self.properties['form']
        form.enabled = True
        form.save()
        name = "Stella"
        invalid_attachment_name = "invalid_attachment.ods"
        invalid_attachment_path = f"./assets/{invalid_attachment_name}"
        mimetype = mimetypes.guess_type(invalid_attachment_path)[0]
        with open(invalid_attachment_path, 'rb') as f:
            stream = BytesIO(f.read())
        file = werkzeug.datastructures.FileStorage(
            stream=stream,
            filename=invalid_attachment_name,
            content_type=mimetype,
        )
        response = anon_client.post(
                        form.url,
                        data = {
                            "text-1620232883208": name,
                            "file-1622045746136": file,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- thank_you_page -->" in html
        answer = form.answers[-1]
        assert vars(answer)['data']['text-1620232883208'] == name
        attachment = AnswerAttachment.find(form_id=form.id,
                                           answer_id=vars(answer)['id'])
        assert attachment is None
