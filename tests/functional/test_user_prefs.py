"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from liberaforms.models.user import User
from liberaforms.utils import validators
import flask_login
from tests import user_creds
from tests.utils import login, is_bool


class TestUserPreferences():

    def test_change_language(self, anon_client, editor_client):
        """ Tests unavailable language and available language
            as defined in ./liberaforms/config.py
            Tests permission
        """
        login(editor_client, user_creds['editor'])
        url = "/user/change-language"
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        response = editor_client.get(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- change_language_page -->' in html
        initial_language = flask_login.current_user.preferences['language']
        unavailable_language = 'af' # Afrikaans
        response = editor_client.post(
                        url,
                        data = {
                            "language": unavailable_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert flask_login.current_user.preferences['language'] == initial_language
        available_language = 'ca'
        response = editor_client.post(
                        url,
                        data = {
                            "language": available_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert flask_login.current_user.preferences['language'] == available_language


    def test_change_password(self,  anon_client, editor_client):
        """ Tests invalid and valid password
            as defined in ./liberaforms/utils/validators.py
            Tests permission
        """
        url = "/user/reset-password"
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        response = editor_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- reset_password_page -->' in html
        initial_password_hash = flask_login.current_user.password_hash
        invalid_password="1234"
        response = editor_client.post(
                        url,
                        data = {
                            "password": invalid_password,
                            "password2": invalid_password
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert flask_login.current_user.password_hash == initial_password_hash
        valid_password="this is another valid password"
        response = editor_client.post(
                        url,
                        data = {
                            "password": valid_password,
                            "password2": valid_password
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert flask_login.current_user.password_hash != initial_password_hash
        # reset the password to the value defined in ./tests/test.env. Required by other tests
        flask_login.current_user.password_hash = initial_password_hash
        flask_login.current_user.save()


    @pytest.mark.skip(reason="No way of currently testing this")
    def test_change_email(self, client):
        """ Not impletmented """



    def test_toggle_new_answer_default_notification(self, anon_client, editor_client):
        """ Tests permission
            Tests toggle bool
        """
        url = "/user/toggle-new-answer-notification"
        response = anon_client.post(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert flask_login.current_user.is_anonymous
        login(editor_client, user_creds['editor'])
        initial_default = flask_login.current_user.preferences["newAnswerNotification"]
        response = editor_client.post(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert flask_login.current_user.preferences["newAnswerNotification"] != initial_default
        assert is_bool(flask_login.current_user.preferences["newAnswerNotification"])
