"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import werkzeug
from io import BytesIO
import mimetypes
from flask import current_app
from factories import UserFactory
import flask_login
from liberaforms.models.user import User
from liberaforms.models.media import Media
from liberaforms.commands.user import create as create_user
from tests.utils import login, logout
from tests import user_creds, VALID_PASSWORD
from pprint import pprint

class TestUserMedia():
    """user uploads, downloads and deletes Media.

    Test permissions with anonymous client.
    """
    def setup_class(self):
        self.user = UserFactory()
        self.user.save()

    def setup_method(self):
        # setup_method called for every method
        pass

    def test_media_page(self, editor_client, anon_client):
        """ Tests list media page
            Tests permission
        """
        #print(self.editor.username)
        url = f"/user/{user_creds['editor']['username']}/media"
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        login(anon_client, {'username': self.user.username, 'password': VALID_PASSWORD})
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        logout(anon_client)
        login(editor_client, user_creds['editor'])
        assert flask_login.current_user.uploads_enabled is True
        response = editor_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        #print(html)
        assert '<!-- list_media_page -->' in html

    def test_vaild_media_upload(self, editor_client, anon_client):
        """ Tests media upload
            Tests permission
        """
        url = "/media/save"
        valid_media_name = "valid_media.png"
        valid_media_path = f"./assets/{valid_media_name}"
        with open(valid_media_path, 'rb') as f:
            stream = BytesIO(f.read())
        mimetype = mimetypes.guess_type(valid_media_path)
        response = anon_client.post(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        login(editor_client, user_creds['editor'])
        file = werkzeug.datastructures.FileStorage(
            stream=stream,
            filename=valid_media_name,
            content_type=mimetype,
        )
        editor = User.find(username=user_creds['editor']['username'])
        initial_media_count = editor.media.count()
        response = editor_client.post(
                        url,
                        data = {
                            'media_file': file,
                            'alt_text': "valid alternative text",
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['media']['file_name'] == valid_media_name
        assert editor.media.count() == initial_media_count + 1
        media = Media.find(id=response.json['media']['id'])
        media_path = os.path.join(current_app.config['MEDIA_DIR'], str(flask_login.current_user.id))
        assert media.does_media_exits() is True
        assert media.does_media_exits(thumbnail=True) is True

    def test_invaild_media_upload(self, editor_client):
        url = "/media/save"
        invalid_media_name = "invalid_media.json"
        invalid_media_path = f"./assets/{invalid_media_name}"
        with open(invalid_media_path, 'rb') as f:
            stream = BytesIO(f.read())
        mimetype = mimetypes.guess_type(invalid_media_path)
        file = werkzeug.datastructures.FileStorage(
            stream=stream,
            filename=invalid_media_name,
            content_type=mimetype,
        )
        login(editor_client, user_creds['editor'])
        editor = User.find(username=user_creds['editor']['username'])
        initial_media_count = editor.media.count()
        response = editor_client.post(
                        url,
                        data = {
                            'file': file,
                            'alt_text': "valid alternative text",
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 406
        assert response.is_json is True
        assert editor.media.count() == initial_media_count

    @pytest.mark.skip(reason="TODO")
    def test_view_media(self, anon_client):
        user = User.find(username=user_creds['editor']['username'])
        media = Media.find_all(user_id=user.id).first()
        url = media.get_url()
        response = anon_client.get(
                        url
                    )


    def test_delete_media(self, app, editor_client, anon_client):
        """ Tests delete media
            Tests Permissions
        """
        initial_total_media = Media.query.count()
        user = User.find(username=user_creds['editor']['username'])
        media = Media.find_all(user_id=user.id).first()
        url = f"/media/delete/{media.id}"
        response = anon_client.post(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        login(anon_client, {'username': self.user.username, 'password': VALID_PASSWORD})
        response = anon_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        assert response.json == 'Denied'
        assert initial_total_media == Media.query.count()
        # attempt media delete with editor user
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert initial_total_media -1 == Media.query.count()
