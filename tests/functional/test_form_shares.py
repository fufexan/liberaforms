"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from flask import g
from liberaforms.models.user import User
from liberaforms.models.formuser import FormUser
from tests.factories import UserFactory
from tests.factories import FormFactory
from tests import user_creds
from tests.utils import login, random_slug

class TestShareForm():

    @classmethod
    def setup_class(cls):
        cls.properties={}
        cls.new_editor = UserFactory(validated_email=True)
        cls.new_editor.save()
        cls.new_reader = UserFactory(validated_email=True)
        cls.new_reader.save()

    def test_requirements(self, editor):
        form=FormFactory(author=editor, slug=random_slug())
        form.save()
        form_user = FormUser(
                user_id = editor.id,
                form_id = form.id,
                is_editor = True,
                notifications = editor.new_form_notifications()
        )
        form_user.save()
        self.properties['editor']=editor
        self.properties['form']=form

    def test_access_shared_forms(self, anon_client, editor_client):
        form_id=self.properties['form'].id
        url = f"/form/{form_id}/share"
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- share_form_page -->' in html
        assert f'"/form/{form_id}/add-editor" method="POST"' in html
        assert f'"/form/{form_id}/add-reader" method="POST"' in html

    def test_add_nonexistent_user_as_editor(self, editor_client):
        form_id=self.properties['form'].id
        initial_log_count = self.properties['form'].log.count()
        nonexistent_user_email = "nonexistent@example.com"
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        f"/form/{form_id}/add-editor",
                        data = {
                            "email": nonexistent_user_email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert f'"/form/{form_id}/add-editor" method="POST"' in html
        assert self.properties['form'].log.count() == initial_log_count

    def test_add_editor(self, editor_client):
        form_id=self.properties['form'].id
        initial_log_count = self.properties['form'].log.count()
        response = editor_client.post(
                        f"/form/{form_id}/add-editor",
                        data = {
                            "email":  self.new_editor.email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- share_form_page -->' in html
        assert self.new_editor.email in html
        assert self.properties['form'].log.count() == initial_log_count + 1
        assert FormUser.find(form_id=self.properties['form'].id,
                             user_id=self.new_editor.id,
                             is_editor=True)

    def test_add_same_editor(self, editor_client):
        form_id=self.properties['form'].id
        initial_log_count = self.properties['form'].log.count()
        response = editor_client.post(
                        f"/form/{form_id}/add-editor",
                        data = {
                            "email":  self.new_editor.email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- share_form_page -->' in html
        assert self.new_editor.email in html
        assert self.properties['form'].log.count() == initial_log_count

    def test_add_nonexistent_user_as_reader(self, editor_client):
        form_id=self.properties['form'].id
        initial_log_count = self.properties['form'].log.count()
        nonexistent_user_email = "nonexistent@example.com"
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        f"/form/{form_id}/add-reader",
                        data = {
                            "email": nonexistent_user_email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert f'"/form/{form_id}/add-editor" method="POST"' in html
        assert self.properties['form'].log.count() == initial_log_count
        assert 'user_not_found_modal' in html
        #assert f'"/form/{form_id}/add-reader" method="POST"' in html

    def test_add_reader(self, editor_client):
        form_id=self.properties['form'].id
        initial_log_count = self.properties['form'].log.count()
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        f"/form/{form_id}/add-reader",
                        data = {
                            "email": self.new_reader.email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- share_form_page -->' in html
        assert self.properties['form'].log.count() == initial_log_count + 1
        assert FormUser.find(form_id=self.properties['form'].id,
                             user_id=self.new_reader.id,
                             is_editor=False)

    def test_add_form_editor_as_reader(self, editor_client):
        form_id=self.properties['form'].id
        initial_log_count = self.properties['form'].log.count()
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        f"/form/{form_id}/add-reader",
                        data = {
                            "email": self.properties['form'].author.email,
                        },
                        follow_redirects=True,
                    )
        assert self.properties['form'].log.count() == initial_log_count

    def test_toggle_restricted_form_access(self, editor_client):
        form_id=self.properties['form'].id
        initial_restriced_access = self.properties['form'].restricted_access
        initial_log_count = self.properties['form'].log.count()
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        f"/form/{form_id}/toggle-restricted-access",
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['restricted'] == True
        assert self.properties['form'].restricted_access != initial_restriced_access
        assert self.properties['form'].log.count() == initial_log_count + 1
