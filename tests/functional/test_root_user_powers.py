"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import uuid
import pytest
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory
from tests.factories import UserFactory
from tests import user_creds
from tests.utils import login, logout, random_slug

class TestRootUserPowers():

    @classmethod
    def setup_class(cls):
        cls.properties={}
        cls.new_author = UserFactory(validated_email=True)
        cls.new_author.save()

    def test_requirements(self, editor):
        form=FormFactory(author=editor, slug=random_slug())
        form.save()
        form_user = FormUser(
                user_id = editor.id,
                form_id = form.id,
                is_editor = True,
                notifications = editor.new_form_notifications()
        )
        form_user.save()
        self.properties['editor']=editor
        self.properties['form']=form


    def test_change_author(self, root_user, anon_client, editor_client, admin_client):
        """ Tests nonexistent username and valid username
            Tests permission
        """
        form_id = self.properties['form'].id
        url = f"/admin/form/{form_id}/change-author"
        response = anon_client.get(
                            url,
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- site_index_page -->' in response.data.decode()
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                            url,
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- site_index_page -->' in response.data.decode()
        logout(editor_client)
        login(admin_client, user_creds['root_user'])
        response = admin_client.get(
                            url,
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- change_author_page -->' in response.data.decode()
        initial_author = self.properties['form'].author
        assert FormUser.find(form_id=self.properties['form'].id,
                             user_id=self.properties['form'].author_id) != None
        nonexistent_username = "nonexistent"
        response = admin_client.post(
                            url,
                            data = {
                                "old_author_username": initial_author.username,
                                "new_author_username": nonexistent_username,
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- change_author_page -->' in html
        assert '<div class="warning flash_message">' in html

        # TODO. Test change author to the alread existing author
        #assert g.current.username != initial_author.username

        response = admin_client.post(
                            url,
                            data = {
                                "old_author_username": initial_author.username,
                                "new_author_username": self.new_author.username,
                            },
                            follow_redirects=True
                        )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<div class="success flash_message">' in html
        assert '<!-- inspect_form_page -->' in html
        assert self.properties['form'].author.id == self.new_author.id
