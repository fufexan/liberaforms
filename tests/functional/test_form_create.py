"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
from flask import g, current_app
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.utils import validators
from tests.utils import login, logout, random_slug
from tests import user_creds
from tests.assets.form_structure import get_form_structure
import flask_login

class TestForm():

    #@classmethod
    #def setup_class(cls):
    #    cls.editor=User.find(username=user_creds['editor']['username'])
    #    cls.form = FormFactory()

    def test_create_form(self, editor_client):
        """ Creates a form with valid data.
            Tests Preview page and saves form
            Tests for a new FormLog
        """
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        "/forms/new",
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<form id="result" method="POST" action="/form/edit" >' in html
        slug = random_slug()
        response = editor_client.post(
                        "/form/edit",
                        data = {
                            "structure": get_form_structure(),
                            "introductionTextMD": "# hello",
                            "slug": slug,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<form action="/form/save" method="post">' in html
        response = editor_client.post(
                        "/form/save",
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        created_form = Form.find(slug=slug)
        assert '<h1>hello</h1>' in created_form.introduction_text['html']
        assert '# hello' in created_form.introduction_text['markdown']
        assert created_form.introduction_text['short_text'] == 'hello'
        assert created_form.author_id == flask_login.current_user.id
        assert created_form.enabled == False
        assert created_form.users.count() == 1
        assert created_form.log.count() == 1
        assert FormUser.find(form_id=created_form.id,
                             user_id=flask_login.current_user.id,
                             is_editor=True)
        html = response.data.decode()
        assert '<!-- inspect_form_page -->' in html


class TestSlugAvailability():
    def test_slug_availability(self, editor_client):
        reserved_slug = current_app.config['RESERVED_SLUGS'][0]
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        "/forms/check-slug-availability",
                        data = {
                            "slug": reserved_slug,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['available'] == False
        unavailable_slug = Form.find().slug
        response = editor_client.post(
                        "/forms/check-slug-availability",
                        data = {
                            "slug": unavailable_slug,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['available'] == False

    def test_preview_save_form_with_unavailable_slug(self, editor_client):
        unavailable_slug = Form.find().slug
        response = editor_client.post(
                        "/form/edit",
                        data = {
                            "structure": get_form_structure(),
                            "introductionTextMD": "hello",
                            "slug": unavailable_slug,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 302
        html = response.data.decode()
        assert '/form/edit</a>' in html
        response = editor_client.post(
                        "/form/save",
                        data = {
                            "structure": get_form_structure(),
                            "introductionTextMD": "hello",
                            "slug": unavailable_slug,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 302
        html = response.data.decode()
        assert '/form/edit</a>' in html

    def test_preview_save_form_with_reserved_slug(self, editor_client):
        reserved_slug = current_app.config['RESERVED_SLUGS'][0]
        response = editor_client.post(
                        "/form/edit",
                        data = {
                            "structure": get_form_structure(),
                            "introductionTextMD": "hello",
                            "slug": reserved_slug,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 302
        html = response.data.decode()
        assert '/form/edit</a>' in html
        #assert '<form action="/form/save" method="post">' in html
        response = editor_client.post(
                        "/form/save",
                        data = {
                            "structure": get_form_structure(),
                            "introductionTextMD": "hello",
                            "slug": reserved_slug,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 302
        html = response.data.decode()
        assert '/form/edit</a>' in html
