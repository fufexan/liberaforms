"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import pytest
from liberaforms.commands.user import create as create_user
from liberaforms.models.user import User
from factories import UserFactory


class TestCLI():

    def test_create_user_via_cli(self, app, users):
        runner = app.test_cli_runner()
        user = UserFactory(validated_email=False)
        with app.app_context():
            result = runner.invoke(create_user, [user.username,
                                                 user.email,
                                                 "a password"
                                                 ])
        assert 'created OK' in result.output
        user = User.find(username=user.username)
        assert user.validated_email == True
        assert user.role == 'editor'
