"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
from tests import user_creds
from tests.utils import login


def test_landing_page(anon_client):
    response = anon_client.get("/")
    assert response.status_code == 200
    html = response.data.decode()
    assert '<div id="blurb" class="marked-up">' in html

def test_template_page(anon_client, editor_client):
    response = anon_client.get(
                    url_for('form_bp.list_templates'),
                    follow_redirects=True)
    assert response.status_code == 200
    html = response.data.decode()
    assert '<!-- user_login_page -->' in html
    login(editor_client, user_creds['editor'])
    response = editor_client.get(
                    url_for('form_bp.list_templates'),
                    follow_redirects=False)
    assert response.status_code == 200
    html = response.data.decode()
    assert '<!-- list_templates_page -->' in html
