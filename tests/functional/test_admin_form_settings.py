"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from liberaforms.models.user import User
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory
from tests import user_creds
from tests.utils import login, logout, random_slug

class TestFormAdminSettings():
    @classmethod
    def setup_class(cls):
        cls.properties={}

    def test_requirements(self, editor, admin):
        form=FormFactory(author=editor, slug=random_slug())
        form.save()
        form_user = FormUser(
                user_id = editor.id,
                form_id = form.id,
                is_editor = True,
                notifications = editor.new_form_notifications()
        )
        form_user.save()
        self.properties['editor']=editor
        self.properties['form']=form

    def test_disable_form(self, anon_client, editor_client, admin_client):
        """ Tests disable form
            Test permissions
        """
        form_id = self.properties['form'].id
        url = f"/admin/form/{form_id}/toggle-public"
        response = anon_client.get(
                            url,
                            follow_redirects=True
                        )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                            url,
                            follow_redirects=True
                        )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        logout(editor_client)
        login(admin_client, user_creds['admin'])
        response = admin_client.get(
                            url,
                            follow_redirects=True
                        )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- inspect_form_page -->' in html
        assert self.properties['form'].admin_preferences['public'] == False
        assert self.properties['form'].is_public() == False
        response = anon_client.get(
                            f"/{self.properties['form'].slug}",
                            follow_redirects=True
                        )
        assert response.status_code == 404
        html = response.data.decode()
        assert '<!-- page_not_found_404 -->' in html
        self.properties['form'].admin_preferences['public'] = True
        self.properties['form'].save()
