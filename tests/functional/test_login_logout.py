"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import flask_login
from tests.utils import login, logout


class TestLoginLogout():
    def test_login_page(cls, anon_client):
        response = anon_client.get(
                        "/user/login",
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- user_login_page -->' in html

    def test_invalid_login(cls, anon_client):
        response = anon_client.post(
                        "/user/login",
                        data = {
                            "username": "",
                            "password": "",
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- user_login_page -->' in html

    def test_valid_login(self, admin, users, anon_client):
        response = anon_client.post(
                        "/user/login",
                        data = {
                            "username": users['admin']['username'],
                            "password": users['admin']['password'],
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert flask_login.current_user.is_authenticated
        assert flask_login.current_user.username == admin.username
        html = response.data.decode()
        assert "<!-- list_templates_page -->" in html
        assert '<a class="nav-link" href="/user/logout">' in html

    def test_logout(self, anon_client):
        response = anon_client.post(
                        "/user/logout",
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert not flask_login.current_user.is_authenticated
        assert '<!-- site_index_page -->' in html
        assert '<a class="nav-link" href="/user/login">' in html
