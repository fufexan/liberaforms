"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
from flask import url_for
import flask_login
from liberaforms.models.site import Site
from tests import user_creds
from tests.utils import login, random_slug


class TestSiteConsent():
    """Test the site's data consent."""

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()

    def test_terms_conditions_page(self, anon_client, editor_client, admin_client):
        response = anon_client.get(
                        url_for('site_bp.edit_terms_and_conditions'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        url_for('site_bp.edit_terms_and_conditions'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        login(admin_client, user_creds['admin'])
        response = admin_client.get(
                        url_for('site_bp.edit_terms_and_conditions'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        terms_and_conditions:dict = self.site.terms_and_conditions
        assert terms_and_conditions['html'] in html
        assert terms_and_conditions['enabled'] is False

    def test_save_consent_text(self, session, admin_client):
        login(admin_client, user_creds['admin'])
        response = admin_client.post(
                        url_for('site_bp.edit_terms_and_conditions'),
                        data = {
                            "markdown": "# Terms!",
                            "checkbox_text": "check box text!"
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<div class="success flash_message">' in html
        terms_and_conditions:dict = self.site.terms_and_conditions
        assert terms_and_conditions['html'] == "<h1>Terms!</h1>"
        assert terms_and_conditions['html'] in html
        assert terms_and_conditions['checkbox_text'] == "check box text!"
        assert terms_and_conditions['checkbox_text'] in html

    def test_toggle_enabled(self, admin_client):
        terms_and_conditions:dict = self.site.terms_and_conditions
        assert terms_and_conditions['enabled'] is False
        response = admin_client.get(
                        url_for('site_bp.preview_new_user_form'),
                        follow_redirects=False,
                    )
        html = response.data.decode()
        assert terms_and_conditions['html'] not in html
        response = admin_client.post(
                        url_for('site_bp.toggle_terms_and_conditions'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        terms_and_conditions:dict = self.site.terms_and_conditions
        assert terms_and_conditions['enabled'] is True
        response = admin_client.get(
                        url_for('site_bp.preview_new_user_form'),
                        follow_redirects=False,
                    )
        html = response.data.decode()
        assert terms_and_conditions['html'] in html
