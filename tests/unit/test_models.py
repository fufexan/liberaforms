"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from factories import UserFactory
from liberaforms.models.site import Site
from liberaforms.models.user import User
from liberaforms.utils import validators


def test_new_user(db):
    new_user=UserFactory()
    assert new_user.role == 'editor'
    assert new_user.validated_email is False
    assert new_user.uploads_enabled == Site.find().newuser_enableuploads


"""
def test_factory_fixture(user_factory):
    user = user_factory(
        username = "charles-dickens",
        email = "person@example.com",
        password = "This is a secret password",
        role = 'admin',
        preferences = User.default_user_preferences(),
        blocked = False,
        admin = User.default_admin_settings(),
        validated_email = True,
        uploads_enabled = False,
        uploads_limit = "10 KB"
    )
    assert user.username == "charles-dickens"
    assert validators.verify_password("This is a secret password", user.password_hash) is True

def test_user_factory(user_factory):
    #Factories become fixtures automatically.
    print(type(user_factory), type(UserFactory))
    assert isinstance(user_factory, UserFactory)

"""
