"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import uuid
import json
import flask_login

def login(client, credentials):
    response = client.post('/user/login', data=dict(
        username=credentials['username'],
        password=credentials['password']
    ), follow_redirects=True)
    assert flask_login.current_user.username == credentials['username']
    return response

def logout(client):
    response = client.post('/user/logout', follow_redirects=True)
    assert flask_login.current_user.is_anonymous == True
    return response

def is_bool(value) -> bool:
    return type(value) == type(bool())

def random_slug():
    return str(uuid.uuid4())

def get_ldap_users() -> dict:
    users = {}
    entries_file = "./assets/ldap/entries.json"
    with open(entries_file, "r") as read_file:
        entries = json.load(read_file)
    entry_cnt=0
    for entry in entries['entries']:
        attributes = entry["attributes"]
        if "inetOrgPerson" in attributes["structuralObjectClass"] \
            and not attributes["entryDN"] == os.environ['LDAP_BIND_ACCOUNT']:
            data = {
                "username": attributes["uid"][0],
                "email": attributes["mail"][0],
                "password": attributes["userPassword"][0]
            }
            users[entry_cnt] = data
            entry_cnt += 1
    return users

#def populate_ldap_mock_server() -> None:
#    """Connect to a real LDAP server and download config into ./assets/ldap
#       This function is not used. It's just here if we need to repopulate the assets
#    """
#    import ldap3
#    REAL_SERVER="ldap://127.0.0.1:1666"
#    REAL_USER="cn=nobody,dc=example,dc=com"
#    REAL_PASS="hello"
#    server = ldap3.Server(REAL_SERVER, get_info=ldap3.ALL)
#    connection = ldap3.Connection(server, REAL_USER, REAL_PASS, auto_bind=True)
#    server.info.to_file('./assets/ldap/server_info.json')
#    server.schema.to_file('./assets/ldap/server_schema.json')
#    if connection.search('dc=example,dc=com', '(objectclass=*)',
#                          attributes=ldap3.ALL_ATTRIBUTES,
#                          get_operational_attributes=True):
#        connection.response_to_file('./assets/ldap/entries.json', raw=True)
#    connection.unbind()
