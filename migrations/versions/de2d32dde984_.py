"""Adds Consent table

Revision ID: de2d32dde984
Revises: df8ed4b3992b
Create Date: 2021-12-15 13:21:46.501140

"""
from datetime import datetime, timezone
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from sqlalchemy.dialects.postgresql import JSONB, ARRAY, ENUM, TIMESTAMP
from sqlalchemy.orm.session import Session
from sqlalchemy.orm import declarative_base
from liberaforms.utils import utils

# revision identifiers, used by Alembic.
revision = 'de2d32dde984'
down_revision = '4a59772da623'
branch_labels = None
depends_on = None

Base = declarative_base()

class Site(Base):
    __tablename__ = "site"
    id = sa.Column(sa.Integer, primary_key=True, index=True)
    consentTexts = sa.Column(ARRAY(JSONB), nullable=False)
    terms_and_conditions = sa.Column(JSONB, nullable=False)

class Form(Base):
    __tablename__ = "forms"
    id = sa.Column(sa.Integer, primary_key=True, index=True)
    consentTexts = sa.Column(ARRAY(JSONB), nullable=False)

class User(Base):
    __tablename__ = "users"
    id = sa.Column(sa.Integer, primary_key=True, index=True)


class Consent(Base):
    __tablename__ = "consents"
    id = sa.Column(sa.Integer, primary_key=True, index=True)
    created = sa.Column(TIMESTAMP, nullable=False)
    is_dpl = sa.Column(sa.Boolean, default=True, nullable=False)
    title = sa.Column(sa.String, default="", nullable=False)
    markdown = sa.Column(sa.String, default="", nullable=False)
    html = sa.Column(sa.String, default="", nullable=False)
    checkbox_text = sa.Column(sa.String, default="", nullable=False)
    required = sa.Column(sa.Boolean, default=True, nullable=False)
    enabled = sa.Column(sa.Boolean, default=False, nullable=False)
    shared = sa.Column(sa.Boolean, default=False, nullable=False)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('site.id'), unique=True, nullable=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    form_id = sa.Column(sa.Integer, sa.ForeignKey('forms.id'), nullable=True)

    def __init__(self, **kwargs):
        self.created = datetime.now(timezone.utc)
        for key, value in kwargs.items():
            setattr(self, key, value)


def upgrade():
    op.create_table('consents',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created', postgresql.TIMESTAMP(), nullable=False),
    sa.Column('is_dpl', sa.Boolean(), default=True, nullable=False),
    sa.Column('title', sa.String(), default="", nullable=False),
    sa.Column('markdown', sa.String(), default="", nullable=False),
    sa.Column('html', sa.String(), default="", nullable=False),
    sa.Column('checkbox_text', sa.String(), default="", nullable=False),
    sa.Column('required', sa.Boolean(), default=True, nullable=False),
    sa.Column('enabled', sa.Boolean(), default=False, nullable=False),
    sa.Column('shared', sa.Boolean(), default=False, nullable=False),
    sa.Column('site_id', sa.Integer(), unique=True, nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('form_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['form_id'], ['forms.id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['site_id'], ['site.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('site_id')
    )
    op.create_index(op.f('ix_consents_id'), 'consents', ['id'], unique=False)
    op.add_column('site', sa.Column('terms_and_conditions', postgresql.JSONB(astext_type=sa.Text()), nullable=True))

    session = Session(bind=op.get_bind())
    site = session.query(Site).first()
    if site:
        for content_text in site.consentTexts:
            if content_text['name'] == 'DPL':
                kwargs = {
                    'site_id': site.id,
                    'title': content_text['name'],
                    'markdown': content_text['markdown'],
                    'html': content_text['html'],
                    'checkbox_text': content_text['label'],
                    'shared': content_text['enabled'],
                    'enabled': content_text['enabled']
                }
                consent = Consent(**kwargs)
                session.add(consent)
            elif content_text['name'] == 'terms':
                site.terms_and_conditions = {
                    'markdown': content_text['markdown'],
                    'html': content_text['html'],
                    'checkbox_text': content_text['label'],
                    'enabled': content_text['enabled'],
                    'required': True
                }
    forms = session.query(Form).all()
    for form in forms:
        if form.consentTexts:
            for content_text in form.consentTexts:
                kwargs = {
                    'form_id': form.id,
                    'title': content_text['name'],
                    'markdown': content_text['markdown'] if content_text['markdown'] else "",
                    'html': content_text['html'] if content_text['html'] else "",
                    'checkbox_text': content_text['label'] if content_text['label'] else "",
                    'enabled': content_text['enabled'],
                    'shared': False
                }
                consent = Consent(**kwargs)
                session.add(consent)
    session.commit()
    session.close()
    op.drop_column('forms', 'consentTexts')
    op.drop_column('site', 'consentTexts')
    op.drop_column('site', 'newUserConsentment')
    op.alter_column('site', 'terms_and_conditions', nullable=False)


def downgrade():
    op.add_column('forms', sa.Column('consentTexts', postgresql.ARRAY(postgresql.JSONB(astext_type=sa.Text())),
                                     autoincrement=False, nullable=True))
    op.add_column('site', sa.Column('consentTexts', postgresql.ARRAY(postgresql.JSONB(astext_type=sa.Text())),
                                    autoincrement=False, nullable=True))
    op.add_column('site', sa.Column('newUserConsentment', postgresql.JSONB(astext_type=sa.Text()),
                                    autoincrement=False, nullable=True))

    TandC_id = utils.gen_random_string()
    DPL_id = utils.gen_random_string()
    session = Session(bind=op.get_bind())
    site = session.query(Site).one()
    site_TandC = site.terms_and_conditions
    site_DPL = session.query(Consent).filter_by(site_id=site.id).one()
    consent = [
        {
            'id': TandC_id,
            'enabled': site_TandC['enabled'],
            'markdown': site_TandC['markdown'],
            'html': site_TandC['html'],
            'required': True,
            'label': site_TandC['checkbox_text'],
            'name': "terms"
        },
        {
            'id': DPL_id,
            'enabled': site_DPL.enabled,
            'markdown': site_DPL.markdown,
            'html': site_DPL.html,
            'required': True,
            'label': site_DPL.checkbox_text,
            'name': "DPL"
        },
    ]
    site.consentTexts = consent
    forms = session.query(Form).all()
    for form in forms:
        form_DPL = session.query(Consent).filter_by(form_id=form.id).first()
        if form_DPL:
            consent = [
                {
                    'id': DPL_id,
                    'enabled': form_DPL.enabled,
                    'markdown': form_DPL.markdown,
                    'html': form_DPL.html,
                    'required': form_DPL.required,
                    'label': form_DPL.checkbox_text,
                    'name': form_DPL.title
                }
            ]
            form.consentTexts = consent
    session.commit()
    session.close()
    op.alter_column('site', 'consentTexts', nullable=False)
    op.drop_index(op.f('ix_consents_id'), table_name='consents')
    op.drop_table('consents')
    op.drop_column('site', 'terms_and_conditions')
