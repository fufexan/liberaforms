# Mutli tenant

> If you are using the LiberaForms docker cluster, please refer to that documentation instead.

A multi tenant setup enables you to serve various LiberaForms instances,
each with their own domain name (FQDN).

This docmentation will help with a set up like this.

* One PostgreSQL server
* One nginx server
* Various LiberaForms servers.

## PostgreSQL

You need to install a postgres server. That server will host multiple databases.
One database for each LiberaForms installation.

## LiberaForms

You may wish to create a directory where you will install each installation in a subdirectory
eg:

```
/opt/liberaforms
/opt/liberaforms/tenant0
/opt/liberaforms/tenant1
/opt/liberaforms/tenant2
```

> `tenant0` is not an easy name to identifiy. You can use a name that is easier to remember (maybe the tenant's name)

## Gunicorn

Each LiberaForms installation must use a different port.
By default the port is `5000`

```
flask config hint gunicorn
```

As you see, the port is `5000`. Change this for each installation.
For example.

```
bind = '127.0.0.1:5000' # tenant0
bind = '127.0.0.1:5001' # tenant1
bind = '127.0.0.1:5002' # tenant2
```

## Nginx

You need to install an Nginx web server. It will act as a proxy.
Each LiberaForms installtion requires a `VHOST` You must adjust each VHOST configuration.

Change all the `paths` to match the tentants' installation directory.

See `nginx.example`


Make sure the port number matches the tentant's gunicorn port number.
```
proxy_pass  http://127.0.0.1:5000;
```

## .env

Edit the `.env` of each LiberaForms' installation.

* Each tenant requires a unique database

```
DB_NAME=my_tenant_com
```

And shoud have a unique database user and password
