# Command line utilities

> Command lines utilities required to install LiberaForms are mentioned in the `docs/INSTALL.md`

## Users

You can create a user when needed.

Note that the emails of users created via the command line do not require validation.

```
flask user create <username> <email> <password> -role=[guest|editor|admin]
```

Disable and enable users.

```
flask user disable <username>
flask user enable <username>
```

Set user properties

```
flask user set <username> -password= -email=
```

## SMTP

Configure SMTP

```
-host: Your SMTP server's name (required)
-port: SMTP port (required)
-user: SMTP username
-password: SMTP password
-encryption: [ SSL | STARTTLS ]
-noreply: Emails will be sent using this address (required)
```

```
flask smtp set -host=localhost -port=25 -noreply=no-reply@domain.com
```

Note: If your password contains the special characters you will need to wrap your password in single quotes.
```
-password='my!password'
```

View SMTP config
```
flask smtp get
```

Test SMTP config
```
flask smtp test -recipent=me@my.domain.com
```

## Disk usage

Returns the total size of uploaded files
```
flask storage usage
```

## Site config

Change params used to create urls. Optionally, define a port number
```
flask site set -hostname=my.domain.com -scheme=[http|https] -port=8080
```
