# LDAP integration

LiberaForms can query an LDAP server to login users. LiberaForms does not submit changes to the DIT.

To enable LDAP integration use these variables in the `.env` file

* **ENABLE_LDAP**: Enable LDAP integration. Default `False`
* **LDAP_SERVER**: Your LDAP server. Default `ldap://localhost`
* **LDAP_ANONYMOUS_BIND**: The server accepts anonymous binds for searching. Default `False`
* **LDAP_BIND_ACCOUNT**: A bind account used for searching. Optional. Default `"cn=nobody,dc=example,dc=com"`
* **LDAP_BIND_PASSWORD**: The bind account password. Optional.
* **LDAP_USER_DN_LIST**: A list of DNs where users can be found. Default `["uid=%uid,ou=users,o=company,dc=example,dc=com"]`
* **LDAP_SEARCH_BASE_DN**: Filter searches begin here. Default `"o=company,dc=example,dc=com"`
* **LDAP_FILTER**: The criteria to find LiberaForms users. Default `"(&(objectclass=inetOrgPerson)(|(uid=%uid)(mail=%uid)))"`
* **LDAP_MAIL_ATTRIB**: The RDN you use for the user's email address attribute. Default `mail`
* **LDAP_RECOVER_PASSWD_URL**: A web service you provide to your users to recover passwords. Optional. Default `https://example.com/recover-your-ldap-password`

## LDAP server config requirements

* User entries in the DIT must have an email address attribute.
* LDAP users must be able to perfom searches on the DIT.

## Users

LiberaForms can login a user using LDAP account credentials. When an LDAP account is authenticated and authorized for the first time, LiberaForms will:

1. Create a new LiberaForms User with these values:
  * username: The DN's first value, typically being the uid value.
  * password: Encrypts the submitted password with PBKDF2. see `liberaforms/utils/validators.py:hash_password()`
  * email: The entry's `LDAP_MAIL_ATTRIB` value.
  * default user values.
2. Login the user

## Authentification

Authentification means both the %uid and password are correct. LiberaForms will:

1. Iterate through the `LDAP_USER_DN_LIST`, replacing each `%uid` with the submitted value.
2. Attempt to bind with each DN using the submitted password.
3. Use the first successful bind to authenticate the user.

People often use their email address to login to websites. When an email is submitted, LiberaForms will:

1. Use the `LDAP_FILTER` to search for the email address.
2. If an entry is retrieved, a bind attempt is made with the entry's DN and submitted password.

`LDAP_ANONYMOUS_BIND` or `LDAP_BIND_ACCOUNT` is required for email logins.

When the `LDAP_SERVER` is unreachable, let's say because of a network issue, LiberaForms will validate the credentials against the  iberaForms database.

## LDAP_FILTER

The `LDAP_FILTER` is used to search for entries in the DIT. See [LDAP filter syntax](https://ldap3.readthedocs.io/en/latest/tutorial_searches.html#finding-entries).

LiberaForms uses the filter to:
* Search for the email address (when the user has attempted a login using an email address).
* Authorize the user to login to LiberaForms.

This group of assertions must be included in your `LDAP_FILTER`. Adjust `uid` and `mail` to suit your configuration.
```
(|(uid=%uid)(mail=%uid))
```

## Anonymous / _nobody_

Anonymous login is important but not required. When the anonymous or _nobody_ account is not enabled:

* The %uid must used to login LDAP users. The email address cannot be used.
*

## Testing

Edit `.env` to configure. Remember to restart Flask after each change you make.

The LDAP configuration can be tested from the Admin page and includes:

* Test Anonymous and _nobody_ bindings.
* Test binding users with the `LDAP_USER_DN_LIST`
* Test, and edit, the `LDAP_FILTER`. Changes are saved to the database and it is not required to restart Flask.
