"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import g, current_app
from liberaforms.models.answer import AnswerAttachment
from liberaforms.models.media import Media
from liberaforms.utils.dispatcher.dispatcher import Dispatcher
from liberaforms.utils import utils


class StorageDomain():

    @staticmethod
    def total_media_usage(user_id=None, human_readable=False):
        kwargs = {'user_id': user_id} if user_id else {}
        total = Media.calc_total_size(**kwargs)
        return utils.human_readable_bytes(total) if human_readable else total

    @staticmethod
    def total_attachments_usage(author_id=None, human_readable=False):
        kwargs = {}
        if author_id:
            kwargs['author_id'] = author_id
        total = AnswerAttachment.calc_total_size(**kwargs)
        return utils.human_readable_bytes(total) if human_readable else total

    @staticmethod
    def total_disk_usage(human_readable=False):
        total = Media.calc_total_size() + AnswerAttachment.calc_total_size()
        return utils.human_readable_bytes(total) if human_readable else total

    @staticmethod
    def default_user_uploads_limit(human_readable=False):
        total = current_app.config['DEFAULT_USER_UPLOADS_LIMIT']
        return utils.string_to_bytes(total) if not human_readable else total

    @staticmethod
    def total_disk_limit(human_readable=False):
        total = current_app.config['TOTAL_UPLOADS_LIMIT']
        return utils.string_to_bytes(total) if not human_readable else total

    @staticmethod
    def human_readable_bytes(bytes):
        return utils.human_readable_bytes(bytes)


    """
    Returns disk usage msg. Optionally sends alert via email.
    """
    @staticmethod
    def disk_usage_message(alert=False):
        total_usage = StorageDomain.total_disk_usage()
        total_limit = StorageDomain.total_disk_limit()
        msg = f"Total disk usage: {utils.human_readable_bytes(total_usage)}"
        if alert and total_usage > total_limit:
            emails = [current_app.config['ROOT_USER']]
            for email in current_app.config['ALERT_MAILS']:
                if email not in emails:
                    emails.append(email)
            Dispatcher().send_message(emails, "Disk usage alert", msg)
        return msg
