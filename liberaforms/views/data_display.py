"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from sqlalchemy.orm.attributes import flag_modified
from flask import Blueprint, request, redirect, jsonify, url_for
from flask_babel import gettext as _
import flask_login
from liberaforms.models.form import Form
from liberaforms.models.user import User
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer
from liberaforms.models.schemas.form import FormSchemaForMyFormsDataDisplay, \
                                            FormSchemaForAdminFormsDataDisplay
from liberaforms.models.schemas.user import UserSchemaForAdminDataDisplay
from liberaforms.models.schemas.answer import AnswerSchema
from liberaforms.utils.utils import human_readable_bytes
from liberaforms.utils import validators
from liberaforms.utils import auth

#from pprint import pprint

data_display_bp = Blueprint('data_display_bp', __name__)


## Admin Forms list

default_admin_forms_field_index = [
                {'name': 'form_name__html', 'label': 'Name'},
                {'name': 'total_answers', 'label': 'Answers'},
                {'name': 'last_answer_date', 'label': 'Last answer'},
                {'name': 'author__html', 'label': 'Author'},
                {'name': 'total_users', 'label': 'Users'},
                {'name': 'is_public', 'label': 'Public'},
                {'name': 'created', 'label': 'Created'}
            ]

def get_admin_forms_field_index(user):
    if 'field_index' in user.admin['forms']:
        return user.admin['forms']['field_index']
    else:
        return default_admin_forms_field_index

def get_admin_forms_ascending(user):
    if 'ascending' in user.admin['forms']:
        #print(user.admin['forms']['ascending'])
        return user.admin['forms']['ascending']
    else:
        return True

def get_admin_forms_order_by(user):
    if 'order_by' in user.admin['forms']:
        return user.admin['forms']['order_by']
    else:
        return 'created'


@data_display_bp.route('/data-display/admin/forms', methods=['GET'])
@auth.enabled_admin_required__json
def admin_forms():
    """ Returns json required by Vue dataDisplay component.
    """
    forms = Form.find_all()
    items = []
    for form in FormSchemaForAdminFormsDataDisplay(many=True).dump(forms):
        item = {}
        data = {}
        slug = form['slug']
        data['form_name__html'] = {
            'value': slug,
            'html': f"<a href='/form/{form['id']}'>{slug}</a>"
        }
        data['author__html'] = {
            'value': form['author']['name'],
            'html': f"<a href='/admin/user/{form['author']['id']}'>{form['author']['name']}</a>"
        }
        for field_name in form.keys():
            if field_name == 'slug':
                continue
            if field_name == 'id':
                item[field_name] = form[field_name]
                continue
            if field_name == 'created':
                item[field_name] = form[field_name]
                continue
            data[field_name] = form[field_name]
        item['data'] = data
        items.append(item)
    return jsonify(
        items=items,
        meta={'name': 'All forms',
              'deleted_fields': [],
              'default_field_index': default_admin_forms_field_index,
              'editable_fields': False,
              'item_endpoint': None,
              'can_edit': False,
              'enable_notification': False,
              'min_column_width': 150,
        },
        user_prefs={'field_index': get_admin_forms_field_index(flask_login.current_user),
                    'order_by': get_admin_forms_order_by(flask_login.current_user),
                    'ascending': get_admin_forms_ascending(flask_login.current_user)
        }
    ), 200

@data_display_bp.route('/data-display/admin/forms/change-index', methods=['POST'])
@auth.enabled_admin_required__json
def admin_forms_change_field_index():
    """Change Admin's forms field index preference."""
    data = request.get_json(silent=True)
    if not 'field_index' in data:
        return jsonify("Not Acceptable"), 406
    field_index = data['field_index']
    if validators.compare_field_index(field_index, default_admin_forms_field_index):
        flask_login.current_user.admin['forms']['field_index'] = field_index
        flag_modified(flask_login.current_user, 'admin')
        flask_login.current_user.save()
    else:
        field_index = default_admin_forms_field_index
    return jsonify(field_index=field_index), 200

@data_display_bp.route('/data-display/admin/forms/reset-index', methods=['POST'])
@auth.enabled_admin_required__json
def admin_reset_forms_field_index():
    """Reset Admin's Form field index preference."""
    flask_login.current_user.admin['forms']['field_index'] = default_admin_forms_field_index
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(field_index=flask_login.current_user.admin['forms']['field_index']), 200

@data_display_bp.route('/data-display/admin/forms/order-by-field', methods=['POST'])
@auth.enabled_admin_required__json
def order_admin_forms_by_field():
    """Set Admin's forms order_by preference."""
    data = request.get_json(silent=True)
    if not 'order_by_field_name' in data:
        return jsonify("Not Acceptable"), 406
    field_names = [ field['name'] for field in default_admin_forms_field_index ]
    if not data['order_by_field_name'] in field_names:
        return jsonify("Not Acceptable"), 406
    flask_login.current_user.admin['forms']['order_by'] = data['order_by_field_name']
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(order_by_field_name=flask_login.current_user.admin['forms']['order_by']), 200

@data_display_bp.route('/data-display/admin/forms/toggle-ascending', methods=['POST'])
@auth.enabled_admin_required__json
def admin_forms_toggle_ascending():
    """Toggle Admin's forms ascending order preference."""
    preference = get_admin_forms_ascending(flask_login.current_user)
    flask_login.current_user.admin['forms']['ascending'] = not preference
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(ascending=flask_login.current_user.admin['forms']['ascending']), 200


## Admin Users list

default_admin_users_field_index = [
                {'name': 'username__html', 'label': 'User name'},
                {'name': 'total_forms', 'label': 'Forms'},
                {'name': 'email', 'label': 'Email'},
                {'name': 'enabled', 'label': 'Enabled'},
                {'name': 'role', 'label': 'Role'},
                {'name': 'disk_usage__html', 'label': 'Disk usage'},
                {'name': 'created', 'label': 'Created'}
            ]

def get_admin_users_field_index(user):
    if 'field_index' in user.admin['users']:
        return user.admin['users']['field_index']
    return default_admin_users_field_index

def get_admin_users_ascending(user):
    if 'ascending' in user.admin['users']:
        return user.admin['users']['ascending']
    return True

def get_admin_users_order_by(user):
    if 'order_by' in user.admin['users']:
        return user.admin['users']['order_by']
    return 'created'

@data_display_bp.route('/data-display/admin/users', methods=['GET'])
@auth.enabled_admin_required__json
def admin_users():
    """Return json required by Vue dataDisplay component."""
    users = User.find_all()
    items = []
    for user in UserSchemaForAdminDataDisplay(many=True).dump(users):
        item = {}
        data = {}
        data['username__html'] = {
            'value': user['username'],
            'html': f"<a href='/admin/user/{user['id']}'>{user['username']}</a>"
        }
        data['disk_usage__html'] = {
            'value': user['disk_usage'],
            'html': human_readable_bytes(user['disk_usage'])
        }
        for field_name in user.keys():
            if field_name == 'username' or field_name == 'disk_usage':
                continue
            if field_name == 'id' or field_name == 'created':
                item[field_name] = user[field_name]
                continue
            data[field_name] = user[field_name]
        item['data'] = data
        items.append(item)
    return jsonify(
        items=items,
        meta={'name': 'Users',
              'deleted_fields': [],
              'default_field_index': default_admin_users_field_index,
              'editable_fields': False,
              'item_endpoint': None,
              'can_edit': False,
              'enable_notification': False,
              'min_column_width': 150,
        },
        user_prefs={'field_index': get_admin_users_field_index(flask_login.current_user),
                    'order_by': get_admin_users_order_by(flask_login.current_user),
                    'ascending': get_admin_users_ascending(flask_login.current_user)
        }
    ), 200

@data_display_bp.route('/data-display/admin/users/change-index', methods=['POST'])
@auth.enabled_admin_required__json
def users_change_field_index():
    """Change User's forms field index preference."""
    data = request.get_json(silent=True)
    if not 'field_index' in data:
        return jsonify("Not Acceptable"), 406
    field_index = data['field_index']
    if validators.compare_field_index(field_index, default_admin_users_field_index):
        flask_login.current_user.admin['users']['field_index'] = field_index
        flag_modified(flask_login.current_user, 'admin')
        flask_login.current_user.save()
    else:
        field_index = default_admin_users_field_index
    return jsonify(field_index=field_index), 200

@data_display_bp.route('/data-display/admin/users/reset-index', methods=['POST'])
@auth.enabled_admin_required__json
def users_reset_field_index():
    """Reset User's field index preference."""
    flask_login.current_user.admin['users']['field_index'] = default_admin_users_field_index
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(field_index=flask_login.current_user.admin['users']['field_index']), 200

@data_display_bp.route('/data-display/admin/users/order-by-field', methods=['POST'])
@auth.enabled_admin_required__json
def users_order_by_field():
    """Set User's forms order_by preference."""
    data = request.get_json(silent=True)
    if not 'order_by_field_name' in data:
        return jsonify("Not Acceptable"), 406
    field_names = [ field['name'] for field in default_admin_users_field_index ]
    if not data['order_by_field_name'] in field_names:
        return jsonify("Not Acceptable"), 406
    flask_login.current_user.admin['users']['order_by'] = data['order_by_field_name']
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(order_by_field_name=flask_login.current_user.admin['users']['order_by']), 200

@data_display_bp.route('/data-display/admin/users/toggle-ascending', methods=['POST'])
@auth.enabled_admin_required__json
def users_toggle_ascending():
    """Toggle User's forms ascending order preference."""
    preference = get_admin_users_ascending(flask_login.current_user)
    flask_login.current_user.admin['users']['ascending'] = False if preference else True
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(ascending=flask_login.current_user.admin['users']['ascending']), 200


## Admin User's forms list

default_admin_userforms_field_index = [
                {'name': 'form_name__html', 'label': 'Name'},
                {'name': 'total_answers', 'label': 'Answers'},
                {'name': 'last_answer_date', 'label': 'Last answer'},
                {'name': 'is_author', 'label': 'Author'},
                {'name': 'total_users', 'label': 'Users'},
                {'name': 'is_public', 'label': 'Public'},
                {'name': 'created', 'label': 'Created'},
            ]

def get_admin_userforms_field_index(user):
    if 'field_index' in user.admin['userforms']:
        return user.admin['userforms']['field_index']
    return default_admin_userforms_field_index

def get_admin_userforms_ascending(user):
    if 'ascending' in user.admin['userforms']:
        return user.admin['userforms']['ascending']
    return True

def get_admin_userforms_order_by(user):
    if 'order_by' in user.admin['userforms']:
        return user.admin['userforms']['order_by']
    return 'created'

@data_display_bp.route('/data-display/admin/user/<int:user_id>/forms', methods=['GET'])
@auth.enabled_admin_required__json
def admin_userforms(user_id):
    """Returns json required by Vue dataDisplay component."""
    user = User.find(id=user_id)
    forms = user.get_forms()
    items = []
    for form in FormSchemaForAdminFormsDataDisplay(many=True).dump(forms):
        item = {}
        data = {}
        slug = form['slug']
        data['form_name__html'] = {
            'value': slug,
            'html': f"<a href='/form/{form['id']}'>{slug}</a>"
        }
        data['is_author'] = True if form['author_id'] == user.id else False
        for field_name in form.keys():
            if field_name == 'slug':
                continue
            if field_name == 'id':
                item[field_name] = form[field_name]
                continue
            if field_name == 'created':
                item[field_name] = form[field_name]
                continue
            data[field_name] = form[field_name]
        item['data'] = data
        items.append(item)
    return jsonify(
        items=items,
        meta={'name': 'User forms',
              'deleted_fields': [],
              'default_field_index': default_admin_userforms_field_index,
              'editable_fields': False,
              'item_endpoint': None,
              'can_edit': False,
              'enable_notification': False,
        },
        user_prefs={'field_index': get_admin_userforms_field_index(flask_login.current_user),
                    'order_by': get_admin_userforms_order_by(flask_login.current_user),
                    'ascending': get_admin_userforms_ascending(flask_login.current_user)
        }
    ), 200

@data_display_bp.route('/data-display/admin/user/<int:user_id>/forms/change-index', methods=['POST'])
@auth.enabled_admin_required__json
def admin_userforms_change_field_index(user_id):
    """Change User's forms field index preference."""
    data = request.get_json(silent=True)
    if not 'field_index' in data:
        return jsonify("Not Acceptable"), 406
    field_index = data['field_index']
    if validators.compare_field_index(field_index, default_admin_userforms_field_index):
        flask_login.current_user.admin['userforms']['field_index'] = field_index
        flag_modified(flask_login.current_user, 'admin')
        flask_login.current_user.save()
    else:
        field_index = default_admin_userforms_field_index
    return jsonify(field_index=field_index), 200

@data_display_bp.route('/data-display/admin/user/<int:user_id>/forms/reset-index', methods=['POST'])
@auth.enabled_admin_required__json
def admin_userforms_reset_field_index(user_id):
    """Reset Users field index preference."""
    flask_login.current_user.admin['userforms']['field_index'] = default_admin_userforms_field_index
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(field_index=flask_login.current_user.admin['userforms']['field_index']), 200

@data_display_bp.route('/data-display/admin/user/<int:user_id>/forms/order-by-field', methods=['POST'])
@auth.enabled_admin_required__json
def admin_userforms_order_by_field(user_id):
    """Set User's forms order_by preference."""
    data = request.get_json(silent=True)
    if not 'order_by_field_name' in data:
        return jsonify("Not Acceptable"), 406
    field_names = [ field['name'] for field in default_admin_userforms_field_index ]
    if not data['order_by_field_name'] in field_names:
        return jsonify("Not Acceptable"), 406
    flask_login.current_user.admin['userforms']['order_by'] = data['order_by_field_name']
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(order_by_field_name=flask_login.current_user.admin['userforms']['order_by']), 200

@data_display_bp.route('/data-display/admin/user/<int:user_id>/forms/toggle-ascending', methods=['POST'])
@auth.enabled_admin_required__json
def admin_userforms_toggle_ascending(user_id):
    """Toggle User's forms ascending order preference."""
    preference = get_admin_userforms_ascending(flask_login.current_user)
    flask_login.current_user.admin['userforms']['ascending'] = not preference
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(ascending=flask_login.current_user.admin['userforms']['ascending']), 200


## My Forms

default_my_forms_field_index = [
                {'name': 'form_name__html', 'label': 'Name'},
                {'name': 'answers__html', 'label': 'Answers'},
                {'name': 'last_answer_date', 'label': 'Last answer'},
                {'name': 'is_public__html', 'label': 'Public'},
                {'name': 'is_shared', 'label': 'Shared'},
                {'name': 'created', 'label': 'Created'}
            ]

def get_my_forms_field_index(user):
    if 'forms_field_index' in user.preferences:
        return user.preferences['forms_field_index']
    return default_my_forms_field_index

def get_my_forms_ascending(user):
    if 'forms_ascending' in user.preferences:
        return user.preferences['forms_ascending']
    return True

def get_my_forms_order_by(user):
    if 'forms_order_by' in user.preferences:
        return user.preferences['forms_order_by']
    return 'created'

@data_display_bp.route('/data-display/my-forms/<int:user_id>', methods=['GET'])
@auth.enabled_user_required__json
def my_forms(user_id):
    """Return json required by Vue dataDisplay component.

    Hyperlinks to be rendered by the component and are declared by trailing '__html'
    """
    if not user_id == flask_login.current_user.id:
        return jsonify("Denied"), 401
    forms = flask_login.current_user.get_forms()
    items = []
    for form in FormSchemaForMyFormsDataDisplay(many=True).dump(forms):
        item = {}
        data = {}
        item['id'] = form['id']
        item['created'] = form['created']
        data['last_answer_date'] = form['last_answer_date']
        data['form_name__html'] = {
            'value': form['slug'],
            'html': f"<a href='/form/{form['id']}'>{form['slug']}</a>"
        }
        data['is_shared'] = form['is_shared']
        # stats column
        total_answers = form['total_answers']
        stats_icon = f"<i class='fa fa-bar-chart' \
                        style='margin-left: 0.25em; font-size: 1.1rem' \
                        aria-label=\"{_('Statistics')}\"></i>"
        stats_url = f"<a href='/form/{form['id']}/answers/stats'>{stats_icon}</a>"
        count_url = f"<a class='badge badge-primary' \
                    style='margin-left: 1em; font-size: 0.9rem' \
                    href='/form/{form['id']}/answers' \
                    aria-label=\"{_('Answers')}\">{total_answers}</a>"
        disk_usage = ""
        if form['attachments_usage'] is not None:
            bytes_str = human_readable_bytes(form['attachments_usage'])
            disk_usage = f"<span class='badge badge-info' \
                         style='margin-left: 0.8em; cursor: initial'>{bytes_str}</span>"
        data['answers__html'] = {
            'value': total_answers,
            'html': f"{stats_url} {count_url} {disk_usage}"
        }
        # is_public column
        badge_text = _('True') if form['is_public'] else _('False')
        badge_color = 'badge-success' if form['is_public'] else 'badge-secondary'
        public_badge = f"<span class='badge {badge_color}'>{badge_text}</span>"
        if form['edit_mode']:
            edit_mode_badge=f"<span class='badge badge-warning'>{_('Edit mode')}</span>"
        else:
            edit_mode_badge = ""
        badge_color = 'badge-warning' if form['edit_mode'] else 'badge-secondary'
        data['is_public__html'] = {
            'value': form['is_public'],
            'html': f"{public_badge} {edit_mode_badge}"
        }
        item['data'] = data
        items.append(item)
    return jsonify(
        items=items,
        meta={'name': 'my-forms',
              'deleted_fields': [],
              'default_field_index': default_my_forms_field_index,
              'editable_fields': False,
              'item_endpoint': None,
              'can_edit': False,
              'enable_notification': False,
        },
        user_prefs={'field_index': get_my_forms_field_index(flask_login.current_user),
                    'order_by': get_my_forms_order_by(flask_login.current_user),
                    'ascending': get_my_forms_ascending(flask_login.current_user)
        }
    ), 200

@data_display_bp.route('/data-display/my-forms/<int:user_id>/change-index', methods=['POST'])
@auth.enabled_user_required__json
def change_forms_field_index(user_id):
    """Change Users' Form (all forms) field index preference."""
    if not user_id == flask_login.current_user.id:
        return jsonify("Forbidden"), 403
    data = request.get_json(silent=True)
    if not 'field_index' in data:
        return jsonify("Not Acceptable"), 406
    field_index = data['field_index']
    if validators.compare_field_index(field_index, default_my_forms_field_index):
        flask_login.current_user.preferences['forms_field_index'] = field_index
        flask_login.current_user.save()
    else:
        field_index = default_my_forms_field_index
    return jsonify(field_index=field_index), 200

@data_display_bp.route('/data-display/my-forms/<int:user_id>/reset-index', methods=['POST'])
@auth.enabled_user_required__json
def reset_forms_field_index(user_id):
    """Reset Users' Form field index preference."""
    if not user_id == flask_login.current_user.id:
        return jsonify("Forbidden"), 403
    flask_login.current_user.preferences['forms_field_index'] = default_my_forms_field_index
    flask_login.current_user.save()
    return jsonify(field_index=flask_login.current_user.preferences['forms_field_index']), 200

@data_display_bp.route('/data-display/my-forms/<int:user_id>/order-by-field', methods=['POST'])
@auth.enabled_user_required__json
def order_forms_by_field(user_id):
    """Set User's forms order_by preference."""
    if not user_id == flask_login.current_user.id:
        return jsonify("Forbidden"), 403
    data = request.get_json(silent=True)
    if not 'order_by_field_name' in data:
        return jsonify("Not Acceptable"), 406
    field_names = [ field['name'] for field in default_my_forms_field_index ]
    if not data['order_by_field_name'] in field_names:
        return jsonify("Not Acceptable"), 406
    flask_login.current_user.preferences['forms_order_by'] = data['order_by_field_name']
    flask_login.current_user.save()
    return jsonify(order_by_field_name=flask_login.current_user.preferences['forms_order_by']), 200

@data_display_bp.route('/data-display/my-forms/<int:user_id>/toggle-ascending', methods=['POST'])
@auth.enabled_user_required__json
def forms_toggle_ascending(user_id):
    """Toggle User's forms ascending order preference."""
    if not user_id == flask_login.current_user.id:
        return jsonify("Forbidden"), 403
    preference = get_my_forms_ascending(flask_login.current_user)
    flask_login.current_user.preferences['forms_ascending'] = not preference
    flask_login.current_user.save()
    return jsonify(ascending=flask_login.current_user.preferences['forms_ascending']), 200


#### A my-form

@data_display_bp.route('/data-display/form/<int:form_id>', methods=['GET'])
@auth.enabled_user_required__json
def form_answers(form_id):
    """Return json required by vue data-display component."""
    formuser = FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not formuser:
        return jsonify("Not found"), 404
    form = formuser.form
    answers = form.answers
    default_field_index = form.get_field_index_for_display()
    field_index = formuser.get_field_index_preference()
    order_by = formuser.get_order_by_field()
    user_prefs = {
        'field_index': field_index if field_index else default_field_index,
        'order_by': order_by if order_by else 'created',
        'ascending': formuser.ascending
    }
    return jsonify(
        items=AnswerSchema(many=True).dump(answers),
        meta={'name': form.slug,
              'deleted_fields': form.get_deleted_fields(),
              'default_field_index': default_field_index,
              'form_structure' : form.structure,
              'item_endpoint': '/data-display/answer/',
              'can_edit': formuser.is_editor,
              'enable_exports': True,
              'enable_graphs': True,
              'enable_notification': True,
        },
        user_prefs=user_prefs
    ), 200

@data_display_bp.route('/data-display/form/<int:form_id>/change-index', methods=['POST'])
@auth.enabled_user_required__json
def change_answer_field_index(form_id):
    """Change User's Answer field index preference for this form."""
    formuser = FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not formuser:
        return jsonify("Not found"), 404
    data = request.get_json(silent=True)
    if not 'field_index' in data:
        return jsonify("Not Acceptable"), 406
    field_index = data['field_index']
    default_field_index = formuser.form.get_field_index_for_display()
    if validators.compare_field_index(field_index, default_field_index):
        formuser.save_field_index_preference(field_index)
        return jsonify(field_index=field_index), 200
    formuser.save_field_index_preference(default_field_index)
    return jsonify(field_index=default_field_index), 200

@data_display_bp.route('/data-display/form/<int:form_id>/reset-index', methods=['POST'])
@auth.enabled_user_required__json
def reset_answers_field_index(form_id):
    """Reset User's Answer field index preference for this form."""
    formuser = FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not formuser:
        return jsonify("Not found"), 404
    field_index = formuser.form.get_field_index_for_display()
    formuser.save_field_index_preference(field_index)
    return jsonify(field_index=field_index), 200

@data_display_bp.route('/data-display/form/<int:form_id>/order-by-field', methods=['POST'])
@auth.enabled_user_required__json
def order_answers_by_field(form_id):
    """Set User's order answers by preference for this form."""
    formuser = FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not formuser:
        return jsonify("Not found"), 404
    data = request.get_json(silent=True)
    if not 'order_by_field_name' in data:
        return jsonify("order_by_field_name required"), 406
    field_preference = data['order_by_field_name']
    field_names = [ field['name'] for field in formuser.form.fieldIndex ]
    if not field_preference in field_names:
        return jsonify("Not a valid field name"), 406
    formuser.save_order_by(field_preference)
    return jsonify(order_by_field_name=field_preference), 200

@data_display_bp.route('/data-display/form/<int:form_id>/toggle-ascending', methods=['POST'])
@auth.enabled_user_required__json
def answers_toggle_ascending(form_id):
    """Toggle user's ascending order preference for this Form's Answers."""
    formuser = FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not formuser:
        return jsonify("Not found"), 404
    return jsonify(ascending=formuser.toggle_ascending_order()), 200

@data_display_bp.route('/data-display/form/<int:form_id>/stats', methods=['GET'])
@auth.enabled_user_required__json
def answers_stats(form_id):
    """Redirect to the answers' stats page."""
    form_user=FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not form_user:
        return jsonify("Not found"), 404
    return redirect(url_for('answers_bp.answers_stats', form_id=form_user.form.id))

@data_display_bp.route('/data-display/form/<int:form_id>/delete-all-items', methods=['GET'])
@auth.enabled_user_required__json
def delete_all_answers(form_id):
    """Redirect to the delete all answers page."""
    formuser =FormUser.find(form_id=form_id,
                            user_id=flask_login.current_user.id,
                            is_editor=True)
    if not formuser:
        return jsonify("Not found"), 404
    return redirect(url_for('answers_bp.delete_answers', form_id=form_id))

@data_display_bp.route('/data-display/form/<int:form_id>/csv', methods=['GET'])
@auth.enabled_user_required__json
def answers_csv(form_id):
    """Redirect to the CSV download."""
    form_user=FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not form_user:
        return jsonify("Not found"), 404
    return redirect(url_for('answers_bp.answers_csv',
                            form_id=form_user.form.id,
                            with_deleted_columns=request.args.get('with_deleted_columns')))

#@data_display_bp.route('/data-display/form/<int:form_id>/toggle-item-notification', methods=['POST'])
#@auth.enabled_user_required__json
#def answers_notification(form_id):
#    """Toggle new answer notification."""
#    form_user = FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
#    if not form_user:
#        return jsonify("Forbidden"), 403
#    return jsonify(notification=form_user.toggle_notification()), 200


## Answers

@data_display_bp.route('/data-display/answer/<int:answer_id>/mark', methods=['POST'])
@auth.enabled_user_required__json
def toggle_answer_mark(answer_id):
    answer = Answer.find(id=answer_id)
    if not answer:
        return jsonify("Not found"), 404
    if not FormUser.find(form_id=answer.form_id, user_id=flask_login.current_user.id):
        return jsonify("Forbidden"), 403
    answer.marked = not answer.marked
    answer.save()
    return jsonify(marked=answer.marked), 200

@data_display_bp.route('/data-display/answer/<int:answer_id>/save', methods=['PATCH'])
@auth.enabled_editor_required__json
def update_answer(answer_id):
    answer = Answer.find(id=answer_id)
    if not answer:
        return jsonify("Not found"), 404
    if not FormUser.find(form_id=answer.form_id,
                         user_id=flask_login.current_user.id,
                         is_editor=True):
        return jsonify("Forbidden"), 403
    content = request.get_json()
    try:
        if not isinstance(content['item_data'], dict):
            return jsonify("Not an array"), 406
        #pprint(content['item_data'])
        answer.data = content['item_data']
        answer.save()
        answer.form.expired = answer.form.has_expired()
        answer.form.save()
        answer.form.add_log(_("Modified an answer"))
        return jsonify(saved=True, data=answer.data)
    except:
        return jsonify(saved=False)

@data_display_bp.route('/data-display/answer/<int:answer_id>/delete', methods=['DELETE'])
@auth.enabled_editor_required__json
def delete_answer(answer_id):
    answer = Answer.find(id=answer_id)
    if not answer:
        return jsonify("Not found"), 404
    form_user=FormUser.find(form_id=answer.form_id,
                            user_id=flask_login.current_user.id,
                            is_editor=True)
    if not form_user:
        return jsonify("Forbidden"), 403
    answer.delete()
    form=form_user.form
    form.expired = form.has_expired()
    form.save()
    if form.author.set_disk_usage_alert():
        form.author.save()
    form.add_log(_("Deleted an answer"))
    return jsonify(deleted=True, show_alert=bool(form.author.alerts)), 200
