"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from flask import g, request, render_template, redirect
from flask import current_app, flash, url_for
from flask import Blueprint, send_file, after_this_request
from flask_babel import gettext as _
import flask_login
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import AnswerAttachment
from liberaforms.utils import auth
from liberaforms.utils import sanitizers
from liberaforms.utils.chart_data import answers_chart_data
from liberaforms.utils.exports import write_answers_csv

#from pprint import pprint as pp

answers_bp = Blueprint('answers_bp', __name__,
                        template_folder='../templates/answers')


@answers_bp.route('/<string:slug>/answers', methods=['GET'])
@answers_bp.route('/form/<int:form_id>/answers', methods=['GET'])
@flask_login.login_required
@auth.enabled_user_required
def list_answers(form_id=None, slug=None):
    """Display the Answers to this Form."""
    if slug:
        slug=sanitizers.sanitize_slug(slug)
        form=Form.find(slug=slug)
        if not form:
            flash(_("Can't find that form"), 'warning')
            return redirect(url_for('form_bp.my_forms'))
        form_id = form.id
    elif not form_id:
        return redirect(url_for('form_bp.my_forms'))
    formuser=FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not formuser:
        flash(_("Can't find that form"), 'warning')
        return redirect(url_for('form_bp.my_forms'))
    return render_template('list-answers.html',
                            form=formuser.form,
                            is_form_editor=formuser.is_editor)


@answers_bp.route('/form/<int:form_id>/answers/stats', methods=['GET'])
@flask_login.login_required
@auth.enabled_user_required
def answers_stats(form_id):
    formuser=FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not formuser:
        flash(_("Can't find that form"), 'warning')
        return redirect(url_for('form_bp.my_forms'))
    return render_template('chart-answers.html',
                            form=formuser.form,
                            chart_data=answers_chart_data(formuser.form))


@answers_bp.route('/form/<int:form_id>/csv', methods=['GET'])
@flask_login.login_required
@auth.enabled_user_required
def answers_csv(form_id):
    formuser=FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not formuser:
        flash(_("Can't find that form"), 'warning')
        return redirect(url_for('form_bp.my_forms'))
    with_deleted_columns=bool(request.args.get('with_deleted_columns'))
    csv_file=write_answers_csv(formuser.form, with_deleted_columns=with_deleted_columns)
    @after_this_request
    def remove_file(response):
        os.remove(csv_file)
        return response
    return send_file(csv_file, mimetype="text/csv", as_attachment=True)


@answers_bp.route('/form/<int:form_id>/delete-all-answers', methods=['GET', 'POST'])
@auth.enabled_editor_required
@auth.instantiate_form
def delete_answers(form_id, **kwargs):
    form = kwargs['form']
    if request.method == 'POST':
        try:
            total_answers = int(request.form['totalAnswers'])
        except:
            flash(_("We expected a number"), 'warning')
            return render_template('delete-all-answers.html', form=form)
        if form.answers.count() == total_answers:
            form.delete_all_answers()
            form.add_log(_("Deleted all answers"))
            if not form.has_expired() and form.expired:
                form.expired=False
                form.save()
            if form.author.set_disk_usage_alert():
                form.author.save()
            # i18n: %s is a number, singular or plural
            flash(_("Deleted %s answers" % total_answers), 'success')
            return redirect(url_for('answers_bp.list_answers', form_id=form.id))
        flash(_("Number of answers does not match"), 'warning')
    return render_template('delete-all-answers.html', form=form)


@answers_bp.route('/form/<int:form_id>/attachment/<string:key>', methods=['GET'])
@flask_login.login_required
@auth.enabled_user_required
@sanitizers.sanitized_key_required
def download_attachment(form_id, key):
    formuser=FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not formuser:
        return render_template('page-not-found.html'), 400
    attachment = AnswerAttachment.find(form_id=form_id, storage_name=key)
    if not attachment:
        return render_template('page-not-found.html'), 400
    (file_bytes, file_name) = attachment.get_attachment()
    try:
        return send_file(file_bytes,
                         download_name=file_name,
                         as_attachment=True)
    except:
        current_app.logger.warning(f"Missing attachment. Answer id: {attachment.answer_id}")
        return render_template('page-not-found.html'), 404
