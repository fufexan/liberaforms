"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
from datetime import datetime, timezone
from urllib.parse import urlparse
from flask import current_app, Blueprint
from flask import g, request, render_template, redirect, url_for, jsonify
from flask import session, flash, send_file
from flask_babel import gettext as _
import flask_login

from liberaforms import csrf
from liberaforms.domain import form as form_domain
from liberaforms.models.form import Form
from liberaforms.models.invite import Invite
from liberaforms.models.schemas.invite import InviteSchema
from liberaforms.models.user import User
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer, AnswerAttachment
from liberaforms.form_templates import form_templates
from liberaforms.utils import utils
from liberaforms.utils import tokens
from liberaforms.utils import auth
from liberaforms.utils import form_helper
from liberaforms.utils import sanitizers
from liberaforms.utils import validators
from liberaforms.utils import html_parser
from liberaforms.utils import ldap
from liberaforms.utils.dispatcher.dispatcher import Dispatcher
from liberaforms.utils import wtf

#from pprint import pprint

form_bp = Blueprint('form_bp', __name__,
                    template_folder='../templates/form')


@form_bp.route('/forms', methods=['GET'])
@flask_login.login_required
@auth.enabled_user_required
def my_forms():
    if FormUser.find(user_id=flask_login.current_user.id):
        return render_template('my-forms.html', user=flask_login.current_user)
    return render_template('list-templates.html', templates=form_templates.templates)


@form_bp.route('/forms/new', methods=['GET'])
@auth.enabled_editor_required
def create_new_form():
    session_data = form_helper.clear_session_form_data(data_id=None)
    session_data['introductionTextMD'] = Form.default_introduction_text()
    return redirect(url_for('form_bp.edit_form'))


@form_bp.route('/forms/check-slug-availability', methods=['POST'])
@auth.enabled_editor_required__json
def is_slug_available():
    if 'slug' in request.form and request.form['slug']:
        slug=request.form['slug']
    else:
        return jsonify(slug="", available=False), 200
    slug=sanitizers.sanitize_slug(slug)
    if not (slug and form_helper.is_slug_available(slug)):
        return jsonify(slug=slug, available=False), 200
    # we return a sanitized slug as a suggestion for the user.
    return jsonify(slug=slug, available=True), 200


@form_bp.route('/form/edit', methods=['GET', 'POST'])
@form_bp.route('/form/<int:form_id>/edit', methods=['GET', 'POST'])
@auth.enabled_editor_required
def edit_form(form_id=None):
    session_data = form_helper.get_session_form_data(data_id=form_id)
    queried_form=None
    if form_id:
        if session_data['form_id'] != str(form_id):
            flash_text = _("Something went wrong. ID does not match session['form_id']")
            flash(flash_text, 'error')
            return redirect(url_for('form_bp.my_forms'))
        queried_form = flask_login.current_user.get_form(form_id, is_editor=True)
        if not queried_form:
            flash(_("You can't edit that form"), 'warning')
            return redirect(url_for('form_bp.my_forms'))
        if queried_form.edit_mode and \
           queried_form.edit_mode['editor_id'] != flask_login.current_user.id:
            return redirect(url_for('form_bp.inspect_form', form_id=queried_form.id))
        queried_form.edit_mode = {
            'editor_id': flask_login.current_user.id,
            'editor_email': flask_login.current_user.email,
            'start_time': str(datetime.now(timezone.utc))
        }
        queried_form.save()
    if request.method == 'POST':
        if queried_form:
            session_data['slug'] = queried_form.slug
        elif 'slug' in request.form:
            session_data['slug'] = sanitizers.sanitize_slug(request.form['slug'])
            if not form_helper.is_slug_available(session_data['slug']):
                flash(_("Something went wrong. Slug not unique!"), 'error')
                return redirect(url_for('form_bp.edit_form'))
        if not session_data['slug']:
            flash(_("Something went wrong. No slug!"), 'error')
            return redirect(url_for('form_bp.my_forms'))

        #if form_id and not form_helper.is_slug_available(session['slug']):
        #    flash(gettext("Something went wrong. slug is not unique."), 'error')
        #    return redirect(url_for('form_bp.edit_form'))
        form_structure = json.loads(request.form['structure'])
        form_structure = form_helper.repair_form_structure(form_structure,
                                                           form=queried_form)
        session_data['formStructure'] = form_structure
        session_data['formFieldIndex'] = Form.create_field_index(form_structure)
        session_data['introductionTextMD'] = sanitizers.remove_html_tags(
                                            request.form['introductionTextMD']
                                        )
        return redirect(url_for('form_bp.preview_form', form_id=form_id))
    options_with_data = {}
    edit_mode_alert = False
    if queried_form:
        options_with_data = queried_form.get_multichoice_options_with_saved_data()
        if queried_form.enabled and session_data['editModeAlert']:
            edit_mode_alert = queried_form.edit_mode
            session_data['editModeAlert'] = False
    return render_template('edit-form.html',
                            multichoiceOptionsWithSavedData=options_with_data,
                            upload_media_form=wtf.UploadMedia(),
                            human_readable_bytes=utils.human_readable_bytes,
                            edit_mode_alert=edit_mode_alert,
                            session_data=session_data)


@form_bp.route('/form/preview', methods=['GET'])
@form_bp.route('/form/preview/<int:form_id>', methods=['GET'])
@auth.enabled_editor_required
def preview_form(form_id=None):
    session_data = form_helper.get_session_form_data(data_id=form_id)
    if not ('slug' in session_data and 'formStructure' in session_data):
        return redirect(url_for('form_bp.my_forms'))
    max_attach_size=utils.human_readable_bytes(current_app.config['MAX_ATTACHMENT_SIZE'])
    return render_template( 'preview-form.html',
                            slug=session_data['slug'],
                            introduction_text=sanitizers.markdown2HTML(
                                                session_data['introductionTextMD']
                            ),
                            max_attachment_size_for_humans=max_attach_size,
                            session_data=session_data
                        )


@form_bp.route('/form/save', methods=['POST'])
@form_bp.route('/form/<int:form_id>/save', methods=['POST'])
@auth.enabled_editor_required
def save_form(form_id=None):
    if form_id:
        queried_form = flask_login.current_user.get_form(form_id=form_id, is_editor=True)
    else:
        queried_form = None
    session_data = form_helper.get_session_form_data(data_id=form_id)
    if 'structure' in request.form:
        # saved at edit_from page
        form_structure = json.loads(request.form['structure'])
        form_structure = form_helper.repair_form_structure(form_structure,
                                                           form=queried_form)
        form_index = Form.create_field_index(form_structure)
    else:
        # saved at preview_form page
        form_structure = session_data['formStructure']
        form_index = session_data['formFieldIndex']
    if 'introductionTextMD' in request.form:
        md_text = sanitizers.remove_html_tags(request.form['introductionTextMD'])
        session_data['introductionTextMD'] = md_text
    if not form_structure:
        form_structure=[{'label': _("Form"),
                        'subtype': 'h1',
                        'type': 'header'}]
    md_text = sanitizers.remove_html_tags(session_data['introductionTextMD'])
    html = sanitizers.markdown2HTML(session_data['introductionTextMD'])
    introduction_text={'markdown': md_text, 'html': html}
    if queried_form:
        if not queried_form.edit_mode or \
           queried_form.edit_mode['editor_id'] == flask_login.current_user.id:
            queried_form.structure=form_structure
            queried_form.update_field_index(form_index)
            queried_form.update_expiry_conditions()
            queried_form.edit_mode = {}
            queried_form.introduction_text=introduction_text
            queried_form.set_short_description()
            queried_form.set_thumbnail()
            queried_form.save()
            # reset formusers' field_index order preference
            FormUser.find_all(form_id=queried_form.id).update({
                                                    FormUser.field_index: None,
                                                    FormUser.order_by: None})
            flash(_("Updated form OK"), 'success')
            queried_form.add_log(_("Form edited"))
        else:
            flash(_("Cannot save your changes"), 'warning')
        form_helper.clear_session_form_data(data_id=form_id)
        return redirect(url_for('form_bp.inspect_form', form_id=queried_form.id))
    # this is a new form
    if not session_data['slug']:
        flash(_("Slug is missing."), 'error')
        return redirect(url_for('form_bp.edit_form'))
    if not form_helper.is_slug_available(session_data['slug']):
        # i18n: %s is the error code. Leave it after the point
        flash(_("Slug is not available. %s" % (session_data['slug'])), 'error')
        return redirect(url_for('form_bp.edit_form'))
    if session_data['duplication_in_progress']:
        # this new form is a duplicate
        after_submit_text=session_data['afterSubmitText']
        expired_text=session_data['expiredText']
    else:
        after_submit_text={'html':"", 'markdown':""}
        expired_text={'html':"", 'markdown':""}
    new_form_data={
        "slug": session_data['slug'],
        "structure": form_structure,
        "fieldIndex": form_index,
        "introduction_text": introduction_text,
        "after_submit_text": after_submit_text,
        "expired_text": expired_text
    }
    try:
        new_form = Form(flask_login.current_user, **new_form_data)
        new_form.set_short_description()
        new_form.set_thumbnail()
        new_form.save()
        form_user = FormUser(
                user_id = flask_login.current_user.id,
                form_id = new_form.id,
                is_editor = True,
                notifications = flask_login.current_user.new_form_notifications()
        )
        form_user.save()
    except Exception as error:
        current_app.logger.error(error)
        flash(_("Failed to save form"), 'error')
        return redirect(url_for('form_bp.edit_form'))
    form_helper.clear_session_form_data(data_id=form_id)
    new_form.add_log(_("Form created"))
    flash(_("Saved form OK"), 'success')
    Dispatcher().send_new_form_notification(new_form) # notify admins
    if g.site.new_form_msg:
        flash(g.site.get_new_form_msg(), 'info')
    return redirect(url_for('form_bp.inspect_form', form_id=new_form.id))


@form_bp.route('/form/<int:form_id>/data-consent', methods=['GET', 'POST'])
@auth.enabled_editor_required
@auth.instantiate_form
def edit_data_consent(form_id, **kwargs):
    form = kwargs['form']
    if request.method == 'POST':
        if 'markdown' in request.form and "checkbox_text" in request.form:
            markdown=sanitizers.bleach_text(request.form['markdown'])
            form.save_consent({
                "markdown": markdown,
                "html": sanitizers.markdown2HTML(markdown),
                "checkbox_text": sanitizers.remove_html_tags(request.form['checkbox_text']),
                "required": "True"
            })
            form.add_log(_("Edited GDPR text"))
            flash(_("GDPR text saved OK"), 'success')
    return render_template('form/data-consent.html', form=form)


@form_bp.route('/form/<int:form_id>/toggle-data-consent', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form__json()
def toggle_data_consent(form_id, **kwargs):
    form=kwargs['form']
    enabled=form.toggle_consent_enabled()
    # i18n: %s is True/Enabled or False/Disabled
    form.add_log(_("Data protection consent set to: %s" % enabled))
    return jsonify(enabled=enabled), 200


@form_bp.route('/form/<int:form_id>/save-after-submit-text', methods=['POST'])
@auth.enabled_editor_required__json
def save_after_submit_text(form_id):
    form_user=FormUser.find(form_id=form_id,
                            user_id=flask_login.current_user.id,
                            is_editor=True)
    if not form_user:
        return jsonify(html="", markdown=""), 406
    if 'markdown' in request.form:
        form=form_user.form
        form.save_after_submit_text(request.form['markdown'])
        form.add_log(_("Edited Thankyou text"))
        pay_load = {'html': form.get_after_submit_text_html(),
                    'markdown': form.get_after_submit_text_markdown()}
        return jsonify(pay_load), 200
    user_id=flask_login.current_user.id
    msg=f"Failed to save After submit text. user.id:{user_id} form.id:{form_id}"
    current_app.logger.warning(msg)
    pay_load = {'html': "<h1>%s</h1>" % _("An error occured"),
                'markdown': ""}
    return jsonify(pay_load), 200


@form_bp.route('/form/<int:form_id>/save-expired-text', methods=['POST'])
@auth.enabled_editor_required__json
def save_expired_text(form_id):
    form_user=FormUser.find(form_id=form_id,
                            user_id=flask_login.current_user.id,
                            is_editor=True)
    if not form_user:
        return jsonify(html="", markdown=""), 406
    if 'markdown' in request.form:
        form=form_user.form
        form.save_expired_text(request.form['markdown'])
        form.add_log(_("Edited expiry text"))
        pay_load = {'html': form.get_expired_text_html(),
                    'markdown': form.get_expired_text_markdown()}
        return jsonify(pay_load), 200
    user_id=flask_login.current_user.id
    msg=f"Failed to save Expired text. user.id:{user_id} form.id:{form_id}"
    current_app.logger.warning(msg)
    pay_load = {'html': "<h1>%s</h1>" % _("An error occured"),
                'markdown': ""}
    return jsonify(pay_load), 200


@form_bp.route('/form/<int:form_id>/delete', methods=['GET', 'POST'])
@auth.enabled_editor_required
@auth.instantiate_form
def delete_form(form_id, **kwargs):
    form=kwargs['form']
    if request.method == 'POST':
        if form.slug == request.form['slug']:
            answer_cnt = form.get_answers().count()
            form.delete()
            # i18n: first %s is form name, second is a number counting answers
            flash_text = _("Deleted '%s' and %s answers" % (form.slug, answer_cnt))
            flash(flash_text, 'success')
            return redirect(url_for('form_bp.my_forms'))
        flash(_("Form name does not match"), 'warning')
    return render_template('delete-form.html', form=form)


@form_bp.route('/form/<int:form_id>', methods=['GET'])
@auth.enabled_user_required
def inspect_form(form_id):
    queried_form = Form.find(id=form_id)
    if not queried_form:
        flash(_("Cannot find that form"), 'warning')
        return redirect(url_for('form_bp.my_forms'))
    form_user = FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not (g.is_admin or form_user):
        flash(_("Permission needed to view form"), 'warning')
        return redirect(url_for('form_bp.my_forms'))
    if not flask_login.current_user.can_inspect_form(queried_form):
        return redirect(url_for('answers_bp.list_answers', form_id=form_id))
    if queried_form.edit_mode:
        if queried_form.edit_mode['editor_id'] == flask_login.current_user.id:
            # edit_mode is cancelled when current editor inspects the form
            queried_form.edit_mode = {}
            queried_form.save()
            if not request.args.get('cancel-edit-mode', type=str):
                # cancel edit_mode was not explicitly requested so we flash a msg
                flash(_("Edit mode cancelled Ok"), 'success')
        elif form_user and form_user.is_editor:   # admins shouldn't see this flash
        #else:
            duration = utils.get_fuzzy_duration(queried_form.edit_mode['start_time'])
            msg = _("{email} started editing this form about {time} ago")
            msg = msg.replace('{email}', queried_form.edit_mode['editor_email'])
            msg = msg.replace('{time}', duration)
            flash(msg, 'info')
    if not queried_form.expired and queried_form.has_expired():
        form_domain.expire_form(queried_form)
        flash(_("This form has expired"), 'info')
    # populate session for possible edit
    form_helper.populate_session_with_form(queried_form)
    return render_template('inspect-form.html',
                            form=queried_form,
                            FormUser=FormUser,
                            is_form_editor=bool(form_user and form_user.is_editor),
                            invites = Invite.find_all(granted_form=form_id),
                            human_readable_bytes=utils.human_readable_bytes)


@form_bp.route('/form/<int:form_id>/fediverse-publish', methods=['GET', 'POST'])
@auth.enabled_editor_required
@auth.instantiate_form
def fedi_publish(form_id, **kwargs):
    form=kwargs['form']
    if not flask_login.current_user.fedi_auth:
        flash(_("Fediverse connect is not configured"), 'warning')
        return redirect(url_for('form_bp.inspect_form', form_id=form.id))
    wtform = wtf.FormPublish()
    if wtform.validate_on_submit():
        status = Dispatcher().publish_form(wtform.text.data,
                                           wtform.image_source.data,
                                           fediverse=True)
        if status['published'] is True:
            form.published_cnt += 1
            form.save()
            status_uri = status['msg']
            # i18n: variable is a Fediverse node. Example: Published at barcelona.social
            flash(_("Published at %s" % status_uri), 'success')
        else:
            flash(status['msg'], 'warning')
        return redirect(url_for('form_bp.inspect_form', form_id=form_id))
    if request.method == 'GET':
        html = form.introduction_text['html']
        text = html_parser.extract_text(html, with_links=True).strip('\n')
        wtform.text.data = f"{form.url}\n\n{text}"
        wtform.image_source.data = form.thumbnail
    fedi_auth = flask_login.current_user.get_fedi_auth()
    return render_template('fedi-publish.html',
                            connection_title=flask_login.current_user.fedi_connection_title(),
                            node_name=urlparse(fedi_auth['node_url']).netloc,
                            form=form,
                            wtform=wtform)


@form_bp.route('/form/<int:form_id>/share', methods=['GET'])
@form_bp.route('/form/<int:form_id>/share/<string:with_email>', methods=['GET'])
@auth.enabled_editor_required
@auth.instantiate_form
def share_form(form_id, with_email=None, **kwargs):
    form=kwargs['form']
    invites=Invite.find_all(granted_form=form.id)
    invites=InviteSchema(many=True,
                         only=('id', 'created', 'message', 'email')).dump(invites)
    return render_template('share-form.html',
                            form=form,
                            FormUser=FormUser,
                            invites=invites,
                            wtform=wtf.GetEmail(),
                            grant='read-only', #grant,
                            invitation_email=with_email)


@form_bp.route('/form/<int:form_id>/add-editor', methods=['POST'])
@auth.enabled_editor_required
@auth.instantiate_form
def add_editor(form_id, **kwargs):
    form = kwargs['form']
    wtform=wtf.GetEmail()
    if wtform.validate_on_submit():
        new_editor=User.find(email=wtform.email.data)
        if not new_editor:
            flash(_("Can't find a user with that email"), 'warning')
            return redirect(url_for('form_bp.share_form', form_id=form.id))
        if FormUser.find(form_id=form.id, user_id=new_editor.id):
			# i18n: %s stands for the user name here
            flash(_("This form is already shared with %s" % new_editor.email), 'warning')
            return redirect(url_for('form_bp.share_form', form_id=form.id))
        try:
            new_form_user=FormUser(form_id=form.id,
                                   user_id=new_editor.id,
                                   notifications=new_editor.new_form_notifications(),
                                   is_editor=True)
            new_form_user.save()
            flash(_("Added user OK"), 'success')
            # i18n: %s is user's email address
            form.add_log(_("Added editor %s" % new_editor.email))
        except Exception as error:
            current_app.logger.error(error)
            flash(_("Could not add the user"), 'error')
    return redirect(url_for('form_bp.share_form', form_id=form.id))


@form_bp.route('/form/<int:form_id>/remove-editor/<int:editor_id>', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form__json()
def remove_editor(form_id, editor_id, **kwargs):
    form = kwargs['form']
    if form.author_id == editor_id:
        return jsonify(False), 200
    form_editor = FormUser.find(form_id=form.id, user_id=editor_id, is_editor=True)
    if form_editor:
        removed_user = form_editor.user
        form_editor.delete()
        # i18n: %s is user's email address
        form.add_log(_("Removed editor %s" % removed_user.email))
        return jsonify(str(removed_user.id)), 200
    return jsonify(False), 404


@form_bp.route('/form/<int:form_id>/add-reader', methods=['POST'])
@auth.enabled_editor_required
@auth.instantiate_form
def add_reader(form_id, **kwargs):
    form = kwargs['form']
    wtform=wtf.GetEmail()
    if wtform.validate_on_submit():
        new_reader=User.find(email=wtform.email.data)
        if not new_reader:
            return redirect(url_for('form_bp.share_form',
                                    form_id=form.id,
                                    with_email=wtform.email.data))
        if new_reader.enabled is False:
            flash(_("That user has been disabled by an Admin"), 'warning')
            return redirect(url_for('form_bp.share_form', form_id=form.id))
        if FormUser.find(form_id=form.id, user_id=new_reader.id):
            flash(_("This form is already shared with %s" % new_reader.email), 'warning')
            return redirect(url_for('form_bp.share_form', form_id=form.id))
        try:
            new_form_user=FormUser(form_id=form.id,
                                   user_id=new_reader.id,
                                   notifications=new_reader.new_form_notifications(),
                                   is_editor=False)
            new_form_user.save()
            flash(_("Added user OK"), 'success')
			# i18n: %s is user's email address
            form.add_log(_("Added read only user %s" % new_reader.email))
        except Exception as error:
            current_app.logger.error(error)
            flash(_("Could not add the user"), 'error')
    return redirect(url_for('form_bp.share_form', form_id=form.id))


@form_bp.route('/form/<int:form_id>/remove-reader/<int:reader_id>', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form__json()
def remove_reader(form_id, reader_id, **kwargs):
    form = kwargs['form']
    form_reader = FormUser.find(form_id=form.id, user_id=reader_id, is_editor=False)
    if form_reader:
        removed_user = form_reader.user
        form_reader.delete()
		# i18n: %s is user's email address
        form.add_log(_("Removed user %s" % removed_user.email))
        return jsonify(str(removed_user.id)), 200
    return jsonify(False), 406


@form_bp.route('/form/<int:form_id>/invitation/<string:email>', methods=['GET', 'POST'])
@auth.enabled_editor_required
@auth.instantiate_form
def send_grant_form_invitation(form_id, email, **kwargs):
    form = kwargs['form']
    wtform=wtf.NewInvite()
    if wtform.validate_on_submit():
        if wtform.granted_form_id.data != form.id:
            current_app.logger.warning(f"Did not create form invitation form_id: {form.id}")
            return redirect(url_for('form_bp.inspect_form', form_id=form.id))
        role = 'guest'
        ldap_uuid = None
        if current_app.config['ENABLE_LDAP']:
            conn, msg = ldap.bind()
            results, msg = ldap.search(conn, wtform.email.data)
            ldap.unbind(conn)
            if msg['status'] == 1:
                ldap_uuid = results[0]['entryUUID']
                role = "editor"
        new_invite=Invite(
                        email=wtform.email.data,
                        message=wtform.message.data,
                        token=tokens.create_token(Invite),
                        role=role,
                        granted_form={'id': form.id, 'grant': 'read-only'},
                        invited_by_id=flask_login.current_user.id,
                        ldap_uuid=ldap_uuid
                    )
        new_invite.save()
        status = Dispatcher().send_invitation(new_invite)
        if status['email_sent'] is True:
            flash_text = _("We have sent an invitation to %s" % new_invite.email)
            flash(flash_text, 'success')
        else:
            new_invite.delete()
            flash(status['msg'], 'warning')
        return redirect(url_for('form_bp.share_form', form_id=form.id))
    wtform.message.data=Invite.default_message()
    wtform.email.data = wtform.email.data if wtform.email.data else email
    wtform.granted_form_id.data = form.id
    return render_template('invite/new-invite.html',
                            wtform=wtform,
                            default_role='guest',
                            granted_form=form,
                            total_invites=Invite.find_all().count())


@form_bp.route('/form/<int:form_id>/expiration', methods=['GET'])
@auth.enabled_editor_required
def expiration(form_id):
    form_user = FormUser.find(form_id=form_id,
                              user_id=flask_login.current_user.id,
                              is_editor=True)
    if not form_user:
        flash(_("Cannot find that form"), 'warning')
        return redirect(url_for('form_bp.my_forms'))
    return render_template('expiration.html', form=form_user.form,
                                              form_user=form_user)


@form_bp.route('/form/<int:form_id>/expiration/set-date', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form__json()
def set_expiration_date(form_id, **kwargs):
    form = kwargs['form']
    if 'date' in request.form and 'time' in request.form:
        if request.form['date'] and request.form['time']:
            expireDate="%s %s:00" % (request.form['date'], request.form['time'])
            if not validators.is_valid_date(expireDate):
                pay_load = {'error': _("Date-time is not valid"),
                            'expired': form.has_expired()}
                return jsonify(pay_load), 200
            else:
                form.save_expiry_date(expireDate)
                # i18n: %s is a date
                form.add_log(_("Expiry date set to: %s" % expireDate))
        elif not request.form['date'] and not request.form['time']:
            if form.expiry_conditions['expireDate']:
                form.save_expiry_date(False)
                form.add_log(_("Expiry date cancelled"))
        else:
            pay_load = {'error': _("Missing date or time"),
                        'expired': form.has_expired()}
            return jsonify(pay_load), 200
        return jsonify(expired=form.has_expired()), 200
    return jsonify(False), 200


@form_bp.route('/form/<int:form_id>/expiration/set-field-condition', methods=['POST'])
@auth.enabled_editor_required__json
def set_expiry_field_condition(form_id):
    form_user = FormUser.find(form_id=form_id,
                              user_id=flask_login.current_user.id,
                              is_editor=True)
    if not form_user:
        return jsonify(condition=False), 406
    if 'field_name' in request.form and 'condition' in request.form:
        form = form_user.form
        condition=form.save_expiry_field_condition(request.form['field_name'],
                                                   request.form['condition'])
        field_label = form.get_field_label(request.form['field_name'])
        # i18n: First %s is the name of the field in the form. Second %s is a condition
        form.add_log(_("Field '%s' expiry set to: %s" % (field_label,
                                                         request.form['condition'])))
        return jsonify(condition=condition, expired=form.expired), 200
    return jsonify(condition=False), 406


@form_bp.route('/form/<int:form_id>/expiration/set-total-answers', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form__json()
def set_expiry_total_answers(form_id, **kwargs):
    form = kwargs['form']
    if not 'total_answers' in request.form:
        return jsonify(expired=False, total_answers=0), 406
    total_answers = request.form['total_answers']
    total_answers = form.save_expiry_total_answers(total_answers)
    # i18n: %s is a number. Example: Expire when total answers set to: 3
    form.add_log(_("Expire when total answers set to: %s" % total_answers))
    return jsonify(expired=form.expired, total_answers=total_answers), 200


@form_bp.route('/form/<int:form_id>/duplicate', methods=['GET'])
@auth.enabled_editor_required
def duplicate_form(form_id):
    form = Form.find(id=form_id)
    if not form:
        flash(_("Can't find that form"), 'warning')
        return redirect(url_for('form_bp.my_forms'))
    form_helper.populate_session_form_duplicate(form)
    #session['slug']=""
    #session['form_id']=None
    #session['duplication_in_progress'] = True
    flash(_("You can edit the duplicate now"), 'info')
    return redirect(url_for('form_bp.edit_form'))


@form_bp.route('/forms/log/list/<int:form_id>', methods=['GET'])
@auth.enabled_editor_required
@auth.instantiate_form
def list_log(form_id, **kwargs):
    return render_template('list-log.html', form=kwargs['form'])


@form_bp.route('/form/<int:form_id>/qr', methods=['GET'])
@auth.enabled_editor_required
@auth.instantiate_form
def form_qr(form_id, **kwargs):
    form = kwargs['form']
    if 'as_png' in request.args:
        return send_file(form.get_qr(as_png=True),
                         download_name=f"{form.slug}-QR.png",
                         mimetype="image/png",
                         as_attachment=True)
    return render_template('form/qr.html', form=form)


## Form settings

@form_bp.route('/form/toggle-enabled/<int:form_id>', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form__json()
def toggle_form_enabled(form_id, **kwargs):
    form=kwargs['form']
    enabled=form.toggle_enabled()
    # i18n: %s is True/Enabled or False/Disabled
    form.add_log(_("Public set to: %s" % enabled))
    return jsonify(enabled=enabled), 200


@form_bp.route('/form/<int:form_id>/kill-edition-mode', methods=['GET'])
@auth.enabled_editor_required
@auth.instantiate_form
def kill_edit_mode(form_id, **kwargs):
    form = kwargs['form']
    form.edit_mode = {}
    form.save()
    flash(_("Edit mode deactivated"), 'success')
    return redirect(url_for('form_bp.inspect_form', form_id=form.id))


@form_bp.route('/form/<int:form_id>/toggle-restricted-access', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form__json()
def toggle_restricted_access(form_id, **kwargs):
    form=kwargs['form']
    access=form.toggle_restricted_access()
    # i18n: %s is True/Enabled or False/Disabled
    form.add_log(_("Restricted access set to: %s" % access))
    return jsonify(restricted=access), 200


@form_bp.route('/form/<int:form_id>/toggle-send-confirmation', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form__json()
def toggle_form_send_confirmation(form_id, **kwargs):
    form=kwargs['form']
    send_confirmation:bool = form.toggle_send_confirmation()
    # i18n: %s is True/Enabled or False/Disabled
    form.add_log(_("Send Confirmation set to: %s" % send_confirmation))
    return jsonify(confirmation=send_confirmation), 200


@form_bp.route('/form/<int:form_id>/toggle-notification', methods=['POST'])
@auth.enabled_user_required__json
def toggle_form_notification(form_id):
    """Toggle current_user's Form preference."""
    form_user = FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not form_user:
        return jsonify(None), 406
    return jsonify(notification=form_user.toggle_new_answer_notification()), 200


@form_bp.route('/form/<int:form_id>/expiration/toggle-notification', methods=['POST'])
@auth.enabled_user_required__json
def toggle_form_expiration_notification(form_id):
    form_user = FormUser.find(form_id=form_id, user_id=flask_login.current_user.id)
    if not form_user:
        return jsonify(None), 406
    return jsonify(notification=form_user.toggle_expiration_notification()), 200


@form_bp.route('/embed/<string:slug>', methods=['GET', 'POST'])
@csrf.exempt
@sanitizers.sanitized_slug_required
def view_embedded_form(slug):
    flask_login.logout_user()
    g.embedded=True
    return view_form(slug=slug)


@form_bp.route('/<string:slug>', methods=['GET', 'POST'])
@sanitizers.sanitized_slug_required
def view_form(slug):
    queried_form = Form.find(slug=slug)
    if not queried_form:
        if flask_login.current_user.is_authenticated:
            flash(_("Cannot find that form"), 'warning')
            return redirect(url_for('form_bp.my_forms'))
        return render_template('page-not-found.html'), 404
    if not queried_form.expired and queried_form.has_expired():
        form_domain.expire_form(queried_form)
    if not queried_form.is_public():
        if flask_login.current_user.is_authenticated: # and is form_user could be a condition
            if queried_form.expired:
                flash(_("That form has expired"), 'warning')
            elif queried_form.edit_mode:
                flash(_("That form is being edited"), 'warning')
            else:
                flash(_("That form is not public"), 'warning')
            return redirect(url_for('form_bp.my_forms'))
        if queried_form.expired:
            return render_template('form-has-expired.html',
                                    form=queried_form,
                                    navbar=False, no_bot=True), 200
        if queried_form.edit_mode:
            return render_template('try-again-soon.html',
                                    navbar=False, no_bot=True), 200
        return render_template('page-not-found.html'), 404
    if queried_form.restricted_access and not flask_login.current_user:
        return render_template('page-not-found.html'), 404

    if request.method == 'POST':
        form_data=request.form.to_dict(flat=False)
        answer_data = {}
        for key in form_data:
            if key=='csrf_token':
                continue
            value = form_data[key]
            if isinstance(value, list): # A checkboxes-group contains multiple values
                value=', '.join(value) # convert list of values to a string
                key=key.rstrip('[]') # remove tailing '[]' from the name attrib (appended by formbuilder)
            value=sanitizers.remove_first_and_last_newlines(value.strip())
            answer_data[key]=value
        answer = Answer(queried_form.id, queried_form.author_id, answer_data)
        answer.save()

        if request.files:
            for file_field_name in request.files.keys():
                if not queried_form.has_field(file_field_name):
                    continue
                file = request.files[file_field_name]
                # TODO: check size
                if not (file.content_type and \
                        file.content_type in g.site.mimetypes['mimetypes']):
                    continue
                if file.filename:
                    attachment = AnswerAttachment(answer)
                    try:
                        saved = attachment.save_attachment(file)
                        if saved:
                            url = attachment.get_url()
                            link = f'<a href="{url}">{file.filename}</a>'
                            answer.update_field(file_field_name, link)
                            if queried_form.author.set_disk_usage_alert():
                                queried_form.author.save()
                    except Exception as error:
                        current_app.logger.error(error)
                        err = f"Failed to save attachment: form:{queried_form.slug}, answer:{answer.id}"
                        current_app.logger.error(err)

        if queried_form.might_send_confirmation_email() \
            and 'send-confirmation' in form_data:
            email=queried_form.get_confirmation_email_address(answer)
            if validators.is_valid_email(email):
                try:
                    Dispatcher().send_answer_confirmation(email, queried_form)
                except Exception as error:
                    current_app.logger.error(error)
        emails=[]
        for form_user in FormUser.find_all(form_id=queried_form.id):
            if form_user.user.enabled and form_user.notifications["newAnswer"]:
                emails.append(form_user.user.email)
        if emails:
            try:
                Dispatcher().send_new_answer_notification(emails, queried_form.slug)
            except Exception as error:
                current_app.logger.error(error)
        if queried_form.has_expired():
            form_domain.expire_form(queried_form)
        return render_template('thankyou.html',
                                form=queried_form,
                                navbar=False)

    return render_template('view-form.html',
                            form=queried_form,
                            opengraph=queried_form.get_opengraph(),
                            navbar=False,
                            no_bot=True,
                            human_readable_bytes=utils.human_readable_bytes)


@form_bp.route('/forms/templates', methods=['GET'])
@flask_login.login_required
@auth.enabled_editor_required
def list_templates():
    return render_template('list-templates.html',
                            templates = form_templates.templates)

@form_bp.route('/forms/template/<int:template_id>', methods=['GET'])
@flask_login.login_required
@auth.enabled_editor_required
def view_template(template_id):
    session_data = form_helper.clear_session_form_data(data_id=None)
    template = next((sub for sub in form_templates.templates if sub['id'] == template_id), None)
    if not template:
        return redirect(url_for('form_bp.list_templates'))
    introduction_text = sanitizers.markdown2HTML(str(template['introduction_md']))
    session_data['formStructure'] = template['structure']
    return render_template('preview-form.html',
                            is_template=True,
                            slug=template['name'],
                            introduction_text=introduction_text,
                            template = template,
                            session_data=session_data)

@form_bp.route('/forms/template/<int:template_id>/create-form', methods=['GET'])
@flask_login.login_required
@auth.enabled_editor_required
def create_form_from_template(template_id):
    session_data = form_helper.clear_session_form_data(data_id=None)
    template = next((sub for sub in form_templates.templates if sub['id'] == template_id), None)
    if not template:
        return redirect(url_for('form_bp.list_templates'))
    session_data['introductionTextMD']=template['introduction_md']
    session_data['formStructure'] = template['structure']
    flash(_("Copied template OK. You can edit your new form"), 'success')
    return redirect(url_for('form_bp.edit_form'))
