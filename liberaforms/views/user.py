"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from flask import g, request, render_template, redirect
from flask import Blueprint, current_app, url_for
from flask import session, flash, jsonify
from flask_babel import gettext as _
from flask_babel import refresh as babel_refresh
import flask_login
from liberaforms.models.user import User
from liberaforms.domain.user import UserDomain
from liberaforms.utils.dispatcher import Dispatcher
from liberaforms.utils import utils
from liberaforms.utils import validators
from liberaforms.utils import auth
from liberaforms.utils import tokens
from liberaforms.utils import wtf
from liberaforms.utils import chart_data

#from pprint import pprint

user_bp = Blueprint('user_bp',
                    __name__,
                    template_folder='../templates/user')

@user_bp.route('/user/new', methods=['GET', 'POST'])
@user_bp.route('/user/new/<string:invite>', methods=['GET', 'POST'])
@tokens.instantiate_invite
def create_new_user(invite=None):
    if g.site.invitation_only and not invite:
        return redirect(url_for('main_bp.index'))
    flask_login.logout_user()
    if invite and invite.has_ldap_entry():
        return redirect(url_for('user_bp.login', invite=invite.token['token']))
    wtform=wtf.NewUser()
    if wtform.validate_on_submit():
        validated_email=False
        is_first_admin = not bool(User.find_all(role='admin'))
        role = 'editor' # default role
        if invite:
            role = invite.role
            if invite.email == wtform.email.data:
                validated_email=True
            if role == 'admin':
                # the first admin of a new Site may need to config SMTP before
                # we can send emails, but when validated_email=False we cannot
                # send a validation email because SMTP is not congifured.
                if is_first_admin:
                    validated_email=True
        if wtform.email.data == current_app.config['ROOT_USER']:
            role = 'admin'
            validated_email=True
        try:
            new_user = User(
                username = wtform.username.data,
                email =  wtform.email.data,
                password = wtform.password.data,
                role = role,
                invited_by_id = invite.invited_by_id if invite else None,
                validated_email = validated_email,
            )
            new_user.save()
        except Exception as error:
            current_app.logger.error(error)
            flash(_("Opps! An error ocurred when creating the user"), 'error')
            return render_template('new-user.html')
        if invite:
            if invite.granted_form:
                domain=UserDomain(new_user.username, wtform.password.data)
                domain.grant_form_answers(invite)
            invite.delete()
        if not is_first_admin:
            Dispatcher().send_new_user_notification(new_user)
        flask_login.login_user(new_user)
        babel_refresh()
        flash(_("Welcome!"), 'success')
        if validated_email is False:
            return send_email_validation()
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    if "user_id" in session:
        session.pop("user_id")
    if not wtform.email.data and invite:
        wtform.email.data = invite.email
    return render_template('new-user.html', wtform=wtform)


@user_bp.route('/user/<string:username>', methods=['GET'])
@flask_login.login_required
def user_settings(username):
    """Render the User settings page."""
    if username != flask_login.current_user.username:
        return redirect(url_for('user_bp.user_settings',
                                 username=flask_login.current_user.username))
    if flask_login.current_user.update_alerts():
        flask_login.current_user.save()
    return render_template('user-settings.html',
                            human_readable_bytes=utils.human_readable_bytes)


@user_bp.route('/user/<string:username>/statistics', methods=['GET'])
@flask_login.login_required
@auth.enabled_user_required
def statistics(username):
    if username != flask_login.current_user.username:
        return redirect(url_for('user_bp.statistics',
                                username=flask_login.current_user.username))
    return render_template('user/statistics.html',
                            stats=chart_data.get_user_statistics(flask_login.current_user))


@user_bp.route('/user/send-email-validation', methods=['GET'])
@auth.authenticated_user_required
def send_email_validation():
    flask_login.current_user.save_token(email=flask_login.current_user.email)
    status = Dispatcher().send_email_address_confirmation(flask_login.current_user,
                                                          flask_login.current_user.email)
    if status['email_sent'] is True:
        # i18n: %s is an email address
        flash(_("We have sent an email to %s") % flask_login.current_user.email, 'info')
    else:
        flash(status['msg'], 'warning')
        current_app.logger.warning(status['msg'])
    return redirect(url_for('user_bp.user_settings',
                            username=flask_login.current_user.username))


## Personal user settings

@user_bp.route('/user/change-language', methods=['GET', 'POST'])
@auth.authenticated_user_required
def change_language():
    if request.method == 'POST':
        if 'language' in request.form and \
            request.form['language'] in current_app.config['LANGUAGES']:
            flask_login.current_user.preferences["language"]=request.form['language']
            flask_login.current_user.save()
            babel_refresh()
            flash(_("Language updated OK"), 'success')
            return redirect(url_for('user_bp.user_settings',
                                    username=flask_login.current_user.username))
    return render_template('common/change-language.html',
                            current_language=flask_login.current_user.language)


@user_bp.route('/user/change-email', methods=['GET', 'POST'])
@auth.authenticated_user_required
def change_email():
    wtform=wtf.ChangeEmail()
    if wtform.validate_on_submit():
        flask_login.current_user.save_token(email=wtform.email.data)
        status = Dispatcher().send_email_address_confirmation(flask_login.current_user,
                                                              wtform.email.data)
        if status['email_sent'] is True:
            flash(_("We have sent an email to %s") % wtform.email.data, 'info')
        else:
            # TODO: Tell the user that the email has not been sent
            pass
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    return render_template('change-email.html', wtform=wtform)


@user_bp.route('/user/reset-password', methods=['GET', 'POST'])
@auth.authenticated_user_required
def reset_password():
    wtform=wtf.ResetPassword()
    if wtform.validate_on_submit():
        flask_login.current_user.password_hash=validators.hash_password(wtform.password.data)
        flask_login.current_user.save()
        flash(_("Password changed OK"), 'success')
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    return render_template('reset-password.html', wtform=wtform)


@user_bp.route('/user/change-timezone', methods=['GET', 'POST'])
@auth.authenticated_user_required
def change_timezone():
    wtform=wtf.ChangeTimeZone()
    if wtform.validate_on_submit():
        flask_login.current_user.timezone = wtform.timezone.data
        flask_login.current_user.save()
        flash(_("Time zone changed OK"), 'success')
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    timezones = {}
    tz_path = os.path.join(current_app.config['ROOT_DIR'], 'assets/timezones.txt')
    with open(tz_path, 'r') as available_timezones:
        lines = available_timezones.readlines()
        for line in lines:
            line=line.strip()
            timezones[line] = line
    return render_template('change-timezone.html',
                            timezones=timezones,
                            wtform=wtform)


@user_bp.route('/user/<string:username>/fediverse', methods=['GET', 'POST'])
@auth.enabled_user_required
def fediverse_config(username):
    if username != flask_login.current_user.username:
        return redirect(url_for('user_bp.fediverse_config',
                                username=flask_login.current_user.username))
    wtform=wtf.FediverseAuth()
    if wtform.validate_on_submit():
        data = {"title": wtform.title.data,
                "node_url": wtform.node_url.data,
                "access_token": wtform.access_token.data}
        flask_login.current_user.set_fedi_auth(data)
        if not flask_login.current_user.fedi_auth:
            current_app.logger.warning('Could not encrypt access token')
            flash(_("Could not encrypt your access token"), 'warning')
        else:
            flash(_("Connected to the Fediverse"), 'success')
        flask_login.current_user.save()
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    if request.method == 'GET' and flask_login.current_user.fedi_auth:
        fedi_auth = flask_login.current_user.get_fedi_auth()
        if fedi_auth: # successfully decrypted
            wtform.title.data = flask_login.current_user.fedi_connection_title()
            wtform.node_url.data = fedi_auth['node_url']
            wtform.access_token.data = fedi_auth['access_token']
    return render_template('fediverse-config.html', wtform=wtform)


@user_bp.route('/user/<string:username>/fediverse/delete-auth', methods=['POST'])
@auth.enabled_user_required
def fediverse_delete(username):
    if username != flask_login.current_user.username:
        return redirect(url_for('user_bp.fediverse_delete',
                                username=flask_login.current_user.username))
    flask_login.current_user.fedi_auth = {}
    flask_login.current_user.save()
    flash(_("Fediverse configuration deleted OK"), 'success')
    return redirect(url_for('user_bp.user_settings',
                            username=flask_login.current_user.username))


@user_bp.route('/user/<int:user_id>/delete-account', methods=['GET', 'POST'])
@auth.enabled_user_required
def delete_account(user_id):
    user = User.find(id=user_id)
    if not user or user.id != flask_login.current_user.id:
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    if flask_login.current_user.is_admin() and User.find_all(role='admin').count() == 1:
        flash(_("Cannot delete. You are the only Admin on this site"), 'warning')
        return redirect(url_for('user_bp.user_settings',
                                username=flask_login.current_user.username))
    wtform=wtf.DeleteAccount()
    if wtform.validate_on_submit():
        flask_login.current_user.delete_user()
        flask_login.logout_user()
        flash(_("Thank you for using LiberaForms"), 'success')
        return redirect(url_for('main_bp.index'))
    return render_template('delete-account.html', wtform=wtform)


@user_bp.route('/user/toggle-new-answer-notification', methods=['POST'])
@auth.enabled_user_required__json
def toggle_new_answer_notification_default():
    return jsonify(default=flask_login.current_user.toggle_new_answer_notification_default())


@user_bp.route('/user/hide-edit-mode-alert', methods=['POST'])
@auth.enabled_user_required__json
def toggle_edit_mode_reminder():
    """Toggle the edit-mode reminder modal displayed when editing public Forms."""
    preference = not flask_login.current_user.preferences['show_edit_alert']
    flask_login.current_user.preferences['show_edit_alert'] = preference
    flask_login.current_user.save()
    return jsonify(default=preference)


@user_bp.route('/user/validate-email/<string:token>', methods=['GET'])
@tokens.sanitized_token
def validate_email(token):
    """Use to validate a New user's email or an existing user's Change email request.

    Note that a user has recieved an email including a link to this route.
    """
    user = User.find(token=token)
    if not user:
        flash(_("We couldn't find that petition"), 'warning')
        return redirect(url_for('main_bp.index'))
    if tokens.has_token_expired(user.token):
        flash(_("Your petition has expired"), 'warning')
        user.delete_token()
        return redirect(url_for('main_bp.index'))
    # On a Change email request, the new email address is saved in the token.
    if 'email' in user.token:
        user.email = user.token['email']

    user.delete_token()
    user.validated_email=True
    user.save()
    #login the user
    flask_login.login_user(user)
    flash(_("Your email address is valid"), 'success')
    return redirect(url_for('user_bp.user_settings',
                            username=user.username))


@user_bp.route('/user/<string:username>/consent', methods=['GET'])
@auth.enabled_user_required
def consent(username):
    if username != flask_login.current_user.username:
        return redirect(url_for('user_bp.consent',
                                username=flask_login.current_user.username))
    return render_template('user/consent.html')


## Login / Logout


@user_bp.route('/user/login', methods=['GET', 'POST'])
@user_bp.route('/user/login/<string:invite>', methods=['GET', 'POST'])
@auth.anonymous_user_required
@tokens.instantiate_invite
def login(invite=None):
    next_url = request.args.get('next', None)
    wtform=wtf.Login()
    if wtform.validate_on_submit():
        domain = UserDomain(wtform.username.data, wtform.password.data)
        user = domain.get_user()
        if not user and current_app.config['ENABLE_LDAP']:
            user = domain.create_user_from_ldap()
        if user:
            if domain.validate_user():
                if invite and invite.email == user.email:
                    if invite.granted_form:
                        domain.grant_form_answers(invite)
                    invite.delete()
                flask_login.login_user(user)
                if not user.validated_email:
                    return redirect(url_for('user_bp.user_settings',
                                            username=user.username))
                return redirect(next_url or url_for("form_bp.my_forms"))
        utils.fake_a_login()
        current_app.logger.info('Failed login')
        flash(_("Bad credentials"), 'warning')
    if invite:
        wtform.username.data = invite.email
    return render_template('login.html',
                            wtform=wtform,
                            next=next_url,
                            token=invite.token['token'] if invite else None)


@user_bp.route('/user/logout', methods=['GET', 'POST'])
@auth.authenticated_user_required
def logout():
    flask_login.logout_user()
    return redirect(url_for('main_bp.index'))
