"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os, re
import mimetypes, pytz
from flask_wtf import FlaskForm
from wtforms import (StringField, TextAreaField, IntegerField, SelectField,
                     PasswordField, BooleanField, RadioField, FileField,
                     HiddenField, URLField, EmailField)
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length
from flask import current_app, g
from flask_babel import gettext
from flask_babel import lazy_gettext as _
import flask_login

from liberaforms.models.user import User
from liberaforms.models.form import Form
from liberaforms.utils import sanitizers
from liberaforms.utils import utils
from liberaforms.utils import validators


""" Password validation not handled by WTForms
"""
def raise_password_strength_errors(password):
    failed = validators.test_password(password)
    if not failed:
        return
    if 'special' in failed:
        # i18n: message displayed while new user is deciding their password. Advices to use at least some ammount (number) of special characters on it
        raise ValidationError(_("Please use at least {number} special character"
                                .format(number=failed['special'])))
    if 'strength' in failed:
        # i18n: message displayed while new user is deciding their password.
        only_scored=_("Your password only scored {number}%."
                    .format(number=failed['strength']))
        # i18n: Refers to password
        make_longer=_("Please make it longer.")
        raise ValidationError(f"{only_scored} {make_longer}")

class NewUser(FlaskForm):
    username = StringField(_("Username"), validators=[DataRequired(),
                                                      Length(min=4, max=16)])
    email = EmailField(_("Email"), validators=[DataRequired(), Email()])
    password = PasswordField(_("Password (min 8 characters)"), validators=[DataRequired(),
                                                        Length(min=8, max=128)])
    password2 = PasswordField(_("Password again"), validators=[EqualTo('password'),
                                                               DataRequired()])
    termsAndConditions = BooleanField()
    DPLConsent = BooleanField()

    def validate_username(self, username):
        if username.data != sanitizers.sanitize_username(username.data):
            raise ValidationError(_("Username is not valid"))
            return False
        if username.data in current_app.config['RESERVED_USERNAMES']:
            raise ValidationError(_("Please use a different username"))
            return False
        if User.find(username=username.data):
            raise ValidationError(_("Please use a different username"))

    def validate_email(self, email):
        if User.find(email=email.data):
            raise ValidationError(_("Please use a different email address"))

    def validate_password(self, password):
        if not password.errors: # no builtin WTForms validator errors
            raise_password_strength_errors(password.data)

    def validate_termsAndConditions(self, termsAndConditions):
        if g.site.terms_and_conditions['enabled'] and not termsAndConditions.data:
            raise ValidationError(_("Please accept our terms and conditions"))

    def validate_DPLConsent(self, DPLConsent):
        if g.site.get_consent().enabled and not DPLConsent.data:
            raise ValidationError(_("Please accept our data protection policy"))


class Login(FlaskForm):
    username = StringField(_("Username"), validators=[DataRequired()])
    password = PasswordField(_("Password"), validators=[DataRequired()])

    def validate_username(self, username):
        if username.data != sanitizers.sanitize_username(username.data):
            return False


class DeleteAccount(FlaskForm):
    delete_username = StringField(_("Enter your username"), validators=[DataRequired()])
    delete_password = PasswordField(_("Enter your password"), validators=[DataRequired()])

    def validate_delete_username(self, delete_username):
        if delete_username.data != flask_login.current_user.username:
            raise ValidationError(_("That is not your username"))

    def validate_delete_password(self, delete_password):
        if not validators.verify_password(delete_password.data,
                                          flask_login.current_user.password_hash):
            raise ValidationError(_("That is not your password"))


class GetEmail(FlaskForm):
    email = EmailField(_("Email address"), validators=[DataRequired(), Email()])


class ChangeEmail(FlaskForm):
    email = EmailField(_("New email address"), validators=[DataRequired(), Email()])

    def validate_email(self, email):
        if User.find(email=email.data) or email.data == current_app.config['ROOT_USER']:
            raise ValidationError(_("Please use a different email address"))

class ResetPassword(FlaskForm):
    password = PasswordField(_("Password"), validators=[DataRequired(),
                                                        Length(min=8, max=128)])
    password2 = PasswordField(_("Password again"), validators=[EqualTo('password')])

    def validate_password(self, password):
        if not password.errors: # no builtin WTForms validator errors
            raise_password_strength_errors(password.data)

class smtpConfig(FlaskForm):
    host = StringField(_("Email server"), validators=[DataRequired()])
    port = IntegerField(_("Port"))
    encryption = SelectField(_("Encryption"), choices=[
                                                    ('None', 'None'),
                                                    ('SSL', 'SSL'),
                                                    ('STARTTLS', 'STARTTLS')
                                                    ])
    user = StringField(_("User"))
    password = StringField(_("Password"))
    noreplyAddress = StringField(_("Sender address"), validators = [
                                                                DataRequired(),
                                                                Email()
                                                            ])

class NewInvite(FlaskForm):
    email = EmailField(_("New user's email"), validators=[DataRequired(), Email()])
    message = TextAreaField(_("Include message"), validators=[DataRequired()])
    role = HiddenField()
    granted_form_id = HiddenField()

    def validate_message(self, message):
        message.data=sanitizers.bleach_text(message.data)
        if not message.data:
            # message was present but bleach return empty string
            raise ValidationError(_("That text was not valid"))
    def validate_email(self, email):
        if User.find(email=email.data) or email.data == current_app.config['ROOT_USER']:
            raise ValidationError(_("Please use a different email address"))
    def validate_role(self, role):
        if not role.data in ('guest', 'editor', 'admin'):
            raise ValidationError("Not a valid role")
    def validate_granted_form_id(self, granted_form_id):
        if granted_form_id.data:
            try:
                granted_form_id.data = int(granted_form_id.data)
                if not Form.find(id=granted_form_id.data):
                    raise ValidationError("Not a valid form id")
            except:
                raise ValidationError("Not a valid form id")

class ChangePrimaryColor(FlaskForm):
    hex_color = StringField(_("HTML color code"), validators=[DataRequired()])
    def validate_hex_color(self, hex_color):
        if not validators.is_hex_color(hex_color.data):
            raise ValidationError(_("Not a valid HTML color code"))

class ChangeTimeZone(FlaskForm):
    timezone = StringField(_("Time zone"), validators=[DataRequired()])
    def validate_timezone(self, timezone):
        if not timezone.data in pytz.common_timezones:
            raise ValidationError(_("Not a valid time zone"))

class FileExtensions(FlaskForm):
    # i18n: Refers to file extensions, such as .odt or .pdf
    extensions = TextAreaField(_("Extensions"), validators=[DataRequired()])
    def validate_extensions(self, extensions):
        mimetypes.init()
        for ext in extensions.data.splitlines():
            if not ext:
                continue
            mimetype, encoding = mimetypes.guess_type(f"file_name.{ext}")
            if not mimetype:
                raise ValidationError(_("Unknown file extension: %s" % ext))

""" Posted via JSON
"""
class UploadMedia(FlaskForm):
    alt_text = StringField(_("Descriptive text"),
                            validators=[DataRequired(), Length(min=12, max=100)])
    media_file = FileField(_("Select a file")) # not required at form/_image-modal
    def validate_media_file(form, field):
        if not field.data:
            raise ValidationError(gettext("File required"))
        if not "image/" in field.data.content_type:
            raise ValidationError(gettext("Not a valid image file"))
        field.data.seek(0, os.SEEK_END)
        file_size = field.data.tell()
        field.data.seek(0, 0)
        max_size = current_app.config['MAX_MEDIA_SIZE']
        if file_size > max_size:
            max_size = utils.human_readable_bytes(max_size)
            # i18n: %s is a digital size. Example: 50 MB
            err_msg = gettext("File too big. Maximum size is %s" % max_size)
            raise ValidationError(err_msg)

class UserUploadLimit(FlaskForm):
    size = StringField(validators=[DataRequired()])
    unit = StringField(validators=[DataRequired()])
    def validate_size(form, field):
        try:
            size = float(field.data)
        except:
            raise ValidationError(_("Must be a number"))
        if size <= 0:
            raise ValidationError(_("Must be greater the zero"))

class FediverseAuth(FlaskForm):
    title = StringField(_("Connection name"), validators=[DataRequired()])
    node_url = URLField(_("Fediverse node"),
                          default="",
                          validators=[DataRequired()])
    access_token = HiddenField(_("Access token"),
                               default="",
                               validators=[DataRequired()])
    def validate_title(form, field):
        field.data=sanitizers.remove_html_tags(field.data)
        if not field.data:
            raise ValidationError(_("That text was not valid"))

class FormPublish(FlaskForm):
    image_source = StringField()
    text = TextAreaField(validators=[DataRequired()])

class FormShortDescription(FlaskForm):
    short_desc = TextAreaField(_("Short description"), validators=[DataRequired()])

class ContactInformation(FlaskForm):
    contact_info = TextAreaField()
    def validate_contact_info(form, field):
        if field.data:
            field.data=sanitizers.remove_html_tags(field.data)
            if not field.data:
                raise ValidationError(_("That text was not valid"))

class NewFormMessage(FlaskForm):
    msg = StringField(_("Short message"))
    language = SelectField(_("Language"), validators=[DataRequired()])
    def validate_msg(form, field):
        if field.data:
            if not sanitizers.bleach_text(field.data):
                raise ValidationError(_("That text was not valid"))
    def validate_language(form, field):
        if field.data not in current_app.config['LANGUAGES'].keys():
            raise ValidationError(_("Not a valid language"))
