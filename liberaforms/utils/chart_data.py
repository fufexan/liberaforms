"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import typing
from datetime import datetime, timezone, timedelta
from dateutil.relativedelta import relativedelta
from flask_babel import gettext as _
from liberaforms.models.form import Form
from liberaforms.models.answer import Answer
from liberaforms.models.user import User
from liberaforms.utils.utils import utc_to_g_timezone


def get_site_statistics() -> dict:
    now = utc_to_g_timezone(datetime.now(timezone.utc))
    today = now.strftime("%Y-%m")
    one_year_ago = now - timedelta(days=354)
    year, month = one_year_ago.strftime("%Y-%m").split("-")
    month = int(month)
    year = int(year)
    result:typing.Dict={
        "labels":[],
        "answers":[],
        "forms":[],
        "users":[],
        "total_answers":[],
        "total_forms": [],
        "total_users":[]
    }
    while 1:
        month = month +1
        if month == 13:
            month = 1
            year = year +1
        two_digit_month="{0:0=2d}".format(month)
        year_month = f"{year}-{two_digit_month}"
        result['labels'].append(year_month)
        if year_month == today:
            break
    total_answers=0
    total_forms=0
    total_users=0
    for year_month in result['labels']:
        date_str = year_month.replace('-', ', ')
        start_date = datetime.strptime(date_str, '%Y, %m')
        stop_date = start_date + relativedelta(months=1)
        monthy_users = User.query.filter(
                                User.created >= start_date,
                                User.created < stop_date).count()
        monthy_forms = Form.query.filter(
                                Form.created >= start_date,
                                Form.created < stop_date).count()
        monthy_answers = Answer.query.filter(
                                Answer.created >= start_date,
                                Answer.created < stop_date).count()
        total_answers = total_answers + monthy_answers
        total_forms= total_forms + monthy_forms
        total_users = total_users + monthy_users
        result['answers'].append(monthy_answers)
        result['forms'].append(monthy_forms)
        result['users'].append(monthy_users)
        result['total_answers'].append(total_answers)
        result['total_forms'].append(total_forms)
        result['total_users'].append(total_users)
    return result


def get_user_statistics(user, year="2020"):
    now = utc_to_g_timezone(datetime.now(timezone.utc))
    today = now.strftime("%Y-%m")
    one_year_ago = now - timedelta(days=354)
    year, month = one_year_ago.strftime("%Y-%m").split("-")
    month = int(month)
    year = int(year)
    result={    "labels":[], "answers":[], "forms":[],
                "total_answers":[], "total_forms": []}
    while 1:
        month = month +1
        if month == 13:
            month = 1
            year = year +1
        two_digit_month="{0:0=2d}".format(month)
        year_month = f"{year}-{two_digit_month}"
        result['labels'].append(year_month)
        if year_month == today:
            break
    total_answers=0
    total_forms=0
    answer_filter=[Answer.author_id == user.id]
    form_filter=[Form.author_id == user.id]
    for year_month in result['labels']:
        date_str = year_month.replace('-', ', ')
        start_date = datetime.strptime(date_str, '%Y, %m')
        stop_date = start_date + relativedelta(months=1)
        monthy_users = User.query.filter(
                                User.id == user.id,
                                User.created >= start_date,
                                User.created < stop_date).count()
        monthy_forms = Form.query.filter(
                                Form.author_id == user.id,
                                Form.created >= start_date,
                                Form.created < stop_date).count()
        monthy_answers = Answer.query.filter(
                                Answer.author_id == user.id,
                                Answer.created >= start_date,
                                Answer.created < stop_date).count()
        total_answers = total_answers + monthy_answers
        total_forms= total_forms + monthy_forms
        result['answers'].append(monthy_answers)
        result['forms'].append(monthy_forms)
        result['total_answers'].append(total_answers)
        result['total_forms'].append(total_forms)
    return result


def answers_chart_data(form) -> dict:
    """Generate data for chartjs display.

    # Some of this could be done with sql, but we'll be doing this on the client
    # in the future anyway.
    """
    answers = form.get_answers_for_display(oldest_first=True)
    answers_length = len(answers)
    number_fields = form.get_number_fields()
    multichoice_fields = form.get_multichoice_fields()
    time_chart=[]
    number_charts=[]
    multi_choice_charts=[]
    for field in number_fields:
        chart={"name": field['name'],
               "title": field['label'],
               "answer_cnt": 0,
               "tally": 0,
               "axis_x": [_('Minimum'), _('Maximum'), _('Average')],
               "axis_y": [0, 0, 0]}
        number_charts.append(chart)
    for field in multichoice_fields:
        chart={"name": field['name'],
               "title": field['label'],
               "axis_x": [],
               "axis_y": []}
        for value in field['values']:
            chart['axis_x'].append(value['label'])
            chart['axis_y'].append(0) #start counting at zero
        multi_choice_charts.append(chart)
    time_unit = 'hour'
    time_span = ""
    frmt = "%Y-%m-%d %H:%M:%S"
    if answers_length == 1:
        created_time = datetime.strptime(answers[0]['created'], frmt)
        time_span = created_time.strftime("%Y-%m-%d")
    elif answers_length > 1:
        start_time = datetime.strptime(answers[0]['created'], frmt)
        stop_time = datetime.strptime(answers[-1]['created'], frmt)
        start_str = start_time.strftime("%Y-%m-%d")
        stop_str = stop_time.strftime("%Y-%m-%d")
        time_span = start_str if start_str == stop_str else  f"{start_str} → {stop_str}"
        time_delta = stop_time - start_time
        if time_delta <= timedelta(minutes=1):
            time_unit = 'second'
        elif time_delta <= timedelta(minutes=60):
            time_unit = 'minute'
        elif time_delta <= timedelta(days=1):
            time_unit = 'hour'
        elif time_delta <= timedelta(days=7):
            time_unit = 'day'
        elif time_delta <= timedelta(days=30):
            time_unit = 'week'
        elif time_delta <= timedelta(days=570):
            time_unit = 'month'
        else:
            time_unit = 'year'
    answer_cnt=0
    for answer in answers:
        answer_cnt += 1
        time_chart.append({'x': answer['created'], 'y': answer_cnt})
        # build number stats
        for field in number_fields:
            try:
                field_value = int(answer[field['name']])
            except:
                continue
            chart=[item for item in number_charts if item["name"]==field['name']][0]
            if 'min' in chart:
                if field_value < chart['min']:
                    chart['min'] = field_value
                if field_value > chart['max']:
                    chart['max'] = field_value
            else:
                chart['min'] = field_value
                chart['max'] = field_value
            chart['tally'] = chart['tally'] + field_value
            chart['answer_cnt'] += 1
        for chart in number_charts:
            try:
                chart['avg'] = round((chart['tally'] / chart['answer_cnt']), 2)
                chart['axis_y']=[chart['min'], chart['max'], chart['avg']]
            except:
                chart['axis_y']=[0, 0, 0]
        # build multi-choice stats
        for field in multichoice_fields:
            if not (field['name'] in answer and answer[field['name']]):
                continue
            chart=[item for item in multi_choice_charts if item["name"]==field['name']][0]
            answer_values=answer[field['name']].split(', ')
            for idx, field_value in enumerate(field['values']):
                if field_value['value'] in answer_values:
                    chart['axis_y'][idx]+=1

    return {'multi_choice_charts': multi_choice_charts,
            'number_charts': number_charts,
            'time_chart': time_chart,
            'time_unit': time_unit,
            'time_span': time_span,
            'total_answers': answers_length}
