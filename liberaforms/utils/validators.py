"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import re, datetime, time, uuid
from email_validator import validate_email, EmailNotValidError
from passlib.hash import pbkdf2_sha256
import password_strength
from flask import current_app


def is_valid_email(email:str) -> bool:
    """Check email address string.

    check_deliverability does DNS lookup.
    """
    try:
        check = not (current_app.config['TESTING'] or \
                     os.environ['FLASK_CONFIG'] == "development")
        validate_email(email, check_deliverability=check)
        return True
    except EmailNotValidError as error:
        #current_app.logger.warning(error)
        return False

def test_password(password:str) -> dict:
    pwd_policy = password_strength.PasswordPolicy.from_names(
        length=0,  # length is handled by WTForms
        uppercase=0,  # need min. uppercase letters
        numbers=0,  # need min. digits
        special=1,  # need min. special characters
        nonletters=0,  # need min. non-letter characters (digits, specials, anything)
        strength=0.5
    )
    result = {}
    policy_password = pwd_policy.password(password)
    for test in policy_password.test(): # failed tests
        if isinstance(test, password_strength.tests.Special):
            result['special']=test.count # required special chars
            continue
        if isinstance(test, password_strength.tests.Strength):
            try:
                strength=int(policy_password.strength() * 100)
            except:
                strength=0
            if strength < int(test.strength * 100):
                result['strength']=strength
    return result

def hash_password(password:str) -> str:
    """Encrypt a password."""
    settings = {
        'rounds': 200000,
        'salt_size': 16,
    }
    return pbkdf2_sha256.using(**settings).hash(password)

def verify_password(password, hash:str):
    """Compare password with hash."""
    return pbkdf2_sha256.verify(password, hash)

def is_hex_color(color:str) -> bool:
    """Ckeck for valid HTML color."""
    return bool(re.search("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", color))

def is_valid_UUID(value) -> bool:
    """Ckeck for valid UUID."""
    try:
        uuid.UUID(value)
        return True
    except ValueError:
        return False

def is_valid_date(date) -> bool:
    """Ckeck for valid date."""
    try:
        datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
        return True
    except:
        return False

def is_future_date(date) -> bool:
    """Return True when date is in the future."""
    now=time.time()
    future=int(datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S").strftime("%s"))
    return future > now

def compare_field_index(index_1:list, index_2:list) -> bool:
    """Compare keys and values of two form field indexes."""
    if not (isinstance(index_1, list) and isinstance(index_2, list)):
        return False
    if len(index_1) != len(index_2):
        return False
    for field in index_1:
        if not field in index_2:
            return False
    return True
