"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json, string, random, datetime, uuid, pytz
from pprint import pformat
from flask import Response, redirect, request
from flask import current_app, has_request_context, g, session
from flask_babel import gettext as _
import flask_login
from liberaforms import babel
from liberaforms.utils import validators


def print_obj_values(obj):
    """Print vars and values of an object."""
    values={}
    obj_vars = vars(obj)
    for var in obj_vars:
        values[var] = obj_vars[var]
    return pformat({obj.__class__.__name__: values})

@babel.localeselector
def get_locale() -> str:
    """Return best locale."""
    if flask_login.current_user.is_authenticated:
        return flask_login.current_user.language
    locale = request.accept_languages.best_match(current_app.config['LANGUAGES'].keys())
    return locale if locale else "en"

#@flask_login.user_logged_in(app, user)
def populate_flask_g() -> None:
    """Load request context with global variables."""
    from liberaforms.models.site import Site
    g.is_admin=False
    g.is_editor=False
    g.embedded=False
    g.site=Site.find()
    g.timezone = pytz.timezone(current_app.config['DEFAULT_TIMEZONE'])
    if flask_login.current_user.is_authenticated:
        if flask_login.current_user.enabled:
            g.is_editor = flask_login.current_user.is_editor()
            g.is_admin = flask_login.current_user.is_admin()
        g.timezone = pytz.timezone(flask_login.current_user.get_timezone())
    g.language = get_locale()

def utc_to_g_timezone(utc_datetime):
    """Timezone adjust a datetime."""
    return utc_datetime.astimezone(g.timezone)

def get_app_version() -> str:
    """Return LiberaForms version."""
    try:
        with open('VERSION.txt') as f:
            app_version = f.readline().strip()
            return app_version if app_version else ""
    except Exception as error:
        current_app.logger.error(error)
        return ""

######## Other ########

def gen_random_string():
    return uuid.uuid4().hex

def str2bool(v):
    """Convert a string to a bool type."""
    return v.lower() in ("true", "1", "yes")

def human_readable_bytes(bytes_cnt:int) -> str:
    """Convert bytes into human readable string.

    1 KibiByte == 1024 Bytes
    1 Mebibyte == 1024*1024 Bytes
    1 GibiByte == 1024*1024*1024 Bytes
    """
    if bytes_cnt < 1024:
        if has_request_context():
            return _("{number} bytes").replace('{number}', str(bytes_cnt))
        else:
            # when called from our CLI
            # flask_babel.gettext throws "Working outside of request context" error
            return f"{bytes_cnt} bytes"
    if bytes_cnt < 1024*1024:
        return f"{float(round(bytes_cnt/(1024), 2))} KB"
    if bytes_cnt < 1024*1024*1024:
        return f"{float(round(bytes_cnt/(1024*1024), 2))} MB"
    return f"{float(round(bytes_cnt/(1024*1024*1024), 2))} GB"

def string_to_bytes(bytes_string) -> int:
    """Convert a string to an integer.

    String format: eg. "123 GB"
    """
    try:
        size = float(bytes_string[:-3])
    except:
        return 0
    unit = bytes_string[-2:]
    if unit == 'KB':
        return size * 1024
    if unit == 'MB':
        return size * 1024*1024
    if unit == 'GB':
        return size * 1024*1024*1024
    return 0

def get_fuzzy_duration(start_time) -> str:
    """Return human friendly time difference between start_time and now."""
    now = datetime.datetime.now(datetime.timezone.utc)
    start_time = datetime.datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S.%f%z")
    duration = now - start_time
    days, seconds = duration.days, duration.seconds
    hours = days * 24 + seconds // 3600
    minutes = (seconds % 3600) // 60
    seconds = seconds % 60
    if days > 0:
        return _("{number} days").replace('{number}', str(days))
    if hours > 0:
        return _("{number} hours").replace('{number}', str(hours))
    if minutes > 0:
        return _("{number} minutes").replace('{number}', str(minutes))
    return _("{number} seconds").replace('{number}', str(seconds))

def fake_a_login():
    # Prevent Timing Side Channel User Enumeration
    fake_hash="$pbkdf2-sha256$200000$5ZyTEgJAaC0lJIQwphSidA$smHO8ewaBgzizP1x36REoLyGLPJ16mtt5YWTku2mws0"
    validators.verify_password(gen_random_string(), fake_hash)
