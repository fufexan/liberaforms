"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import g
from liberaforms.models.consent import Consent
from liberaforms.models.schemas.consent import ConsentSchema
from liberaforms.utils import utils
from liberaforms.utils import sanitizers


from pprint import pprint

class ConsentMixin:
    """Provide resuable code for Site, and Form models."""

    def _id_criteria(self) -> dict:
        return {
            f"{self.__class__.__name__.lower()}_id": self.id
        }

    def _insert(self, kwargs) -> Consent:
        if self.__class__.__name__ == "Form":
            consent=Consent.find(site_id=g.site.id, shared=True)
            if consent:
                kwargs['enabled'] = True # True because site consent is shared
        consent = Consent(**kwargs)
        consent.save()
        return consent

    #def get_raw(self):
    #    #print('mixin _id_criteria(): ', self._id_criteria())
    #    return Consent.find(**self._id_criteria())

    def get_consent(self) -> Consent:
        """Return a Consent object.

        Ensure markdown and html properties are not empty.
        Return a default object if not present in database."""
        consent = Consent.find(**self._id_criteria())
        if consent:
            if not consent.markdown:
                default = self.get_default_consent()
                consent.markdown = default.markdown
                consent.html = default.html
            return consent
        return self.get_default_consent()

    def get_default_consent(self) -> Consent:
        """Return the default Consent object."""
        if self.__class__.__name__ == "Form":
            consent=Consent.find(site_id=g.site.id, shared=True)
            if consent:
                default = Consent.get_default_consent()
                kwargs = {
                    'enabled': True, # True because site consent is shared
                    'markdown': consent.markdown if consent.markdown else default.markdown,
                    'html': consent.html if consent.html else default.html,
                }
                return Consent(**kwargs)
                #if not consent.markdown:
                #    default = Consent.get_default_consent()
                #    consent.markdown = default.markdown
                #    consent.html = default.html
                #consent.enabled = True
                #return consent
        return Consent.get_default_consent()

    def get_consent_for_rendering(self): #, default_values:bool=False) -> dict:
        """Return ready to be rendered values."""
        #if default_values:
        #    return ConsentSchema().dump(self.get_default_consent())
        return ConsentSchema().dump(self.get_consent())

    def save_consent(self, data:dict) -> Consent:
        """Save consent.

        Values that equal default values are set to "" for i18n purposes.
        """
        default_consent = self.get_default_consent()
        markdown = sanitizers.remove_html_tags(data['markdown'])
        if markdown != "" and markdown == default_consent.markdown:
            markdown = ""
            html = ""
        else:
            html = sanitizers.markdown2HTML(markdown)
        checkbox_text = sanitizers.remove_html_tags(data['checkbox_text'])
        if checkbox_text != "" and checkbox_text == default_consent.checkbox_text:
            checkbox_text = ""
        kwargs = {
            'checkbox_text': checkbox_text,
            'markdown': markdown,
            'html': html
        }
        if 'required' in data:
            kwargs['required'] = utils.str2bool(data['required'])
        consent = Consent.find(**self._id_criteria())
        if not consent:
            kwargs = {**self._id_criteria(), **kwargs}
            consent = self._insert(kwargs)
        else:
            if 'title' in data:
                title = sanitizers.remove_html_tags(data['title'])
                if consent.title != title:
                    consent.title = title
            for key, value in kwargs.items():
                setattr(consent, key, value)
            consent.save()
        return consent

    def toggle_consent_enabled(self) -> bool:
        consent = Consent.find(**self._id_criteria())
        if not consent:
            consent = self._insert(self._id_criteria())
        consent.enabled = not consent.enabled
        consent.save()
        return consent.enabled

    def toggle_consent_shared(self) -> bool:
        consent = Consent.find(**self._id_criteria())
        if not consent:
            consent = self._insert(self._id_criteria())
        consent.shared = not consent.shared
        consent.save()
        return consent.shared
