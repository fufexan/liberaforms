"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from flask import current_app, g
from flask_babel import gettext as _
from threading import Thread

from email.header import Header
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from liberaforms.models.site import Site
from liberaforms.models.user import User
from liberaforms.utils.dispatcher.email import EmailServer
from liberaforms.utils.dispatcher.fediverse import FediPublisher
from liberaforms.utils import sanitizers



class Dispatcher(EmailServer):
    def __init__(self):
        self.site = Site.find()
        EmailServer.__init__(self, self.site)

    def create_multipart_message(self, text_body, html_body):
        text_part = MIMEText(text_body, _subtype='plain', _charset='UTF-8')
        html_part = MIMEText(html_body, _subtype='html', _charset='UTF-8')
        message = MIMEMultipart("alternative")
        message.attach(text_part)
        message.attach(html_part)
        return message

    def send_test_email(self, recipient_email):
        body = _("Congratulations!")
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("MAIL test")).encode()
        message['To'] = recipient_email
        status = self.send_mail(message, [recipient_email])
        return status

    def send_invitation(self, invite):
        link = invite.get_link()
        text_body = f"{invite.message}\n\n{link}"
        message = MIMEText(text_body, _subtype='plain', _charset='UTF-8')
        header = Header(_("Invitation to LiberaForms"))
        message['Subject'] = header.encode()
        message['To'] = invite.email
        status = self.send_mail(message, [invite.email])
        return status

    def send_invitation_accepted(self, inviter, invited_user):
        # i18n: %s is an email address
        body = _("Your invitation has been accepted by %s" % invited_user.email)
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        # i18n: %s stands for sitename
        message['Subject'] = Header(_("%s new user notification" % self.site.name)).encode()
        message['To'] = inviter.email
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, [inviter.email]]
        )
        thr.start()

    def send_account_recovery(self, user):
        link = f"{current_app.config['BASE_URL']}/site/recover-password/{user.token['token']}"
        body = _("Please use this link to create a new password")
        body = f"{body}\n\n{link}"
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("LiberaForms. Recover password")).encode()
        message['To'] = user.email
        # thread smtp to Prevent Timing Side Channel User Enumeration
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, [user.email]]
        )
        thr.start()

    def send_email_address_confirmation(self, user, email_to_validate):
        host_url=current_app.config['BASE_URL']
        link = f"{host_url}/user/validate-email/{user.token['token']}"
        body = _("Use this link to validate your email")
        body = f"{body}\n\n{link}"
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("LiberaForms. Email validation")).encode()
        message['To'] = email_to_validate
        status = self.send_mail(message, [email_to_validate])
        return status

    def send_new_user_notification(self, user):
        admins = User.find_all( role='admin',
                                notifyNewUser=True,
                                validated_email=True,
                                blocked=False)
        admin_emails = [admin.email for admin in admins]
        if not admin_emails:
            return
        # i18n: First %s is username. Second %s is sitename
        body = _("New user '%s' created at %s" % (  user.username,
                                                    self.site.name))
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("LiberaForms. New user notification")).encode()
        message['To'] = ', '.join(admin_emails)
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, admin_emails]
        )
        thr.start()

    def send_new_form_notification(self, form):
        admins = User.find_all( role='admin',
                                notifyNewForm=True,
                                validated_email=True,
                                blocked=False)
        admin_emails = [admin.email for admin in admins]
        if not admin_emails:
            return
        body = _("New form '%s' created at %s" % (form.slug, self.site.name))
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("LiberaForms. New form notification")).encode()
        message['To'] = ', '.join(admin_emails)
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, admin_emails]
        )
        thr.start()

    def send_new_answer_notification(self, formuser_emails, slug):
        # i18n: First %s is form name. Second %s is sitename
        body = _("Hello,\n\nYour form '%s' at '%s' has been answered." \
                % (slug, self.site.name))
        answer_link=f"{current_app.config['BASE_URL']}/{slug}/answers"
        body = f"{body}\n\n{answer_link}"
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("LiberaForms. New form answer")).encode()
        message['To'] = ', '.join(formuser_emails)
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, formuser_emails]
        )
        thr.start()

    def send_answer_confirmation(self, anon_email, form):
        html_body=form.get_after_submit_text_html()
        text_body=sanitizers.remove_html_tags(html_body)
        message = self.create_multipart_message(text_body, html_body)
        message['Subject'] = Header(_("Confirmation message")).encode()
        message['To'] = anon_email
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, [anon_email]]
        )
        thr.start()

    def send_expired_form_notification(self, formuser_emails, form):
        # i18n: First %s is form name. Second %s is sitename
        body = _("The form '%s' has expired at %s" % (form.slug, self.site.name))
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header(_("LiberaForms. A form has expired")).encode()
        message['To'] = ', '.join(formuser_emails)
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(), message, formuser_emails]
        )
        thr.start()

    def send_error(self, error):
        if not current_app.config["ALERT_MAILS"]:
            current_app.logger.info('Cannot dispatch error. No ALERT_MAILS')
            return
        message = MIMEText(error, _subtype='plain', _charset='UTF-8')
        message['Subject'] = Header("Error at %s" % self.site.name).encode()
        message['To'] = ', '.join(current_app.config["ALERT_MAILS"])
        thr = Thread(
            target=self.send_mail_async,
            args=[current_app._get_current_object(),
                  message,
                  current_app.config["ALERT_MAILS"]
            ]
        )
        thr.start()

    def send_message(self, emails, subject, body):
        body = sanitizers.remove_html_tags(body)
        message = MIMEText(body, _subtype='plain', _charset='UTF-8')
        subject = f"Message from {self.site.name}: {subject}"
        message['Subject'] = Header(subject).encode()
        message['To'] = ', '.join(emails)
        status = self.send_mail(message, emails)
        return status

    def publish_form(self, text, img_src, fediverse=True):
        published, msg = FediPublisher().publish(text, img_src)
        return {"published": published, "msg": msg}
