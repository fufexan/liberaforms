��    �      �              	  C   	  #   Q	     u	     |	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     �	     

     
     
     &
     6
  	   =
     G
     T
     h
     p
     �
     �
     �
     �
     �
     �
     �
     �
     �
          &     <     D  
   M     X     x     �  	   �     �     �     �     �     �     �  
   �     �     �  	   �     �     �       	   '  
   1     <     B     R     W     ^  7   d  '   �     �     �     �     �            
        *     0     9     E     K     P     W     e     l     t     z     �     �     �  
   �     �  	   �     �     �     �     �     	           .  
   5  	   @     J     \     k     p     u     �     �     �     �     �  	   �     �     �  
             $     9     E     [  J   q     �     �     �     �     �     �               #     5  
   B     M     V  $   \     �     �     �  2   �      �  '   �  0   "  1   S     �     �     �  �  �  N   �  #   �     #     )     <     J  	   `  '   j     �     �     �     �     �     �     �  	   �     �            
        $     2     J     S     c     p     |     �     �     �     �     �     �  $   �          9     B     O  %   \  
   �     �     �     �     �     �     �     �  
   	               &  	   ,     6     B  '   Z     �     �     �     �     �  	   �  	   �  =   �  2   *  
   ]     h     u     �     �     �     �     �     �     �     �     �     �  
                       6     >     P     b     g     u     �     �  $   �     �     �  $   �          '     0  	   =     G     W     p     y     �     �     �     �     �     �            #   %     I      V     w  
   �     �     �  R   �          $     7     J  
   `     k     �     �     �     �     �     �  	   �  ,        .     K     S  6   h  !   �  +   �  /   �  .     )   L     v     �    * Describe your form.
 * Add relevant content, links, images, etc. %(total_uploads)s of %(disk_limit)s Active Add editors Admin All forms Answers Answers shared with %(number)s Are you sure? Attachment types Attachments Author Average Blocked by admin Cancel Change Change author Change language Config Configure Confirmation Contact information Context Create an account Created Current author Current author's username Data protection Default language Default timezone Delete Delete answers Delete user Delete user and authored forms Delete user and forms Details Disabled Disk usage Dont show me this message again Download CSV Edit Edit mode Editor Editors Email Email server Enable form attachments Enabled Expiration Expired False Fediverse Form Form details Form shared with %(number)s Form site Form title Forms Front page text GDPR Graphs Guest Hello,

You have been invited to LiberaForms.

Regards. Helpful text only visible to your users I understand Installation Invitation message Invitation only Invitations Invite a new user Invited by Label Language Last answer Login Logo Logout Look and feel Marked Maximum Media Minimum Multiple choice My forms Name New answer New author's username New forms New user Notify me by email Numeric fields Pending %(number)s Please try again soon. Primary color Public Public URL Published Restricted access Return to form Role Save Set upload limit Settings Share Shared answers Show invitation message Site configuration Site name Site settings Sorry, this form has expired. Statistics Super Admin options Terms and conditions Thank you!! This form has expired This form will expire This form will not be publically available until you have finished editing Total Total answers Total disk usage Total forms True Upload files limit Uploads enabled Uploads limit Use default value User details User guide Username Users Users will see this friendly message Validated email Version View answers We're sorry, this form is temporarily unavailable. Write the name of the user below Write the total number of answers below You are going to change the author of this form. You are going to delete %(total_answers)s answers You are going to delete a user You have an alert and their forms Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2022-04-08 10:01+0200
PO-Revision-Date: 2022-04-08 07:14+0000
Last-Translator: J. Lavoie <j.lavoie@net-c.ca>
Language: pt_BR
Language-Team: Portuguese (Brazil) <https://hosted.weblate.org/projects/liberaforms/server-liberaforms/pt_BR/>
Plural-Forms: nplurals=2; plural=n > 1
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
  *Descreva seu formulário.
 *Anexar conteúdo relevante, links, imagens, etc. %(total_uploads)s do %(disk_limit)s Ativo Adicionar editores Administrador Todos os Formulários Respostas Respostas compartilhadas com %(number)s Você tem certeza? Tipos de anexos Anexos Autor Média Bloqueado pelo Administrador Cancelar Modificar Mudar autor Mudar Idioma Config Configurar Confirmação Informação de Contato Contexto Criar uma conta Desenvolvido Autor atual Nome do autor atual Proteção de Dados Idioma Padrão Fuso horário padrão Deletar Deletar respostas Deletar usuário Deletar usuário e seus formulários Deletar usuário e formulários Detalhes Desabilitado Uso do disco Não mostrar está mensagem novamente Baixar CSV Editar Modo de Edição Editor Editores Correio Eletrônico Servidor de correio eletrônico Habilitar anexos no formulário Habilitado Validade Expirado Falso Fediverse Formulário Detalhes do formulário Fomulário compartilhado com %(number)s Site do formulário Título do formulário Formulários Texto da página inicial RGPD Gráficos Convidado Olá,

Você foi convidado para LiberaForms.

Atenciosamente. Texto de ajuda visível apenas para seus usuários Eu entendo Instalação Mensagem do convite Somente por convite Convites Convidar um novo usuário Convidado por Etiqueta Idioma Última resposta Entrar Logotipo Sair Aparência Marcado Máximo Meios de comunicação Mínimo Múltipla escolha Meus formulários Nome Nova resposta Novo nome do autor Novos Formulários Novo Usuário Notifique-me por correio eletrônico Campos numéricos Pendente %(number)s Por favor, tente novamente em brave. Cor primária Público URL público Publicado Acesso restrito Retorna para formulário Função Salvar Definir limite de envios Configurações Compartilhar Respostas compartilhadas Exibir mensagem de convite Configuração do site Nome do Site Configuração do site Desculpe, esse formulário expirou. Estatística Opições de Super Administrador Termos e Condições Obrigado!! Este formulário expirou Este formulário expirará Esse formulário não estará disponível publicamente até o término da edição Total Total de respostas Uso total do disco Total de Formulários Verdadeiro Limite de envio de arquivos Envios Habilitado Limites de Envios Usar valor padrão Detalhes do Usuário Manual do Usuário Nome do Usuário Usuários Usuários receberão esta mensagem amigável Correio eletrônico Validado Versão Visualizar respostas Lamentamos, formulário temporariamente indisponível. Escreva o nome do usuário abaixo Escreva o número total de respostas abaixo Você irá modificar o autor desse formulário. Você irá deletar %(total_answers)s respostas Você está prestes a deletar um usuário Você tem um alerta e seus formulários 