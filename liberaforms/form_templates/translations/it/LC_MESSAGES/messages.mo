��    �      D              l
  �   m
  s   �
  g   o  (  �  O      �   P  :  @  �   {  .   �     ,     A     Z     s  +   |     �  	   �  5   �  N   �  4   9  	   n     x  +   �  &   �  &   �               
                                   /     ;     M  -   l  /   �  .   �  8   �     2  "   >     a     u     �     �     �     �     �               &     3     ;     N     V     i  D   y  &   �  
   �     �       1     3   @  (   t     �     �     �  (   �     �       M   '     u  B   �     �  !   �  	   
  $     
   9     D     S  
   _  O   j     �     �  "   �     �     �               ,     I     Z     g  !   y     �     �     �     �     �     �  !   �  (         I     e     v     �     �  "   �  -   �       +   "  
   N  L   Y     �     �     �     �  6   �               #  	   ,     6     C     P  
   V     a     i  '   x  >   �     �     �          /     K     g     �  "   �  #   �     �  
   �  
     	          )   0  %   Z     �  X   �  ,   �  U     =   j  :   �  $   �       	         *  
   7  /   B  1   r     �     �     �     �  �  �  �   �!  �   ~"  h   #  C  �#  S   �$  �   %  E  &  �   ]'  F   �'     ?(  !   S(     u(     �(  3   �(     �(     �(  K   �(  Y   *)  6   �)     �)     �)  *   �)  (   	*  (   2*     [*     ]*     c*     f*     l*     o*     q*     s*     u*     �*     �*  &   �*  -   �*  2   +  :   7+  D   r+     �+  "   �+     �+     ,     ,     %,  #   :,      ^,     ,     �,     �,     �,  	   �,     �,     �,     �,     -  H   -  "   c-     �-     �-     �-  <   �-  2   �-  ,   '.     T.     e.     v.  %   �.     �.     �.  i   �.     Q/  R   m/     �/  %   �/  	   �/  (   0  
   *0     50  	   D0     N0  u   Z0     �0     �0  $   �0  	   1     1     1     +1     E1     _1     v1     �1     �1     �1     �1     �1     �1     
2     $2  ,   62  '   c2     �2     �2     �2     �2     �2  "   3  ;   73     s3  7   �3     �3  Q   �3  	   #4     -4     :4     B4  <   U4     �4     �4     �4     �4     �4     �4     �4     �4     �4     �4  .   	5  M   85     �5     �5     �5  &   �5      �5      6     ;6      U6  "   v6     �6     �6     �6     �6     �6  )   �6  /   7     M7  x   Z7  /   �7  i   8  =   m8  C   �8  '   �8     9  	   29     <9  
   H9  :   S9  /   �9     �9     �9     �9     �9   # Activity Feedback 
Thank you for participating in this activity. Please share with us your impression so we can improve future activities. # Congress II
Last year's congress was such a success we doing it again.

Register now for this year's sessions! # Contact Form
Have a question? Fill out this contact form and we'll get in touch as soon as possible. # Hotel booking
Come stay with us! Fill out this form to book a room at our hotel.

El Riuet Hotel
Carrer d'Orient, 33
Vidreres, 17411, Girona.
Catalunya, Espanya

Phone number: (+34) 93 553 43 25 
[Website](https://elriuethotel.com)
[Map](https://www.openstreetmap.org/way/218557184)
 # Lottery
Win 5 tickets to the cinema! Answer this form before September 20th. # Project Application Form
Have a project? Fill out this form to tell us about your proposal. 

This form is for project applications only. If you have any other question, please use our [contact form](https://example.com/contact-form). # Restaurant booking
Do you want to taste our delicious food? Fill out this form and save the date in your agenda!

[Delicious Restaurant Map](https://www.openstreetmap.org/way/314016327)
Carrer de les Avellanes, 15
El Secà de Sant Pere, 25005, Lleida.
Catalunya, Espanya

Phone number: (+34) 93 423 44 44 # Summer courses

We've prepared three days of courses and workshops.

Please reserve your place now. First in, first served. # We need your help
Please sign our petition. (check in from 12pm) (check out 12pm maximum) (from Tuesday to Sunday) (if any) (remember we only open from 20h30 to 23h30) 1 10h - 12h 10h - 13h. Computer Lab management with Free software 10h - 13h. Neutral networks. A practical presentation of our WIFI installation 10h - 13h. Presentation/demo. TPV to manage the cafe 12h - 14h 14h - 15h Lunch 16h - 19h. Social currencies. Local economy 18h - 20h. GIT for beginners Session 1 18h - 20h. GIT for beginners Session 2 2 20h30 21h 21h30 22h 3 4 5 A friend told me About right Activity feedback Add a second file if you wish. Add additional information about the project. Apply for a grant. Tell us about your proposal. Ask citizens to support your local initiative. Ask participants for their feedback about your activity. Attachments Book a room and come stay with us! Booking information Breakfast and dinner Breakfast and lunch Browsing the Internets Choose the day you want to come Comment about instructor Comment about tempos Comment about the content Comments Contact Form Content Cost justification Country Date for departure Date of arrival Do you want to taste our delicious food? Save a date in your agenda! Do you want us to send you a reminder? Double bed Double bed + single bed Email Explain here your project. Focus on what and how. Explain what the requested budget will be used for. Full board (breakfast, lunch and dinner) Future activities Have a comment? Hotel booking How did you find out about this lottery? How many people are you? How should we call you? I allow you to keep the information I submit for future funding opportunities I receive your newletters I want you to erase all information if the project is not selected I'm eating else where I'm eating out, thanks for asking ID number If you win, we'll send you an email! Instructor Just breakfast Just dinner Just lunch Let attendees choose one of two talks running in parallel and their lunch menu. Lottery Lunch and dinner Maximum 10 people per reservation. Message Name Name or nick No thanks. I will remember. No, thanks. I will remember. One day congress Organization Other information Other things you want to express. Poor to excellent Privacy Project Application Project description Project information Project name Put your name down to win a prize Rate the explainations of the instructor Rate the quality of content Requested amount Restaurant booking Room 1. AI descrimination Room 1. Municipal Strategies Room 2. Build an anonymous website Room 2. Decentralized tech. Freedom of speech Save our shelter Shared room (bunk beds with 3 other people) Single bed Students can enroll in a variety of activities spread out across three days. Subjects Summer courses Surname Telephone number Tell us if you are interested in a particular subject. Tempos Thursday 27th Too long Too short Tuesday 25th Type of room Vegan Vegaterian Website Wednesday 26th What did you like? What can we improve? What should we do if your project is not immediately selected? When do you want to eat? Will you eat at the hotel? Yes, by email 3 days before Yes, by email 5 days before Yes, by phone 3 days before Yes, by phone 5 days before You felt breaks were... You felt that first session was... You felt that second session was... Your contact information Your data. Your email Your name e.g. +34 678 655 333 e.g. Association, cooperative, collective e.g. Buy hardware, hire someone, etc. e.g. FediBook e.g. First session was very interesting although I did expect more information about it. e.g. GDPR compliance and linguistic justice. e.g. I did enjoy both sessions. The instructor was very passionate about the subject. e.g. I've already been in your restaurant and it's very nice! e.g. I've already stayed at your hotel and it's very nice! e.g. It was nice in general. Thanks. e.g. Just to say hello! e.g. Mary e.g. Poppins e.g. Spain e.g. There was no green tea in the coffeebreak. e.g. We want to write a book about the Fediverse. e.g. https://fedi.cat e.g. mary@example.com e.g. mary@exemple.com up to €5,000 Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2022-04-08 10:01+0200
PO-Revision-Date: 2022-04-08 07:21+0000
Last-Translator: LiberaForms <info@liberaforms.org>
Language: it
Language-Team: Italian <https://hosted.weblate.org/projects/liberaforms/form-templates-liberaforms/it/>
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
 # Feedback delle attività 
Grazie per aver partecipato a questa attività. Per favore, condividi con noi la tua impressione così possiamo migliorare nel futuro. # Congresso II
Il congresso dell’anno scorso è stato talmente un successo che lo stiamo rifacendo.

Registrati ora per le conferenze di quest’anno! # Modulo di contatto
Hai una domanda? Completa questo modulo e ti contatteremo al più presto possibile. # Prenotazione dell’hotel
Stai con noi! Riempi questo modulo per prenotare una stanza presso il nostro hotel.

El Riuet Hotel
Carrer d'Orient, 33
Vidreres, 17411, Girona.
Catalunya, Espanya

Numero di telefono: (+34) 93 553 43 25 
[Sito web](https://elriuethotel.com)
[Mappa](https://www.openstreetmap.org/way/218557184)
 # Lottery
Vinci 5 ticket al cinema! Rispondi a questo modulo prima del 20 dicembre. # Modulo di partecipazione al bando
Hai un progetto? Compila questo modulo per esporci la tua proposta.

Questo modulo è esclusivamente per partecipazione a bandi. Se hai una domanda, usa il nostro [modulo di contatto](https://example.com/contact-form). # Prenotazione per ristorante
Vuoi assaggiare il nostro cibo delizioso? Compila questo modulo e tieniti libero in questa data!

[Mappa Ristorante Delizioso](https://www.openstreetmap.org/way/314016327)
Carrer de les Avellanes, 15
El Secà de Sant Pere, 25005, Lleida.
Catalogna, Spagna

Numero di telefono: (+34) 93 423 44 44 # Corsi estivi

Abbiamo preparato tre giorni di corsi e laboratori.

Ti preghiamo di riservare ora il tuo posto. Il primo che arriva è il meglio servito. # Abbiamo bisogno del tuo aiuto
Per favore, firma la nostra petizione. (check in dalle 12) (check out al più tardi alle 12) (da martedì a domenica) (se esiste) (nota che siamo aperti solo dalle 20:30 alle 23:30) 1 10:00 - 12:00 10 - 13. Utilizzate Free software per gestire il laboratorio di Informatica 10 - 13. Network neutrali. Una presentazione pratica sull’installazione del nostro WiFi 10 - 13. Presentazione/demo. TVP per gestire il caffè 12:00 - 14:00 14:00 - 15:00 Pranzo 16 - 19. Economia sociale. Economia locale 18 - 20. GIT per principianti Sessione 1 18 - 20. GIT per principianti Sessione 2 2 20:30 21 21:30 22 3 4 5 Me ne ha parlato un amico Giusta Feedback delle attività Aggiungi un secondo file, se desideri. Aggiungi ulteriori informazioni sul progetto. Partecipa al bando. Raccontaci della tua proposta. Chiedi ai cittadini supporto per la tua iniziativa locale. Chiedi ai partecipanti il loro feedback riguardo alla tua attività. Allegati Prenota una stanza e stai con noi! Informazioni della prenotazione Colazione e cena Colazione e cena Navigare su internet Scegli il giorno in cui vuoi venire Commenta riguardo l’istruttore Un commento sulle tempistiche Commento sul contenuto Commenti Modulo di contatto Contenuto Giustificazione del costo Paese Data di partenza Data di arrivo Vuoi assaggiare il nostro cibo delizioso? Tieniti libero in questa data! Vuoi che ti inviamo un promemoria? Letto doppio Letto doppio e letto singolo E-mail Spiega qui il tuo progetto. Concentrati sul cosa e sul come. Spiega come il budget richiesto verrà utilizzato. Pensione completa (colazione, pranzo e cena) Attività future Hai un commento? Prenotazione dell’hotel Come hai scoperto di questa lotteria? Quante persone siete? Come ti dovremmo chiamare? Concedo il permesso per conservare le informazioni che condivido per future possibilità di finanziamento Ricevo le vostre newsletter Vorrei che eliminaste tutte le informazioni se il progetto non venisse selezionato Mangerò altrove Mangerò fuori, grazie dell’offerta Numero ID Se avrai vinto, ti invieremo un’email. Istruttore Solo colazione Solo cena Solo pranzo Permetti ai partecipanti di scegliere una o due conferenze che si svolgono in parallelo e il menu del proprio pranzo. Lotteria Pranzo e cena Massimo 10 persone per prenotazione. Messaggio Nome Nome o nickname No grazie. Mi ricorderò. No grazie, mi ricorderò. Congresso di un giorno Organizzazione Altre informazioni Altre cose che vorresti dire. Da pessimo a eccellente Privacy Invia proposta di progetto Descrizione del progetto Informazioni sul progetto Nome del progetto Scrivi qui il tuo nome per vincere un premio Valuta le spiegazioni dell’istruttore Vota la qualità del contenuto Somma richiesta Prenotazioni per il ristorante Sala 1. Discriminazione di AI Sala 1, Strategie municipali Sala 2. Costruisci un sito anonimo Sala 2. Tecnologia decentralizzata. Libertà di espressione Salva il nostro rifugio Stanza condivisa (letto a castello con altre 3 persone) Letto singolo Gli studenti possono iscriversi a varie attività sparse nel corso di tre giorni. Argomenti Corsi estivi Cognome Numero di telefono Specifica se sei interessato in un argomento in particolare. Tempistiche Giovedì 27 Troppo lunga Troppo breve Martedì 25 Tipo di stanza Vegano Vegetariano Sito web Mercoledì 26 Cosa ti è piaciuto? Cosa possiamo migliorare? Cosa dovremmo fare se il tuo progetto non venisse immediatamente selezionato? A che ora vorresti cenare? Mangerai in hotel? Sì, 3 giorni prima via email Sì, inviami un’email 5 giorni prima Sì, 3 giorni prima via telefono Sì, 5 giorni prima via telefono Pensi le pause fossero… Pensi la prima sessione fosse… Pensi la seconda sessione fosse… Le tue informazioni di contatto I tuoi dati. La tua email Il tuo nome es. +39 678 655 333 ee. Associazione, cooperativa, collettivo es. Comprare materiale, assumere qualcuno, ecc. es. FediBook ad esempio: “La prima sessione era molto interessante, anche se mi aspettavo di avere più informazioni su di essa”. es. aderenza al GDPR e correttezza linguistica. per esempio: “ho apprezzato entrambe le sessioni. Il docente era molto appassionato all’argomento”. es. Sono già stato nel vostro ristorante ed è molto carino! per esempio: sono già statə al vostro hotel ed è proprio carino! es. È stato bello in generale. Grazie. es. Volevo solo dire ciao! es. Maria es. Poppins es. Spagna per esempio: “Non c’era tè verde durante la pausa”. es. Intendiamo scrivere un libro sul Fediverse. es. https://fedi.cat es. mary@esempio.com es. mary@exemple.com fino a 5000€ 