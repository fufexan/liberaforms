��    �      D              l
  �   m
  s   �
  g   o  (  �  O      �   P  :  @  �   {  .   �     ,     A     Z     s  +   |     �  	   �  5   �  N   �  4   9  	   n     x  +   �  &   �  &   �               
                                   /     ;     M  -   l  /   �  .   �  8   �     2  "   >     a     u     �     �     �     �     �               &     3     ;     N     V     i  D   y  &   �  
   �     �       1     3   @  (   t     �     �     �  (   �     �       M   '     u  B   �     �  !   �  	   
  $     
   9     D     S  
   _  O   j     �     �  "   �     �     �               ,     I     Z     g  !   y     �     �     �     �     �     �  !   �  (         I     e     v     �     �  "   �  -   �       +   "  
   N  L   Y     �     �     �     �  6   �               #  	   ,     6     C     P  
   V     a     i  '   x  >   �     �     �          /     K     g     �  "   �  #   �     �  
   �  
     	          )   0  %   Z     �  X   �  ,   �  U     =   j  :   �  $   �       	         *  
   7  /   B  1   r     �     �     �     �  �  �  �   �!  ~   x"  ~   �"  C  v#  b   �$    %  L  6&  �   �'  :   (     H(     a(     }(     �(  -   �(     �(     �(  <   �(  T   ")  I   w)  	   �)     �)  ,   �)  +   	*  +   5*     a*     c*     i*     m*     s*     w*     y*     {*     }*  
   �*     �*  '   �*  0   �*  3   +  ,   C+  @   p+     �+  2   �+     �+     ,     ,     *,      G,     h,     �,     �,     �,     �,  	   �,     �,     -     	-     -  L   *-     w-     �-  $   �-     �-  :   �-  .   .  +   J.     v.     �.     �.      �.     �.  "   �.  Q   /     g/  J   /     �/  $   �/     0  $   0  
   50     @0  	   N0     X0  U   d0     �0     �0      �0     �0     �0     �0     1     (1     C1     W1     e1  !   w1     �1  
   �1     �1     �1     �1     2  "   2  '   :2      b2     �2     �2  9   �2     �2  "   3  ;   +3     g3  5   {3     �3  f   �3     (4     .4  	   ?4     I4  0   ^4     �4  	   �4     �4     �4  	   �4     �4     �4     �4     �4     �4  %   5  J   25     }5     �5  *   �5  *   �5      6      &6     G6  &   g6  &   �6  #   �6  
   �6     �6  	   �6     7  *   7  2   G7     z7  a   �7  3   �7  O   8  :   o8  7   �8  '   �8  !   
9     ,9     :9     I9  1   X9  5   �9     �9     �9     �9     
:   # Activity Feedback 
Thank you for participating in this activity. Please share with us your impression so we can improve future activities. # Congress II
Last year's congress was such a success we doing it again.

Register now for this year's sessions! # Contact Form
Have a question? Fill out this contact form and we'll get in touch as soon as possible. # Hotel booking
Come stay with us! Fill out this form to book a room at our hotel.

El Riuet Hotel
Carrer d'Orient, 33
Vidreres, 17411, Girona.
Catalunya, Espanya

Phone number: (+34) 93 553 43 25 
[Website](https://elriuethotel.com)
[Map](https://www.openstreetmap.org/way/218557184)
 # Lottery
Win 5 tickets to the cinema! Answer this form before September 20th. # Project Application Form
Have a project? Fill out this form to tell us about your proposal. 

This form is for project applications only. If you have any other question, please use our [contact form](https://example.com/contact-form). # Restaurant booking
Do you want to taste our delicious food? Fill out this form and save the date in your agenda!

[Delicious Restaurant Map](https://www.openstreetmap.org/way/314016327)
Carrer de les Avellanes, 15
El Secà de Sant Pere, 25005, Lleida.
Catalunya, Espanya

Phone number: (+34) 93 423 44 44 # Summer courses

We've prepared three days of courses and workshops.

Please reserve your place now. First in, first served. # We need your help
Please sign our petition. (check in from 12pm) (check out 12pm maximum) (from Tuesday to Sunday) (if any) (remember we only open from 20h30 to 23h30) 1 10h - 12h 10h - 13h. Computer Lab management with Free software 10h - 13h. Neutral networks. A practical presentation of our WIFI installation 10h - 13h. Presentation/demo. TPV to manage the cafe 12h - 14h 14h - 15h Lunch 16h - 19h. Social currencies. Local economy 18h - 20h. GIT for beginners Session 1 18h - 20h. GIT for beginners Session 2 2 20h30 21h 21h30 22h 3 4 5 A friend told me About right Activity feedback Add a second file if you wish. Add additional information about the project. Apply for a grant. Tell us about your proposal. Ask citizens to support your local initiative. Ask participants for their feedback about your activity. Attachments Book a room and come stay with us! Booking information Breakfast and dinner Breakfast and lunch Browsing the Internets Choose the day you want to come Comment about instructor Comment about tempos Comment about the content Comments Contact Form Content Cost justification Country Date for departure Date of arrival Do you want to taste our delicious food? Save a date in your agenda! Do you want us to send you a reminder? Double bed Double bed + single bed Email Explain here your project. Focus on what and how. Explain what the requested budget will be used for. Full board (breakfast, lunch and dinner) Future activities Have a comment? Hotel booking How did you find out about this lottery? How many people are you? How should we call you? I allow you to keep the information I submit for future funding opportunities I receive your newletters I want you to erase all information if the project is not selected I'm eating else where I'm eating out, thanks for asking ID number If you win, we'll send you an email! Instructor Just breakfast Just dinner Just lunch Let attendees choose one of two talks running in parallel and their lunch menu. Lottery Lunch and dinner Maximum 10 people per reservation. Message Name Name or nick No thanks. I will remember. No, thanks. I will remember. One day congress Organization Other information Other things you want to express. Poor to excellent Privacy Project Application Project description Project information Project name Put your name down to win a prize Rate the explainations of the instructor Rate the quality of content Requested amount Restaurant booking Room 1. AI descrimination Room 1. Municipal Strategies Room 2. Build an anonymous website Room 2. Decentralized tech. Freedom of speech Save our shelter Shared room (bunk beds with 3 other people) Single bed Students can enroll in a variety of activities spread out across three days. Subjects Summer courses Surname Telephone number Tell us if you are interested in a particular subject. Tempos Thursday 27th Too long Too short Tuesday 25th Type of room Vegan Vegaterian Website Wednesday 26th What did you like? What can we improve? What should we do if your project is not immediately selected? When do you want to eat? Will you eat at the hotel? Yes, by email 3 days before Yes, by email 5 days before Yes, by phone 3 days before Yes, by phone 5 days before You felt breaks were... You felt that first session was... You felt that second session was... Your contact information Your data. Your email Your name e.g. +34 678 655 333 e.g. Association, cooperative, collective e.g. Buy hardware, hire someone, etc. e.g. FediBook e.g. First session was very interesting although I did expect more information about it. e.g. GDPR compliance and linguistic justice. e.g. I did enjoy both sessions. The instructor was very passionate about the subject. e.g. I've already been in your restaurant and it's very nice! e.g. I've already stayed at your hotel and it's very nice! e.g. It was nice in general. Thanks. e.g. Just to say hello! e.g. Mary e.g. Poppins e.g. Spain e.g. There was no green tea in the coffeebreak. e.g. We want to write a book about the Fediverse. e.g. https://fedi.cat e.g. mary@example.com e.g. mary@exemple.com up to €5,000 Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2022-04-08 10:01+0200
PO-Revision-Date: 2021-12-18 15:32+0000
Last-Translator: LiberaForms <info@liberaforms.org>
Language: es
Language-Team: Spanish <https://hosted.weblate.org/projects/liberaforms/form-templates-liberaforms/es/>
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
 # Evaluación de actividad
Gracias por participar en esta actividad. Por favor, comparte con nosotros tus impresiones para que podamos mejorar futuras actividades. # Congreso II
El año pasado el congreso tuvo tanto éxito que repetimos.

¡Regístrate ahora para las sesiones de esta año! # Formulario de contacto
¿Tienes alguna pregunta? Rellena este formulario y nos pondremos en contacto lo más pronto posible. # Reserva de hotel
¡Quédate con nosotros! Rellena este formulario para reservar una habitación en nuestro.

Hotel El Riuet
Calle d'Orient, 33
Vidreres, 17411, Girona.
Cataluña, España

Número de teléfono: (+34) 93 553 43 25
[Página web](https://elriuethotel.com)
[Mapa](https://www.openstreetmap.org/way/218557184)
 # Sorteo
¡Gana 5 entradas para ir al cine! Responde a este formulario antes del 20 de septiembre. # Formulario de propuesta de proyecto
¿Tienes un proyecto? Rellena este formulario para informarnos de tu propuesta.

Este formulario solo sirve para presentar proyectos. Si tienes otras preguntas, por favor usa nuestro [formulario de contacto](https://example.com/contact-form). # Reserva de restaurante
¿Quieres probar nuestros deliciosos platos? Rellena este formulario y ¡reserva el día en tu agenda!

[Mapa del restaurante Delicious](https://www.openstreetmap.org/way/314016327)
Calle de las Avellanes, 15
El Secà de Sant Pere, 25005, Lleida.
Cataluña, España

Número de teléfono: (+34) 93 423 44 44 # Cursos de verano

Hemos preparado tres días de cursos y talleres.

Por favor, reserva plaza ahora. Se validará por órden de llegada. # Necesitamos tu ayuda
Por favor, firma nuestra petición. (entrada desde las 12pm) (salida màximo a las 12pm) (de martes a domingo) (en el caso que la haya) (recuerda que sólo abrimos de 20h30 a 23h30) 1 10h -12h 10h -13h. Gestión de aulas informáticas con software libre 10h - 13h. Redes neutrales. Una presentación práctica de nuestra instalación WIFI 10h - 13h. Presentación/demostración. TPV para gestionar una cafetería 12h - 14h 14h - 15h Comida 16h - 19h. Monedas sociales. Economía local 18h - 20h. GIT para principiantes Sesión 1 18h - 20h. GIT para principiantes Sesión 2 2 20h30 21h 21h30 22h 3 4 5 Me lo dijo una amiga Suficiente Evaluación de actividad Añade un segundo fichero si lo deseas. Añade información adicional sobre el proyecto. Solicita apoyo económico. Cuéntanos tu propuesta. Pide apoyo popular para tu iniciativa local. Pregunta a las participantes sus impresiones sobre tu actividad. Adjuntos Reserva una habitación y ¡quédate con nosotros! Información de la reserva Desayuno y cena Desayuno y comida Navegando por los Internetes Escoge el día que quieres venir Comenta sobre el formador/a Comenta sobre los tiempos Comenta sobre el contenido Comentarios Formulario de contacto Contenido Justificación de los costes País Fecha de salida Fecha de llegada ¿Quieres probar nuestros deliciosos platos? ¡Reserva un día en tu agenda! ¿Necesitas un recordatorio? Cama de matrimonio Cama de matrimonio + cama individual Correo electrónico Explica tu proyecto aquí. Focaliza en el qué y el cómo. Explica para qué se va a usar el presupuesto. Pensión completa (desayuno, comida y cena) Próximas actividades ¿Algún comentario? Reserva de hotel ¿Cómo encontraste este sorteo? ¿Cuántas personas vendréis? ¿Cómo te tendríamos que llamar? Permito que guardéis la información para futuras oportunidades de financiación Recibo vuestro boletín Quiero que borréis toda la información si el proyecto no es seleccionado Comeré en algún otro lugar Comeré fuera, gracias por preguntar DNI Si ganas, ¡te mandaremos un correo! Formador/a Solo desayuno Solo cena Solo comida Las participantes pueden escoger una o dos charlas paralelas y el menú de la comida. Sorteo Comida y cena Máximo 10 personas por reserva. Mensaje Nombre Nombre o apodo No gracias. Me acordaré. No, gracias. Me acordaré. Congreso de un día Organización Otra información Otras cosas que quieras expresar. De pobre a excelente Privacidad Propuesta de proyecto Descripción del proyecto Información del proyecto Nombre del proyecto Escribe tu nombre y gana un premio Evalua las explicaciones del formador/a Puntúa la calidad del contenido Cantidad solicitada Reserva de restaurante Sala 1. Discriminación de las Inteligencias Artificiales Aula 1. Estrategias municipales Sala 2. Crea un sitio web anónimo Aula 2. Tecnología descentralizada. Libertad de expresión Salvamos el refugio Habitación compartida (literas con otras 3 personas) Cama individual Los estudiantes pueden inscribirse en una variedad de actividades repartidas a lo largo de tres días. Temas Cursos de verano Apellidos Número de teléfono Dinos si te insteresa algún tema en particular. Tiempos Jueves 27 Demasiado larga Demasiado corta Martes 25 Tipo de habitación Vegana Vegateriana Página web Miércoles 26 ¿Qué te gustó? ¿Qué mejorarías? ¿Qué deberíamos hacer si tu proyecto no es seleccionado inmediatamente? ¿A qué hora quieres cenar? Comidas en el hotel? Sí, por correo electrónico 3 días antes Sí, por correo electrónico 5 días antes Sí, por teléfono 3 días antes Sí, por teléfono 5 días antes Sentiste que las pausas eran... Sentiste que la primera sesión fue... Sentiste que la segunda sesión fue... Información para contactar contigo Tus datos. Tu correo electrónico Tu nombre p. ej. +34 678 655 333 p. ej. Asociación, cooperativa, colectivo p. ej. Comprar hardware, contratar a alguien, etc. p.ej. FediBook p. ej. La primera sesión fue muy interesante aunque me esperaba más información sobre el tema. p. ej. Cumplir el RGPD y la justicia lingüística. p. ej. Disfruté de las dos sesiones. La formadora era muy apasionada del tema. p. ej. Ya estuve en vuestro restaurante y ¡es muy bonito! p. ej. ¡Ya he estado en vuestro hotel y es muy bonito! p. ej. Estuvo bien en general. Gracias. p. ej. ¡Solo quería decir hola! p. ej. María p. ej. Poppins p. ej. España p. ej. En la pausa del café no había té verde. p. ej. Queremos escribir un libro sobre el Fediverso. p. ej. https://fedi.cat p. ej. maria@example.com p. ej. maria@exemple.com hasta 5000 euros 