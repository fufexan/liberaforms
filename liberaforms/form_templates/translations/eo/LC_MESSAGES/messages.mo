��    �      D              l
  �   m
  s   �
  g   o  (  �  O      �   P  :  @  �   {  .   �     ,     A     Z     s  +   |     �  	   �  5   �  N   �  4   9  	   n     x  +   �  &   �  &   �               
                                   /     ;     M  -   l  /   �  .   �  8   �     2  "   >     a     u     �     �     �     �     �               &     3     ;     N     V     i  D   y  &   �  
   �     �       1     3   @  (   t     �     �     �  (   �     �       M   '     u  B   �     �  !   �  	   
  $     
   9     D     S  
   _  O   j     �     �  "   �     �     �               ,     I     Z     g  !   y     �     �     �     �     �     �  !   �  (         I     e     v     �     �  "   �  -   �       +   "  
   N  L   Y     �     �     �     �  6   �               #  	   ,     6     C     P  
   V     a     i  '   x  >   �     �     �          /     K     g     �  "   �  #   �     �  
   �  
     	          )   0  %   Z     �  X   �  ,   �  U     =   j  :   �  $   �       	         *  
   7  /   B  1   r     �     �     �     �  �  �  �   �!  {   v"  }   �"  2  p#  a   �$    %  Q  &  �   o'  :   �'     ,(     @(     _(  	   v(  5   �(     �(  	   �(  B   �(  J   )  [   P)  	   �)     �)  (   �)  &   �)  &   *     A*     C*     I*     M*     S*     W*     Y*     [*     ]*     p*     �*  !   �*  (   �*  8   �*  /   +  2   K+  
   ~+  '   �+     �+     �+     �+     �+  &   	,     0,     J,     `,     w,     �,     �,     �,     �,     �,     �,  S   �,  +   #-  
   O-     Z-     s-  ;   �-  +   �-  9   �-     $.     1.     H.  $   Y.     ~.     �.  P   �.     �.  I   /     ^/     q/     �/  )   �/  
   �/     �/     �/     �/  Z   �/     Y0     a0  $   |0     �0     �0     �0     �0     �0     �0     1     1  $   1     A1  	   U1     _1     p1     �1     �1  "   �1  &   �1     �1     2     %2  -   :2     h2  $   �2  .   �2     �2  ,   �2     3  E   (3     n3     t3     �3     �3  )   �3     �3     �3  	   �3     �3     �3     4     4     4     #4     *4  (   74  3   `4     �4     �4  !   �4      �4      5     ,5  !   L5  $   n5  #   �5     �5     �5     �5     �5     �5  #   6  '   16     Y6  R   g6  *   �6  H   �6  >   .7  ;   m7  "   �7     �7     �7     �7     8  )   8  ,   >8     k8     �8     �8     �8   # Activity Feedback 
Thank you for participating in this activity. Please share with us your impression so we can improve future activities. # Congress II
Last year's congress was such a success we doing it again.

Register now for this year's sessions! # Contact Form
Have a question? Fill out this contact form and we'll get in touch as soon as possible. # Hotel booking
Come stay with us! Fill out this form to book a room at our hotel.

El Riuet Hotel
Carrer d'Orient, 33
Vidreres, 17411, Girona.
Catalunya, Espanya

Phone number: (+34) 93 553 43 25 
[Website](https://elriuethotel.com)
[Map](https://www.openstreetmap.org/way/218557184)
 # Lottery
Win 5 tickets to the cinema! Answer this form before September 20th. # Project Application Form
Have a project? Fill out this form to tell us about your proposal. 

This form is for project applications only. If you have any other question, please use our [contact form](https://example.com/contact-form). # Restaurant booking
Do you want to taste our delicious food? Fill out this form and save the date in your agenda!

[Delicious Restaurant Map](https://www.openstreetmap.org/way/314016327)
Carrer de les Avellanes, 15
El Secà de Sant Pere, 25005, Lleida.
Catalunya, Espanya

Phone number: (+34) 93 423 44 44 # Summer courses

We've prepared three days of courses and workshops.

Please reserve your place now. First in, first served. # We need your help
Please sign our petition. (check in from 12pm) (check out 12pm maximum) (from Tuesday to Sunday) (if any) (remember we only open from 20h30 to 23h30) 1 10h - 12h 10h - 13h. Computer Lab management with Free software 10h - 13h. Neutral networks. A practical presentation of our WIFI installation 10h - 13h. Presentation/demo. TPV to manage the cafe 12h - 14h 14h - 15h Lunch 16h - 19h. Social currencies. Local economy 18h - 20h. GIT for beginners Session 1 18h - 20h. GIT for beginners Session 2 2 20h30 21h 21h30 22h 3 4 5 A friend told me About right Activity feedback Add a second file if you wish. Add additional information about the project. Apply for a grant. Tell us about your proposal. Ask citizens to support your local initiative. Ask participants for their feedback about your activity. Attachments Book a room and come stay with us! Booking information Breakfast and dinner Breakfast and lunch Browsing the Internets Choose the day you want to come Comment about instructor Comment about tempos Comment about the content Comments Contact Form Content Cost justification Country Date for departure Date of arrival Do you want to taste our delicious food? Save a date in your agenda! Do you want us to send you a reminder? Double bed Double bed + single bed Email Explain here your project. Focus on what and how. Explain what the requested budget will be used for. Full board (breakfast, lunch and dinner) Future activities Have a comment? Hotel booking How did you find out about this lottery? How many people are you? How should we call you? I allow you to keep the information I submit for future funding opportunities I receive your newletters I want you to erase all information if the project is not selected I'm eating else where I'm eating out, thanks for asking ID number If you win, we'll send you an email! Instructor Just breakfast Just dinner Just lunch Let attendees choose one of two talks running in parallel and their lunch menu. Lottery Lunch and dinner Maximum 10 people per reservation. Message Name Name or nick No thanks. I will remember. No, thanks. I will remember. One day congress Organization Other information Other things you want to express. Poor to excellent Privacy Project Application Project description Project information Project name Put your name down to win a prize Rate the explainations of the instructor Rate the quality of content Requested amount Restaurant booking Room 1. AI descrimination Room 1. Municipal Strategies Room 2. Build an anonymous website Room 2. Decentralized tech. Freedom of speech Save our shelter Shared room (bunk beds with 3 other people) Single bed Students can enroll in a variety of activities spread out across three days. Subjects Summer courses Surname Telephone number Tell us if you are interested in a particular subject. Tempos Thursday 27th Too long Too short Tuesday 25th Type of room Vegan Vegaterian Website Wednesday 26th What did you like? What can we improve? What should we do if your project is not immediately selected? When do you want to eat? Will you eat at the hotel? Yes, by email 3 days before Yes, by email 5 days before Yes, by phone 3 days before Yes, by phone 5 days before You felt breaks were... You felt that first session was... You felt that second session was... Your contact information Your data. Your email Your name e.g. +34 678 655 333 e.g. Association, cooperative, collective e.g. Buy hardware, hire someone, etc. e.g. FediBook e.g. First session was very interesting although I did expect more information about it. e.g. GDPR compliance and linguistic justice. e.g. I did enjoy both sessions. The instructor was very passionate about the subject. e.g. I've already been in your restaurant and it's very nice! e.g. I've already stayed at your hotel and it's very nice! e.g. It was nice in general. Thanks. e.g. Just to say hello! e.g. Mary e.g. Poppins e.g. Spain e.g. There was no green tea in the coffeebreak. e.g. We want to write a book about the Fediverse. e.g. https://fedi.cat e.g. mary@example.com e.g. mary@exemple.com up to €5,000 Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2022-04-08 10:01+0200
PO-Revision-Date: 2021-12-18 15:32+0000
Last-Translator: phlostically <phlostically@mailinator.com>
Language: eo
Language-Team: Esperanto <https://hosted.weblate.org/projects/liberaforms/form-templates-liberaforms/eo/>
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
 # Rimarkoj pri okazaĵo
Dankon por partopreni ĉi tiun okazaĵon. Bonvolu doni al ni vian takson por ke ni povos plibonigi venontajn okazaĵojn. # Kongreso II
La lastjara kongreso estis tiel bona sukceso, ke ni faras ĝin denove.

Registriĝu nun por ĉi jaraj sesioj! # Kontakto-Formularo
Ĉu vi havas demandon? Plenigu ĉi tiun kontakto-formularon kaj ni kontaktos vin kiel eble plej baldaŭ. # Hotela rezervigo
Venu gasti ĉe ni! Plenigu ĉi tiun formularon por rezervigi ĉambron en nia hotelo.

El Riuet Hotel
Carrer d'Orient, 33
Vidreres, 17411, Girona.
Catalunya, Espanya

Telefonnumero: (+34) 93 553 43 25
[Retejo](https://elriuethotel.com)
[Mapo](https://www.openstreetmap.org/way/218557184)
 # Loterio
Gajnu 5 biletojn al la kinejo! Respondu ĉi tiun formularon antaŭ la 20a de Septembro. # Formularo de Projekto-Propono
Ĉu vi havas projekton? Plenigu ĉi tiun formularon por informi nin pri via propono.

Ĉi tiu formularo estas nur por projekto-proponoj. Se vi havas aliajn demandojn, bonvolu uzi nian [kontakto-formularon](https://ekzemplo.com/kontakto-formularo). # Restoracia rezervigo
Ĉu vi volas gustumi niajn bongustegajn manĝaĵojn? Plenigu ĉi tiun formularon kaj konservu la daton en via agendo!

[Mapo de elstaraj restoracioj](https://www.openstreetmap.org/way/314016327)
Carrer de les Avellanes, 15
El Secà de Sant Pere, 25005, Lleida.
Catalunya, Espanya

Telefonnumero: (+34) 93 423 44 44 # Someraj kursoj

Ni preparis tri tagojn da kursoj kaj laborsesioj.

Bonvolu rezervi vian spacon nun. Unue veninta, unue servita. # Ni bezonas vian helpon.
Bonvolu subskribi nian peticion. (ensaluti ekde 12h) (elsaluto je la 12h maksimume) (de mardo al dimanĉo) (se ajna) (memoru ke ni nur malfermiĝas ekde 20h30 ĝis 23h30) 1 10h - 12h 10h - 13h. Mastrumado de komputillaboratorio per libera programaro 10h - 13h. Neŭtralaj retoj. Praktika prezentaĵo pri nia vifia instalaĵo 10h - 13h. Prezentaĵo/montraĵo. Ekstera liveranto de kontrolado por administri la kafejon 12h - 14h 14h - 15h Tagmanĝo 16h - 19h. Sociaj valutoj. Loka ekonomio 18h - 20h. GIT por komencantoj Sesio 1 18h - 20h. GIT por komencantoj Sesio 2 2 20h30 21h 21h30 22h 3 4 5 Amiko informis min Pli malpli bona Rimarkoj pri okazaĵoj Aldonu duan dosieron se vi volas. Aldonu pliajn informojn pri la projekto. Kandidatiĝu por subvencio. Informu nin pri via propono. Petu al civitanoj subteni vian lokan projekton. Petu rimarkojn de partoprenantoj pri via okazaĵo. Aldonaĵoj Rezervu ĉambron kaj venu gasti ĉe ni! Rezervadaj informoj Matenmanĝo kaj vespermanĝo Matenmanĝo kaj tagmanĝo Per retumado Elektu la tagon en kiu vi volas alveni Komentu pri la instruisto Komentu pri la tempoj Komentu pri la enhavon Komentoj Kontakto-Formularo Enhavo Pravigo de kosto Lando Dato de foriro Dato de alveno Ĉu vi volas gustumi niajn bongustajn manĝaĵojn? Konservu la daton en via agendo! Ĉu vi volas ke ni sendu al vi memorigilon? Duopa lito Duopa lito + unuopa lito Retpoŝtadreso Klarigu ĉi tie vian projekton. Fokusiĝu pri kio kaj kiel. Klarigu kiel estos uzata la petita buĝeto. Plene inkluziva (matenmanĝo, tagmanĝo kaj vespermanĝo) Okazontaĵoj Ĉu vi havas komenton? Hotela rezervigo Kiel vi eksciis pri ĉi tiu loterio? Kiom da homoj vi estas? Kiel ni voku vin? Mi permesas al vi teni la informojn kiujn mi sendas por estontaj financaj okazoj Mi ricevas viajn bultenojn Mi volas ke vi forviŝu ĉiujn informojn se la projekto ne estas elektita Mi manĝas aliloke Mi manĝos aliloke, dankon Identignumero Se vi gajnos, ni sendos al vi retpoŝton! Instruisto Nur matenmanĝo Nur vespermanĝo Nur tagmanĝo Lasu ĉeestantojn elekti unu el du prelegoj samtempe okazantaj kaj sian tagmanĝan menuon. Loterio Tagmanĝo kaj vespermanĝo Maksimume 10 homoj po unu rezervigo. Mesaĝo Nomo Nomo aŭ kromnomo Ne, dankon. Mi memoros. Ne, dankon. Mi memoros. Unutaga kongreso Organizaĵo Aliaj informoj Aliaj aferoj kiujn vi volas esprimi. Malbona ĝis bonega Privateco Projekta Propono Priskribo de projekto Informoj pri projekto Nomo de projekto Enigu vian nomon por gajni premion Pritaksu la klarigojn de la instruisto Pritaksu la kvaliton de enhavo Petita monsumo Restoracia rezervigo Ĉambro 1. Distingado en artefarita intelekto Ĉambro 1. Enurbaj Strategioj Ĉambro 2. Konstrui anoniman retejon Ĉambro 2. Malcentra teknologio. Parollibereco Savu nian rifuĝejon Komuna ĉambro (dulitoj kun tri aliaj homoj) Unuopa lito Studentoj povas aliĝi al diversaj okazaĵoj okazantaj dum tri tagoj. Temoj Someraj kursoj Familia nomo Telefonnumero Diru al ni se vin interesas specifa temo. Tempoj Ĵaŭdo la 27a Tro longa Tro mallonga Mardo la 25a Speco de ĉambro Vegana Vegetara Retejo Mardo la 26a Kion vi ŝatis? Kion ni povas plibonigi? Kion ni faru se via projekto ne estas tuj elektita? Kiam vi volas manĝi? Ĉu vi manĝos ĉe la hotelo? Jes, retpoŝte je 3 tagoj antaŭe Jes, retpoŝte je antaŭ 5 tagoj Jes, telefone je 3 tagoj antaŭe Jes, telefone je antaŭ 5 tagoj Vi opiniis ke la paŭzoj estis... Vi opiniis ke la unua sesio estis... Vi opiniis ke la dua sesio estis... Viaj kontaktinformoj Viaj datumoj. Via retpoŝta adreso Via nomo ekz. +34 678 655 333 ekz. Asocio, kooperativo, kolektivo ekz. Aĉeti aparataron, dungi iun, ktp. ekz. FediBook ekz. Unua sesio estis tre interesa, kvankam mi ja atendis pli da informoj pri ĝi. ekz. ĜDPR-komformiĝo kaj lingva justeco. ekz. Mi ĝuis ambaŭ sesiojn. La instruisto estis ege pasia pri la temo. ekz. Mi jam estis en via restoracio kaj ĝi estas tre agrabla! ekz. Mi jam gastis en via hotelo kaj ĝi estas tre agrabla! ekz. Estis ĝenerale bona. Dankon. ekz. Simple por diri saluton! ekz. Ludoviko ekz. Zamenhof ekz. Hispanio ekz. Ne estis verda teo dum la kafpaŭzo. ekz. Ni volas verki libron pri la Fediverso. ekz. https://fedi.cat ekz. ludoviko@example.com ekz. maria@ekzemplo.com ne pli ol €5000 