��    �      D              l
  �   m
  s   �
  g   o  (  �  O      �   P  :  @  �   {  .   �     ,     A     Z     s  +   |     �  	   �  5   �  N   �  4   9  	   n     x  +   �  &   �  &   �               
                                   /     ;     M  -   l  /   �  .   �  8   �     2  "   >     a     u     �     �     �     �     �               &     3     ;     N     V     i  D   y  &   �  
   �     �       1     3   @  (   t     �     �     �  (   �     �       M   '     u  B   �     �  !   �  	   
  $     
   9     D     S  
   _  O   j     �     �  "   �     �     �               ,     I     Z     g  !   y     �     �     �     �     �     �  !   �  (         I     e     v     �     �  "   �  -   �       +   "  
   N  L   Y     �     �     �     �  6   �               #  	   ,     6     C     P  
   V     a     i  '   x  >   �     �     �          /     K     g     �  "   �  #   �     �  
   �  
     	          )   0  %   Z     �  X   �  ,   �  U     =   j  :   �  $   �       	         *  
   7  /   B  1   r     �     �     �     �  �  �  �   �!  �   �"  m   #  4  |#  _   �$  �   %  .  &  �   7'  @   �'  '   (  '   0(     X(  
   p(  +   {(     �(  	   �(  ?   �(  W   �(  D   K)  	   �)     �)  *   �)  +   �)  +   *     -*     /*     5*     9*     ?*     C*     E*     G*     I*  	   _*     i*  !   �*  1   �*  3   �*  =   +  B   F+     �+  .   �+     �+     �+     �+     �+     ,  $   2,      W,  #   x,  
   �,     �,  	   �,     �,     �,     �,     �,  J   	-  -   T-     �-  #   �-     �-  A   �-  '   .  *   5.     `.     t.     �.     �.     �.     �.  B   �.     :/  G   S/     �/  #   �/     �/  -   �/  
   0     *0     :0     G0  X   T0     �0     �0     �0     �0     �0     �0     1     1     >1     T1     \1  #   p1     �1  
   �1     �1     �1     �1     2  &   2  &   ;2      b2  
   �2     �2  8   �2     �2  %   �2  =   #3     a3  5   y3     �3  <   �3     �3     4     4     4  6   -4     d4  	   j4     t4     �4  
   �4     �4     �4  
   �4     �4     �4  $   �4  I   5     P5     i5  (   {5  (   �5     �5     �5  $   6  +   06  *   \6     �6     �6     �6  
   �6     �6  *   �6  0   7     F7  d   T7  4   �7  V   �7  6   E8  1   |8  '   �8     �8  
   �8     �8     9  /   9  0   E9     v9     �9     �9     �9   # Activity Feedback 
Thank you for participating in this activity. Please share with us your impression so we can improve future activities. # Congress II
Last year's congress was such a success we doing it again.

Register now for this year's sessions! # Contact Form
Have a question? Fill out this contact form and we'll get in touch as soon as possible. # Hotel booking
Come stay with us! Fill out this form to book a room at our hotel.

El Riuet Hotel
Carrer d'Orient, 33
Vidreres, 17411, Girona.
Catalunya, Espanya

Phone number: (+34) 93 553 43 25 
[Website](https://elriuethotel.com)
[Map](https://www.openstreetmap.org/way/218557184)
 # Lottery
Win 5 tickets to the cinema! Answer this form before September 20th. # Project Application Form
Have a project? Fill out this form to tell us about your proposal. 

This form is for project applications only. If you have any other question, please use our [contact form](https://example.com/contact-form). # Restaurant booking
Do you want to taste our delicious food? Fill out this form and save the date in your agenda!

[Delicious Restaurant Map](https://www.openstreetmap.org/way/314016327)
Carrer de les Avellanes, 15
El Secà de Sant Pere, 25005, Lleida.
Catalunya, Espanya

Phone number: (+34) 93 423 44 44 # Summer courses

We've prepared three days of courses and workshops.

Please reserve your place now. First in, first served. # We need your help
Please sign our petition. (check in from 12pm) (check out 12pm maximum) (from Tuesday to Sunday) (if any) (remember we only open from 20h30 to 23h30) 1 10h - 12h 10h - 13h. Computer Lab management with Free software 10h - 13h. Neutral networks. A practical presentation of our WIFI installation 10h - 13h. Presentation/demo. TPV to manage the cafe 12h - 14h 14h - 15h Lunch 16h - 19h. Social currencies. Local economy 18h - 20h. GIT for beginners Session 1 18h - 20h. GIT for beginners Session 2 2 20h30 21h 21h30 22h 3 4 5 A friend told me About right Activity feedback Add a second file if you wish. Add additional information about the project. Apply for a grant. Tell us about your proposal. Ask citizens to support your local initiative. Ask participants for their feedback about your activity. Attachments Book a room and come stay with us! Booking information Breakfast and dinner Breakfast and lunch Browsing the Internets Choose the day you want to come Comment about instructor Comment about tempos Comment about the content Comments Contact Form Content Cost justification Country Date for departure Date of arrival Do you want to taste our delicious food? Save a date in your agenda! Do you want us to send you a reminder? Double bed Double bed + single bed Email Explain here your project. Focus on what and how. Explain what the requested budget will be used for. Full board (breakfast, lunch and dinner) Future activities Have a comment? Hotel booking How did you find out about this lottery? How many people are you? How should we call you? I allow you to keep the information I submit for future funding opportunities I receive your newletters I want you to erase all information if the project is not selected I'm eating else where I'm eating out, thanks for asking ID number If you win, we'll send you an email! Instructor Just breakfast Just dinner Just lunch Let attendees choose one of two talks running in parallel and their lunch menu. Lottery Lunch and dinner Maximum 10 people per reservation. Message Name Name or nick No thanks. I will remember. No, thanks. I will remember. One day congress Organization Other information Other things you want to express. Poor to excellent Privacy Project Application Project description Project information Project name Put your name down to win a prize Rate the explainations of the instructor Rate the quality of content Requested amount Restaurant booking Room 1. AI descrimination Room 1. Municipal Strategies Room 2. Build an anonymous website Room 2. Decentralized tech. Freedom of speech Save our shelter Shared room (bunk beds with 3 other people) Single bed Students can enroll in a variety of activities spread out across three days. Subjects Summer courses Surname Telephone number Tell us if you are interested in a particular subject. Tempos Thursday 27th Too long Too short Tuesday 25th Type of room Vegan Vegaterian Website Wednesday 26th What did you like? What can we improve? What should we do if your project is not immediately selected? When do you want to eat? Will you eat at the hotel? Yes, by email 3 days before Yes, by email 5 days before Yes, by phone 3 days before Yes, by phone 5 days before You felt breaks were... You felt that first session was... You felt that second session was... Your contact information Your data. Your email Your name e.g. +34 678 655 333 e.g. Association, cooperative, collective e.g. Buy hardware, hire someone, etc. e.g. FediBook e.g. First session was very interesting although I did expect more information about it. e.g. GDPR compliance and linguistic justice. e.g. I did enjoy both sessions. The instructor was very passionate about the subject. e.g. I've already been in your restaurant and it's very nice! e.g. I've already stayed at your hotel and it's very nice! e.g. It was nice in general. Thanks. e.g. Just to say hello! e.g. Mary e.g. Poppins e.g. Spain e.g. There was no green tea in the coffeebreak. e.g. We want to write a book about the Fediverse. e.g. https://fedi.cat e.g. mary@example.com e.g. mary@exemple.com up to €5,000 Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2022-04-08 10:01+0200
PO-Revision-Date: 2021-12-28 09:53+0000
Last-Translator: Rita Barrachina <rita@liberaforms.org>
Language: ca
Language-Team: Catalan <https://hosted.weblate.org/projects/liberaforms/form-templates-liberaforms/ca/>
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
 # Avaluació d'activitat
Gràcies per participar en aquesta activitat. Si us plau, comparteix amb nosaltres les teves impressions. Així podrem millorar futures activitats. # Congrés II
El congrés de l'any passat va anar tant bé que el farem un altre cop.

Registra't ara per les sessions d'enguany! # Formulari de contacte
Tens alguna pregunta? Omple aquest formulari i et contactarem el més aviat possible. # Reserva d'hotel
Vine a visitar-nos! Omple aquest formulari per reservar una habitació al nostre hotel.

Hotel El Riuet
Carrer d'Orient, 33
Vidreres, 17411, Girona.
Catalunya, Espanya

Telèfon: (+34) 93 553 43 25
[Pàgina web](https://elriuethotel.com)
[Mapa](https://www.openstreetmap.org/way/218557184)
 # Sorteig
Guanya 5 tiquets per anar al cinema! Omple aquest formulari abans del 20 de setembre. # Proposta de projecte
Tens un projecte? Omple aquest formulari per explicar-nos la teva proposta.

Aquest formulari és només per propostes. Si tens preguntes, si us plau, usa el nostre [formulari de contacte](https://example.com/contact-form). # Reserva de restaurant
Vols tastar els nostres deliciosos plats? Reserva un dia a la teva agenda!

[Mapa del Restaurant Delicious](https://www.openstreetmap.org/way/314016327)
Carrer de les Avellanes, 15
El Secà de Sant Pere, 25005, Lleida.
Catalunya, Espanya

Número de telèfon: (+34) 93 423 44 44 # Cursos d'estiu

Hem preparat tres dies de cursos i tallers.

Si us plau, reserva la teva plaça. Els registres es faran per ordre d'arribada. # Necessitem la teva ajuda
Si us plau, signa la nostra petició. (entrada a partir de les 12 del migdia) (sortida màxim fins les 12 del migdia) (de dimarts a diumenge) (si escau) (recorda que només obrim de 20h30 a 23h30) 1 10h - 12h 10h - 13h. Gestió d'aules informàtiques amb programari lliure 10h - 13h. Xarxes neutrals. Una presentació pràctica de la nostra instal·lació WIFI 10h - 13h. Presentació/demostració. TPV per gestionar la cafeteria 12h - 14h 14h - 15h Dinar 16h - 19h. Monedes socials. Economia local 18h - 20h. GIT per a principiants Sessió 1 18h - 20h. GIT per a principiants Sessió 2 2 20h30 21h 21h30 22h 3 4 5 M'ho va dir una amiga Suficient Avaluació d'activitat Si vols, adjunta un segon fitxer. Afegeix informació addicional sobre el projecte. Demana ajut econòmic. Explica'ns la teva proposta. Demana suport a la ciutadania per a la teva iniciativa local. Pregunta a les participants què els ha semblat la teva activitat. Adjunts Queda't amb nosaltres, reserva una habitació! Informació de la reserva Esmorzar i sopar Esmorzar i dinar Navegant per les Internets Tria el dia que vols venir Fes un comentari sobre el formador/a Fes un comentari sobre els temps Fes un comentari sobre el contingut Comentaris Formulari de contacte Contingut Justificació de les despeses País Dia de sortida Dia d'arribada Vols tastar els nostres deliciosos plats? Reserva un dia a la teva agenda! Vols que et fem un recordatori de la reserva? Llit de matrimoni Llit de matrimoni + llit individual Correu electrònic Explica aquí el teu projecte. Posa el focus en el què i el com. Explica en què s'usarà el pressupost. Pensió completa (esmorzar, dinar i sopar) Properes activitats Vols afegir algun comentari? Reserva d'hotel Com has trobat aquest sorteig? Quantes persones sereu? Com vols que ens adrecem a tu? Permeto que guardeu la informació donada per futures oportunitats Rebo el vostre butlletí Vull que esborreu tota la informació si el projecte no és seleccionat No em quedaré a sopar Mengem fora, gràcies per preguntar Document d'identitat (DNI) Si guanyes, t'enviarem un correu electrònic! Formador/a Només esmorzar Només sopar Només dinar Permet a les assistents escollir una o dues xerrades en paral·lel i el menú del dinar. Sorteig Dinar i sopar Màxim 10 persones per reserva. Missatge Nom Nom o pseudònim No gràcies, me'n recordaré. No, gràcies. Me'n recordaré. Congrés d'un sol dia Entitat Altres informacions Altres coses que vulguis expressar. De insuficient a excel·lent Privacitat Proposta de projecte Descripció del projecte Informació del projecte Nom del projecte Escriu el teu nom per guanyar un premi Puntua les explicacions del formador/a Puntua la qualitat del contingut Pressupost Reserva de restaurant Sala 1. Discriminació de la Intel·ligència Artificial Sala 1. Estratègies municipals Sala 2. Crea una pàgina web anònima Sala 2. Tecnologies descentralitzades. Llibertat d'expressió Salvem el nostre refugi Habitació compartida (lliteres, amb 3 persones més) Llit individual Proposem participar en diverses activitats durant tres dies. Temes Cursos d'estiu Cognoms Número de telèfon Diga'ns si tens interès per algun tema en particular. Temps Dijous 27 Massa llarga Massa curta Dimarts 25 Tipus d'habitació Vegà Vegetarià Pàgina web del projecte Dimecres 26 Què t'ha agradat? Què milloraries? Què hauríem de fer si el teu projecte no és seleccionat immediatament? A quina hora vols sopar? Àpats a l'hotel? Sí, per correu electrònic 3 dies abans Sí, per correu electrònic 5 dies abans Sí, per telèfon 3 dies abans Sí, per telèfon 5 dies abans Vas sentir que les pauses van ser... Vas sentir que la primera sessió va ser... Vas sentir que la segona sessió va ser... Informació de contacte Les teves dades. El teu correu electrònic El teu nom p.e. +34 678 655 333 p.e. Associació, cooperativa, col·lectiu p.e. Comprar maquinari, contractar a algú, etc. p.e. FediBook p.e. La primera sessió va ser molt interessant tot i que m'esperava més informació sobre el tema. p.e. Acompliment del RGPD i justícia lingüística. p.e. Em van agradar les dues sessions. La formadora va posar molta passió en el tema. p.e. Ja he estat al vostre restaurant i és molt maco! p.e. Ja he estat al vostre hotel i és molt maco! p.e. Va estar bé en general. Gràcies. p.e. Només vull saludar! p.e. Maria p.e. Poppins p.e. Espanya p.e. A la pausa del cafè no hi havia té verd. p.e. Volem escriure un llibre sobre la Fedivers. p.e. https://fedi.cat p.e. maria@exemple.com p.e. maria@exemple.com fins a 5000 euros 