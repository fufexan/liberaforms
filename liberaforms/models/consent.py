"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from datetime import datetime, timezone
from sqlalchemy.dialects.postgresql import JSONB, TIMESTAMP, ENUM
from flask_babel import gettext as _
from liberaforms import db

from liberaforms.utils.database import CRUD
from liberaforms.utils import utils

#from pprint import pprint

class Consent(db.Model, CRUD):


    __tablename__ = "consents"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    is_dpl = db.Column(db.Boolean, default=True, nullable=False)
    title = db.Column(db.String, default="", nullable=False)
    markdown = db.Column(db.String, default="", nullable=False)
    html = db.Column(db.String, default="", nullable=False)
    checkbox_text = db.Column(db.String, default="", nullable=False)
    required = db.Column(db.Boolean, default=True, nullable=False)
    enabled = db.Column(db.Boolean, default=False, nullable=False)
    shared = db.Column(db.Boolean, default=False, nullable=False)
    site_id = db.Column(db.Integer, db.ForeignKey('site.id'), unique=True, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=True)
    form_id = db.Column(db.Integer, db.ForeignKey('forms.id', ondelete="CASCADE"), nullable=True)

    def __init__(self, **kwargs):
        self.created = datetime.now(timezone.utc)
        for key, value in kwargs.items():
            setattr(self, key, value)

    #def save(self):
    #    super().save()
    #    print(utils.print_obj_values(self))

    def __str__(self) -> str:
        return utils.print_obj_values(self)

    @classmethod
    def find(cls, **kwargs):
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        return cls.query.filter_by(**kwargs)

    @classmethod
    def get_default_consent(cls):
        title = _("GDPR")
        text = _("We take your data protection seriously. Please contact us for any inquiries.")
        kwargs = {
            'markdown': f"###### {title}\n\n{text}",
            'html': f"<h6>{title}</h6><p>{text}</p>",
            'checkbox_text': '',
            'required': True,
            'enabled': False,
            'shared': False,
        }
        return Consent(**kwargs)
