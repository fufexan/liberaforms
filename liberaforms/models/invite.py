"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from datetime import datetime, timezone
from flask import current_app
from flask_babel import gettext as _
from sqlalchemy.dialects.postgresql import JSONB, TIMESTAMP, UUID
from liberaforms.utils.database import CRUD
from liberaforms import db
from liberaforms.models.user import User
from liberaforms.utils import utils

#from pprint import pprint

class Invite(db.Model, CRUD):
    """Invite model definition."""

    __tablename__ = "invites"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    email = db.Column(db.String, nullable=False)
    message = db.Column(db.String, nullable=True)
    token = db.Column(JSONB, nullable=False)
    role = db.Column(db.String, default="editor", nullable=False)
    invited_by_id = db.Column(db.Integer, nullable=True)
    granted_form = db.Column(JSONB, nullable=True)  # Invitation grants access to this form
    ldap_uuid = db.Column(UUID, nullable=True) # The invited user can be found on the LDAP DIT

    def __init__(self, **kwargs):
        """Create a new Invite object."""
        self.created = datetime.now(timezone.utc)
        self.email = kwargs["email"]
        self.message = kwargs["message"]
        self.token = kwargs["token"]
        self.role = kwargs["role"]
        self.invited_by_id = kwargs["invited_by_id"]
        self.granted_form = kwargs["granted_form"] if 'granted_form' in kwargs else None
        self.ldap_uuid = kwargs["ldap_uuid"] if 'ldap_uuid' in kwargs else None

    def __str__(self):
        """Use for debugging."""
        return utils.print_obj_values(self)

    @classmethod
    def find(cls, **kwargs):
        """Return first Invite filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        """Return all Invites filtered by kwargs."""
        filters = []
        if 'token' in kwargs:
            filters.append(cls.token.contains({'token':kwargs['token']}))
            kwargs.pop('token')
        if 'granted_form' in kwargs:
            filters.append(cls.granted_form.contains({'id':kwargs['granted_form']}))
            kwargs.pop('granted_form')
        for key, value in kwargs.items():
            filters.append(getattr(cls, key) == value)
        return cls.query.filter(*filters).order_by(Invite.created.desc())

    def get_link(self) -> str:
        """Return public link to Invite."""
        return f"{current_app.config['BASE_URL']}/user/new/{self.token['token']}"

    def get_message(self) -> str:
        """Return Invite message."""
        return f"{self.message}\n\n{self.get_link()}"

    def get_inviter(self):
        """Return User object."""
        if not self.invited_by_id:
            return None
        user = User.find(id=self.invited_by_id)
        return user if user else None

    def has_ldap_entry(self):
        """When the invited email address exists on the LDAP DIT."""
        return bool(self.ldap_uuid)

    @staticmethod
    def default_message() -> str:
        """Return defaut Invite message."""
        # i18n: Template message for email invitation. '\n' is used for linebreak.
        return _("Hello,\n\nYou have been invited to LiberaForms.\n\nRegards.")
