"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
from liberaforms import ma
from liberaforms.models.invite import Invite
from liberaforms.utils import utils


class InviteSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Invite

    id = ma.Integer()
    created = ma.Method('get_created')
    email = ma.auto_field()
    message = ma.Method('get_message')
    role = ma.auto_field()
    invited_by = ma.Method('get_invited_by')

    def get_created(self, obj) -> str:
        return utils.utc_to_g_timezone(obj.created).strftime("%Y-%m-%d %H:%M:%S")

    def get_message(self, obj) -> str:
        return obj.get_message().replace("\n", "<br/>")

    def get_invited_by(self, obj):
        if obj.invited_by_id:
            user=obj.get_inviter()
            if user:
                url=url_for('admin_bp.inspect_user', user_id=obj.invited_by_id)
                return f"<a href='{url}'>{user.username}</a>"
        return ""
