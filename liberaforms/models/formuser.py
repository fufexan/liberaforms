"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from datetime import datetime, timezone
from sqlalchemy.dialects.postgresql import JSONB, TIMESTAMP
from sqlalchemy.ext.mutable import MutableDict
from liberaforms import db
from liberaforms.utils.database import CRUD

class FormUser(db.Model, CRUD):
    """FormUser model definition."""

    __tablename__ = "form_users"
    id = db.Column(db.Integer, primary_key=True, index=True, unique=True)
    created = db.Column(TIMESTAMP, nullable=False)
    form_id = db.Column(db.Integer, db.ForeignKey('forms.id',
                                                ondelete="CASCADE"),
                                                nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id',
                                                ondelete="CASCADE"),
                                                nullable=False)
    is_editor = db.Column(db.Boolean, default=False)
    notifications = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    field_index = db.Column(JSONB, nullable=True)
    order_by = db.Column(db.String, nullable=True) # data_display preference
    ascending = db.Column(db.Boolean, default=True) # data_display preference
    user = db.relationship("User", viewonly=True)
    form = db.relationship("Form", viewonly=True)


    def __init__(self, **kwargs):
        """Create a new FormUser object."""
        self.created = datetime.now(timezone.utc)
        self.user_id = kwargs['user_id']
        self.form_id = kwargs['form_id']
        self.is_editor = kwargs['is_editor']
        self.notifications = kwargs['notifications']

    @classmethod
    def find(cls, **kwargs):
        """Return first FormUser filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        """Return all FormUsers filtered by kwargs."""
        return cls.query.filter_by(**kwargs)

    def toggle_expiration_notification(self) -> bool:
        """Enable/disable expiration notification."""
        self.notifications['expiredForm'] = not self.notifications['expiredForm']
        self.save()
        return self.notifications['expiredForm']

    def toggle_new_answer_notification(self) -> bool:
        """Enable/disable new answer notification."""
        self.notifications['newAnswer'] = not self.notifications['newAnswer']
        self.save()
        return self.notifications['newAnswer']

    def get_field_index_preference(self) -> list:
        """Return a User's field_index order preference."""
        return self.field_index

    def save_field_index_preference(self, field_index:list) -> None:
        """Save a User's field_index order preference."""
        self.field_index = field_index
        self.save()

    def get_order_by_field(self) -> str:
        """User's "order answers by field" preference.

        Returns a field name.
        """
        return self.order_by

    def save_order_by(self, field_name:str) -> None:
        """Save a User's "order answers by field" preference."""
        self.order_by = field_name
        self.save()

    def toggle_ascending_order(self) -> bool:
        """Toggle User's ascending order preference."""
        self.ascending = not self.ascending
        self.save()
        return self.ascending
